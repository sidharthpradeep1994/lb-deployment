'use strict';

module.exports = function (EtlLog) {

    const { registerWebservices } = require('../factories/factory');

    registerWebservices(EtlLog, 'etlLog', ['filterEtlLogs']);

};
