'use strict';

module.exports = function (Therapy) {
    const { registerWebservices } = require('../factories/factory');
    registerWebservices(Therapy, 'therapy', ['addClientTherapy', 'fetchTherapies']);
};
