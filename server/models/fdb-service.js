'use strict';

module.exports = function (Fdbservice) {
    const { registerWebservices } = require('../factories/factory');
    registerWebservices(Fdbservice, 'fdbService', ['ndcScreen']);
};
