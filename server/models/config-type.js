'use strict';

module.exports = function (Configtype) {
    const { registerWebservices } = require('../factories/factory');
    registerWebservices(Configtype, 'configType', ['fetchTherapies']);
};
