'use strict';

module.exports = function(Languages) {
    const { registerHooks, registerWebservices } = require('../factories/factory');
    registerWebservices(Languages, 'languages', ['addLanguage','fetchLanguages']);
};
