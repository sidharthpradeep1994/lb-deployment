'use strict';

module.exports = function(Logdump) {
    const { registerHooks, registerWebservices } = require('../factories/factory');

    registerWebservices(Logdump, 'logDump', ['filterLogs'])
};
