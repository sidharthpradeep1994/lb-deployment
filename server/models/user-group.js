'use strict';

module.exports = function (UserGroup) {

    
    const { registerWebservices, registerACLs } = require('../factories/factory');

    registerWebservices(UserGroup, 'userGroup', ['addUserGroup' , 'addUsers', 'fetchUserGroups', 'fetchUsers', 'toggleMemberAdmin']);


};
 