'use strict';

module.exports = function (Patientmaster) {
    const { registerHooks, registerWebservices } = require('../factories/factory');
    registerHooks(Patientmaster, 'patientMaster', ['beforeSave']);
    registerWebservices(Patientmaster, 'patientMaster', ['filterPatients', 'fetchPatientInfo', 'updatePatientInfo']);

    Patientmaster.validatesUniquenessOf('patientClientId');
};
