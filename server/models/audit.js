'use strict';

module.exports = function (Audit) {
    const { registerHooks, registerWebservices } = require('../factories/factory');

    registerWebservices(Audit, 'audit', ['filterAudits']);


};
