'use strict';

module.exports = function(Medication) {
    const { registerHooks, registerWebservices } = require('../factories/factory');
    registerHooks(Medication, 'medication', ['beforeSave']);
};
