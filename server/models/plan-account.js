'use strict';

module.exports = function(Planaccount) {
const { registerWebservices, registerHooks } = require('../factories/factory');

registerWebservices(Planaccount, 'planAccount', ['filterAccounts', 'createAccount']);
};
