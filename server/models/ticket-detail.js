'use strict';
module.exports = function (Ticketdetail) {
    const { registerHooks, registerWebservices } = require('../factories/factory');
    registerWebservices(Ticketdetail, 'ticketDetail', ['addTicketDetail','fetchTicketDetail','deleteTicketDetail']);
};
