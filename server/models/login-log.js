'use strict';

module.exports = function(Loginlog) {

 const { registerWebservices } = require('../factories/factory');

 registerWebservices(Loginlog, 'loginLog', ['filterLoginLogs'])

};
