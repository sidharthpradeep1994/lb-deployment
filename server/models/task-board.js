'use strict';
module.exports = function (TaskBoard) {
    const { registerHooks, registerWebservices } = require('../factories/factory');
    registerWebservices(TaskBoard, 'taskBoard', ['assignTask','createUpdateTaskboard','fetchTaskboard']);
};
