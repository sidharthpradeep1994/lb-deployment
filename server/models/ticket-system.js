'use strict';
module.exports = function (Ticketsystem) {
    const { registerHooks, registerWebservices } = require('../factories/factory');
    registerWebservices(Ticketsystem, 'ticketSystem', ['createTicket','fetchTickets','deleteTicket','updateTicketStatus','filterTickets']);
};
