'use strict';

module.exports = function (Program) {
    const { registerHooks, registerWebservices } = require('../factories/factory');

    registerWebservices(Program, 'program', ['fetchPrograms', 'fetchProgram', 'saveProgram', 'deleteProgram',
        'saveConfig', 'selectProgram', 'getProgramProperties', 'saveReportingProperties', 'abortProgram']);
};
