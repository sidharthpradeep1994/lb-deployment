'use strict';

module.exports = function (Client) {
    const { registerHooks, registerWebservices } = require('../factories/factory');
    registerWebservices(Client, 'client', ['fetchUserLogs','addTherapyById', 'addTherapyByTag', 'fetchTherapies', 'filterClients', 'disableClient']);
};
