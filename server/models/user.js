'use strict';

module.exports = function (User) {
    const { registerWebservices, registerACLs, registerHooks } = require('../factories/factory');
    const acls = require('../acls/user');
    User = registerACLs(User, acls);
    registerWebservices(User, 'user', ['registerUser', 'filterUsers','filterUsersForLogs','filterUsersForChat','updateUser', 'fetchUser', 'fetchSecurityQuestions',
        'resetOneTimePassword', 'signin', 'setPassword', 'userCreateOneTimePassword', 'resetUserPassword', 'fetchResetQuestions',
        'filterToSelect', 'filterLoginLogs', 'toggleUserActivation', 'deleteUser', 'validatePasswordReset']);
    registerHooks(User, 'user', ['login', 'changePassword', 'logout']);

};
