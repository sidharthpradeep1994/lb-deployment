'use strict';

module.exports = function (Patientcaseregistry) {
    Patientcaseregistry.validatesUniquenessOf('clientCaseId');
    const { registerWebservices, registerACLs } = require('../factories/factory');
    const acls = require('../acls/patientcase-registry')
    registerWebservices(Patientcaseregistry, 'patientCaseRegistry', ['filterRegistries', 'addCaseLog',
        'fetchSummary', 'saveTherapy', 'fetchRecommendation', 'updateRecommendation', 'resetMedication', 'removeMedication',
        'addMedication', 'updateMedication', 'updateAllergy', 'updateCaseNotes', 'fetchOverallRecommendations', 'updateOverallRecommendation'
        , 'completePatientCase', 'fetchStatusCount', 'updateMedicationNotes', 'updateMedicineOrder',
        'savePdfLetters', 'downloadPdf', 'assignUsers', 'suspendCase', 'scheduledCases', 'fetchMonthlyCaseList', 'filterToSelect', 'assignCasesToUsers',
        'downloadReports', 'caseStatistics']);

    registerACLs(Patientcaseregistry, acls); 

};
