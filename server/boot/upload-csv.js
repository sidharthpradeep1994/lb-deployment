module.exports = function (app) {

  const fs = require('fs-extra');
  const busboy = require('connect-busboy');
  const path = require('path');
  const csv = require('csvtojson');
  const cors = require('cors');
  const whitelist = ['http://adysaas.com', 'http://www.adysaas.com']
  const corsOptions = {
    origin: function (origin, callback) {
      if (whitelist.indexOf(origin) !== -1) {
        callback(null, true)
      } else {
        callback(new Error('Not allowed by CORS'))
      }
    }
  }
  app.use(cors());

  app.use(busboy());
  // app.use(app.static(path.join(__dirname, 'public')));

  app.route('/upload/:recordType/:clientId')
    .post(function (req, res, next) {

      let fstream;
      req.pipe(req.busboy);
      req.busboy.on('file', function (fieldname, file, filename) {
        const CURRENT_TIMESTAMP = Date.now();
        //Path where image will be uploaded
        fstream = fs.createWriteStream(__dirname + '/../csv/' + CURRENT_TIMESTAMP + '.csv');
        const csvFilePath = __dirname + '/../csv/' + CURRENT_TIMESTAMP + '.csv';
        file.pipe(fstream);
        fstream.on('close', function () {
          switch (req.params.recordType) {
            case 'patient':
              uploadPatients(csvFilePath, req.params.clientId, CURRENT_TIMESTAMP);
              break;
            case 'medication':
              uploadMedicationRecords(csvFilePath + filename, req.params.clientId, null, CURRENT_TIMESTAMP);
              break;

            case 'therapies':
              uploadTherapyRecords(csvFilePath + filename, req.params.clientId, CURRENT_TIMESTAMP);
              break;

            default:

              break;
          }

          res.send({ status: 'Upload Successful', action: true, failed: [] });           //where to go next
        });
      });
    });



  // using program id
  // should we make a new app.route() function to add account id
  app.route('/upload/:recordType/:clientId/:programId')
    .post(function (req, res, next) {

      let fstream;
      req.pipe(req.busboy);
      req.busboy.on('file', function (fieldname, file, filename) {

        const CURRENT_TIMESTAMP = Date.now();
        //Path where image will be uploaded
        fstream = fs.createWriteStream(__dirname + '/../csv/' + CURRENT_TIMESTAMP + '.csv');
        const csvFilePath = __dirname + '/../csv/' + CURRENT_TIMESTAMP + '.csv';
        file.pipe(fstream);
        fstream.on('close', function () {
          switch (req.params.recordType) {
            case 'patient':
              uploadPatients(csvFilePath, req.params.clientId, CURRENT_TIMESTAMP);
              break;
            case 'medication':
              uploadMedicationRecords(csvFilePath, req.params.clientId, req.params.programId, CURRENT_TIMESTAMP);
              break;

            case 'therapies':
              uploadTherapyRecords(csvFilePath, req.params.clientId, req.params.programId, CURRENT_TIMESTAMP);
              break;

            default:

              break;
          }

          res.send({ status: 'Upload Successful', action: true, failed: [] });           //where to go next
        });
      });
    });



  app.route('/upload-cases/:clientId/:programId/:accountId')
    .post(function (req, res, next) {

      let fstream;

      const CURRENT_TIMESTAMP = Date.now();

      req.pipe(req.busboy);

      let patientFile;

      let medicationFile;

      let description;

      files = [];


      req.busboy.on('file', function (fieldname, file, filename) {
        let csvFilePath;

        csvFilePath = __dirname + '/../csv/' + CURRENT_TIMESTAMP + '_' + fieldname + '.csv';

        fstream = fs.createWriteStream(csvFilePath);

        file.pipe(fstream);

        files.push({
          fieldName: fieldname, fileName: filename, file: file, path: csvFilePath
        });




      });





      req.busboy.on('field', function (fieldName, value) {

        description = value;

      });


      req.busboy.on('finish', async function () {



        const medicationsPath = files[files.findIndex(file => file.fieldName == 'medications')].path;

        const patientsPath = files[files.findIndex(file => file.fieldName == 'patients')].path;

        const programData = await app.models.program.findById(req.params.programId);
        const planData = await app.models.planAccount.findById(req.params.accountId);
        const clientData = await app.models.client.findById(req.params.clientId);
        const etlLog = await app.models.etlLog.create({ description: description, programId: req.params.programId, programName: programData.name, planId: req.params.accountId, createdTimeStamp: CURRENT_TIMESTAMP, clientId: req.params.clientId, plan: planData.name, clientName: clientData.name });
        res.send({ status: 'Upload Successful' });
        uploadPatients(patientsPath, req.params.clientId, CURRENT_TIMESTAMP, req.params.accountId, etlLog).then(() => uploadMedicationRecords(medicationsPath, req.params.clientId, req.params.programId, CURRENT_TIMESTAMP, req.params.accountId, etlLog).then(() => {

        }));

      });
      // req.busboy.on('file', function (fieldname, file, filename) {

      //   const CURRENT_TIMESTAMP = Date.now();
      //   //Path where image will be uploaded
      //   fstream = fs.createWriteStream(__dirname + '/../csv/' + CURRENT_TIMESTAMP + '.csv');
      //   const csvFilePath = __dirname + '/../csv/' + CURRENT_TIMESTAMP + '.csv';
      //   file.pipe(fstream);
      //   fstream.on('close', function () {
      //     switch (req.params.recordType) {
      //       case 'patient':
      //         uploadPatients(csvFilePath, req.params.clientId, CURRENT_TIMESTAMP);
      //         break;
      //       case 'medication':
      //         uploadMedicationRecords(csvFilePath, req.params.clientId, req.params.programId, CURRENT_TIMESTAMP);
      //         break;

      //       case 'therapies':
      //         uploadTherapyRecords(csvFilePath, req.params.clientId, req.params.programId, CURRENT_TIMESTAMP);
      //         break;

      //       default:

      //         break;
      //     }

      //     res.send({ status: 'Upload Successful', action: true, failed: [] });           //where to go next
      //   });
      // });
    });


  //Change: Should we add accountId: for associating a case to particular account?
  async function uploadPatients(path, clientId, currentTimeStamp, accountId, etlLog) {
    return new Promise(async (resolve, reject) => {

      try {

        const csvFilePath = path;
        //converts csv in given path to respective json
        const csvArray = await csv().fromFile(csvFilePath);
        etlLog.updateAttributes({ patientRecordCount: csvArray.length });
        const patientArray = csvArray.map((patient) => {
          return {
            memberId: patient['Member ID'],
            contractName: patient['Contract Name'],
            contractId: patient['Contract ID'],
            //Change: adherenceCaseId to caseId: adherenceCaseId: patient['Adherence Case ID'],
            caseId: patient['Case ID'],
            //Change: adherence to case
            caseInitiationDate: new Date(patient['Case Initiation Date']),
            //Change: adherence to case
            caseExpirationDate: new Date(patient['Case Expiration Date']),
            firstName: patient['First Name'],
            middleName: patient['Middle Name'],
            lastName: patient['Last Name'],
            gender: patient['Gender'],
            dob: patient['DOB'],
            //Change: key changed from Language to Preferred Language
            language: patient['Preferred Language'],
            primaryPhoneNumber: patient['Phone Number (P)'],
            secondaryPhoneNumber: patient['Phone Number (S)'],
            primaryAddress: {
              addressLine1StreetName: patient['Address (P)'],
              city: patient['City (P)'],
              state: patient['State (P)'],
              zipCode: patient['Zip (P)']
            },
            secondaryAddress: {
              addressLine1StreetName: patient['Address (S)'],
              city: patient['City (S)'],
              state: patient['State (S)'],
              zipCode: patient['Zip (S)']
            },
            addresses: [{
              addressLine1StreetName: patient['Address (P)'],
              city: patient['City (P)'],
              state: patient['State (P)'],
              zipCode: patient['Zip (P)']
            },
            {
              addressLine1StreetName: patient['Address (S)'],
              city: patient['City (S)'],
              state: patient['State (S)'],
              zipCode: patient['Zip (S)']
            }],
            //Do we need patientId and memberID separately?
            patientId: patient['Member ID'],
            //Change: in the following 4 fields; Also address changed to
            primaryCarePhysician: patient['PCP Name'],
            primaryCarePhysicianNPINumber: patient['PCP NPI'],
            primaryCarePhysicianPhoneNumber: patient['PCP Phone'],
            primaryCarePhysicianFaxNumber: patient['PCP Fax'],
            //The following field has been changed to an object. Would that affect the pdf creation?
            primaryCarePhysicianAddress: {
              addressLine1StreetName: patient['PCP Address'],
              city: patient['PCP City'],
              state: patient['PCP State'],
              zipCode: patient['PCP Zip']
            },

            clientId: clientId,
            createdTimeStamp: currentTimeStamp,
            patientClientId: patient['Member ID'] + clientId,
            accountId: accountId, etlId: etlLog.id, programName: etlLog.programName, clientName: etlLog.clientName
          };

        });
        const PatientMaster = app.models.patientMaster;

        let index = 0;
        createPatient();
        async function createPatient() {
          const patient = await PatientMaster.findOne({ where: { and: [{ memberId: patientArray[index].memberId, clientId: patientArray[index].clientId }] }, skip: 0, limit: 0, fields: [], order: '' })
          if (patient) {
            await patient.updateAttributes(patientArray[index]);
          }
          else
            await PatientMaster.create(patientArray[index]);
          index++;
          if (index < patientArray.length) {
            createPatient();
          } else {
            resolve();
          }
        }
      } catch (error) {
        console.error(error);
      }
    })
  }


  async function uploadMedicationRecords(path, clientId, programId, currentTimeStamp, accountId, etlLog) {
    return new Promise(async (resolve, reject) => {
      try {


        const csvFilePath = path;
        //converts csv in given path to respective json
        const csvArray = await csv().fromFile(csvFilePath);
        etlLog.updateAttributes({ medicationRecordCount: csvArray.length });
        const caseArray = csvArray.map((medication) => {
          console.log(medication['Drug Therapy Problems'])
          return {
            memberId: medication['Member ID'],
            //Change: Changed adherenceCaseId to caseId
            caseId: medication['Case ID'],
            ndc: medication['NDC'],
            medicationName: medication['Medication'],
            formulation: medication['Administering'],
            dosage: medication['Dosage'],
            condition: medication['Condition'],
            //Change: Added rxNumber
            rxNumber: medication['RX Number'],
            lastFilled: medication['Last Fill'],
            daysSupply: medication['Days Supply'],
            quantity: medication['Quantity'],
            //Re-ordered
            brand: medication['Brand'],
            frequency: medication['Frequency'],
            refills: medication['Refills'],
            //Re-ordered
            prescribedDate: medication['Prescribed Date'],
            renewedDate: medication['Renewed Date'],
            //
            prescriberName: medication['Prescriber Name'],
            prescriberNPINumber: medication['Prescriber NPI'],
            prescriberPhoneNumber: medication['Prescriber Phone'],
            prescriberFaxNumber: medication['Prescriber Fax'],
            //Change: The following field has been added
            prescriberAddress: {
              addressLine1StreetName: medication['Prescriber Address'],
              city: medication['Prescriber City'],
              state: medication['Prescriber State'],
              zipCode: medication['Prescriber Zip']
            },
            //

            pharmacyName: medication['Pharmacy Name'],
            //Change:
            pharmacyNCPDPNumber: medication['Pharmacy NABP/NCPDP Number'],
            pharmacyNPI: medication['Pharmacy NPI'],
            pharmacyPhoneNumber: medication['Pharmacy Phone'],
            pharmacyFaxNumber: medication['Pharmacy Fax'],

            //The following field has been added
            pharmacyAddress: {
              addressLine1StreetName: medication['Pharmacy Address'],
              city: medication['Pharmacy City'],
              state: medication['Pharmacy State'],
              zipCode: medication['Pharmacy Zip']
            },

            adheranceFlag: medication['Adherence Flag Raised'],
            noLongerTakingFlag: medication['No Longer Taking Flag'],
            //added later *
            drugTherapyProblems: medication['Drug Therapy Problems'] ? medication['Drug Therapy Problems'].trim().split(',') : [],
            /** */
            programId: programId,
            clientId: clientId,
            clientName: etlLog.clientName,
            programName: etlLog.programName,
            accountName: etlLog.plan,
            createdTimeStamp: currentTimeStamp, accountId, etlId: etlLog.id, createdAt: new Date(), updatedAt: new Date()
          };

        });
        const PatientCaseregistry = app.models.medication;
        // const medications = await PatientCaseregistry.create(caseArray);
        let index = 0;
        while (index < caseArray.length) {
          await createMedications(caseArray[index]);

          index++;
        }
        // ;
        console.log(index);
        resolve();
      } catch (error) {
        console.error(error);
      }
    });
  }



  async function createMedications(medication) {
    try {
      let patientCaseRegistry = await app.models.patientCaseRegistry.findOne({
        where: {
          and: [{ memberId: medication.memberId }, { caseId: medication.caseId }, { clientId: medication.clientId }]
        }
      });
      // let patientCaseRegistry = await app.models.patientCaseRegistry.findOne({
      //   where: {
      //     and: [{ clientCaseId: medication.caseId + medication.memberId + medication.clientId}]
      //   }
      // });
      console.log(medication)
      console.log(patientCaseRegistry)
      const patient = await app.models.patientMaster.
        findOne({ where: { and: [{ memberId: medication.memberId }, { clientId: medication.clientId }] } });

      if (!patientCaseRegistry) {
        patientCaseRegistry = await app.models.patientCaseRegistry.create({
          patientCode: patient.patientCode,
          caseId: medication.caseId,
          contractId: patient.contractId,
          contractName: patient.contractName,
          memberId: medication.memberId,
          patientUid: patient.id,
          clientId: medication.clientId,
          patientInfo: patient,
          programId: medication.programId,
          accountId: medication.accountId,
          programName: medication.programName,
          accountName: medication.accountName,
          createdTimeStamp: medication.createdTimeStamp,
          clientCaseId: medication.caseId + medication.memberId + medication.clientId
        });
      }
      const existingMedication = await patientCaseRegistry.medicationRegistry.findOne({
        where: {
          and: [
            { caseId: medication.caseId }, { rxNumber: medication.rxNumber }, { ndc: medication.ndc }, { medicationName: medication.medicationName }
          ]
        }
      });
      if (!existingMedication) {
        return await patientCaseRegistry.medicationRegistry.create(medication);
      }
      // ctx.instance.patientUid = patient.id;
      else {
        return;
      }
    } catch (error) {
      throw error;
    }
  }

  async function uploadTherapyRecords(path, clientId, currentTimeStamp) {
    try {

      const csvFilePath = path;
      //converts csv in given path to respective json
      const csvArray = await csv().fromFile(csvFilePath);


      const tps = csvArray.map(async (tp) => await createTherapy(tp, clientId, currentTimeStamp));
      console
        .log(tps);
    }
    catch (error) {
      throw error;
    }
  }


  async function createTherapy(therapyData, clientId, currentTimeStamp) {
    const configType = app.models.configType;
    const category = app.models.category;
    const therapy = app.models.therapy;
    const configData = await configType.findOne({ where: { and: [{ name: therapyData.configName }, { clientId: clientId }] }, skip: 0, limit: 0, fields: [], order: '' });
    const categoryData = await category.findOne({ where: { and: [{ name: therapyData.category }, { clientId: clientId }] }, skip: 0, limit: 0, fields: [], order: '' });
    if (configData) {
      therapyData.configId = configData.id;
      therapyData.configName = configData.name;
      therapyData.programId = configData.programId;
    }
    if (!categoryData) {
      therapyData.categoryName = configData.category;
    }
    else {
      therapyData.categoryId = categoryData.id;
      therapyData.categoryName = categoryData.name;
    }


    therapyData.orderIndex = therapyData.Index;
    therapyData.createdTimeStamp = currentTimeStamp;
    therapyData.clientId = clientId;
    therapyData.name = therapyData.shortDesc;
    return await therapy.create(therapyData);

  }


  app.route('/upload-therapy/:category/:configType/:programId/:clientId')
    .post(function (req, res, next) {
      let fstream;
      const reservedFields = ['Index', 'shortDesc', 'toolTip', 'Short Desc', 'Tool Tip', 'index', 'Tool Tip'];
      req.pipe(req.busboy);
      req.busboy.on('file', function (fieldname, file, filename) {

        const CURRENT_TIMESTAMP = Date.now();
        //Path where image will be uploaded
        fstream = fs.createWriteStream(__dirname + '/../csv/' + CURRENT_TIMESTAMP + '.csv');
        file.pipe(fstream);

        fstream.on('close', async function () {

          const csvFilePath = __dirname + '/../csv/' + CURRENT_TIMESTAMP + '.csv';
          //converts csv in given path to respective json
          const csvArray = await csv().fromFile(csvFilePath);


          const tps = csvArray.map((tp) => {
            tp.reportingStatements = {};
            for (const key in tp) {
              if (tp.hasOwnProperty(key)) {
                if (reservedFields.findIndex(fld => fld === key) < 0 && key !== 'reportingStatements') {

                  tp.reportingStatements[camelCase(key)] = tp[key];

                  tp[key] = undefined;
                  console.log(tp)
                }


              }
            }
            tp.index = tp.Index ? tp.Index : tp.index;
            tp.shortDesc = tp['Short Desc'];
            tp.name = camelCase(tp['Short Desc']);
            tp['Short Desc'] = undefined;
            tp.createdTimeStamp = CURRENT_TIMESTAMP;
            return tp;
          }).map(async (tp) => await addTherapy(tp, req.params.clientId, req.params.category, req.params.programId, req.params.configType));


          res.send({ status: 'Upload Successful', action: true, failed: [] });           //where to go next
        });
      });
    });

  function camelCase(str) {
    return str.replace(/(?:^\w|[A-Z]|\b\w)/g, (word, index) => {
      return index === 0 ? word.toLowerCase() : word.toUpperCase();
    }).replace(/\s+/g, '');
  }



  async function addTherapy(therapyData, clientId, categoryName, programId, configName) {
    const configType = app.models.configType;
    const category = app.models.category;
    const therapy = app.models.therapy;
    const configData = await configType.findOne({ where: { and: [{ name: configName }, { clientId: clientId }, { programId: programId }] }, skip: 0, limit: 0, fields: [], order: '' });
    const categoryData = await category.findOne({ where: { and: [{ name: therapyData.category }, { clientId: clientId }] }, skip: 0, limit: 0, fields: [], order: '' });
    if (configData) {
      therapyData.configId = configData.id;
      therapyData.configName = configData.name;

    } else {
      therapyData.configName = configName;
    }
    // if (!categoryData) {
    //     therapyData.categoryName = configData.category;
    // }
    // else {
    //     therapyData.categoryId = categoryData.id;
    //     therapyData.categoryName = categoryData.name;
    // }

    therapyData.programId = programId;
    therapyData.categoryName = categoryName;
    therapyData.orderIndex = therapyData.Index;

    therapyData.clientId = clientId;
    therapyData.name = therapyData.shortDesc;
    return await therapy.create(therapyData);

  }


}
