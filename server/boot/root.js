'use strict';

module.exports = function (server) {
  // Install a `/` route that returns server status
  var router = server.loopback.Router();
  // router.get('/', server.loopback.status());


  const sendEmail = require('../utils/send-email');

  // sendEmail('adil@adysas.com', 'support@adysas.com', 'Password Reset', ' ', '<strong>Your password is reset</strong>');


  router.get('/test', async function (req, res) {
    const transactions = [{
      slNo: 1, referenceId: '12124',
      vehicleNumber: 'Kl 41 c 8281',
      baseAmount: 8,
      gst: 2,
      total: 10
    }]
    const caseRegistry = {
      "patientCode": "AD8",
      "patientUid": "5d54469426fb1133ca162014",
      "caseLogs": [
        {
          "id": "3e7f8230-c1a1-11e9-8b9c-df9684cee4ec",
          "callType": "outbound",
          "connectedTo": "patient",
          "dispositionOption": "connected",
          "status": "inprogress",
          "notes": "",
          "attemptedOn": "2019-08-18T10:16:29.395Z",
          "attemptedBy": "5ce7bdfcc6cca95c8535160a",
          "username": "adil",
          "createdAt": "2019-08-18T10:16:29.395Z"
        }
      ],
      "contractId": "10000008",
      "contractName": "ABC Healthplan",
      "memberId": "1000008",
      "clientId": "5ce3b0ab6176a04ece7b4292",
      "status": "inprogress",
      "discontinued": false,
      "physicianLetter": [
        {
          "attributes": {
            "bold": true
          },
          "insert": "Date: "
        },
        {
          "attributes": {},
          "insert": " 18/08/2019\n"
        },
        {
          "attributes": {
            "bold": true
          },
          "insert": "Patient: "
        },
        {
          "attributes": {},
          "insert": "Mike Campell Barnhart,"
        },
        {
          "attributes": {
            "bold": true
          },
          "insert": "DOB: "
        },
        {
          "attributes": {},
          "insert": "Wed Mar 06 2002,"
        },
        {
          "attributes": {
            "bold": true
          },
          "insert": "Phone: "
        },
        {
          "attributes": {},
          "insert": "3943317041,"
        },
        {
          "attributes": {
            "bold": true
          },
          "insert": "Health Plan: "
        },
        {
          "attributes": {},
          "insert": "ABC Healthplan \n"
        },
        {
          "attributes": {
            "bold": true
          },
          "insert": "To: "
        },
        {
          "attributes": {},
          "insert": "Dr.  \n"
        },
        {
          "attributes": {
            "bold": true
          },
          "insert": "Phone: "
        },
        {
          "attributes": {},
          "insert": "5678798978"
        },
        {
          "attributes": {
            "bold": true
          },
          "insert": " Fax: "
        },
        {
          "attributes": {},
          "insert": "5678956787 \n\n"
        },
        {
          "attributes": {},
          "insert": "Our review of pharmacy activity and discussion\n       with the patient confirmed medication adherence issues identified below.\n        Please review our findings and suggested actions and provide appropriate feedback.\n \n"
        },
        {
          "attributes": {
            "bold": true
          },
          "insert": "Drug: "
        },
        {
          "attributes": {},
          "insert": "Phenytoin, James Smith\n      , CVS, . \n"
        },
        {
          "attributes": {
            "bold": true
          },
          "insert": "Physician letter (Recommended actions for you): "
        },
        {
          "attributes": {},
          "insert": "Please contact pharmacy and authorize refills if applicable.. \n"
        },
        {
          "attributes": {
            "bold": true
          },
          "insert": "Physician letter (Discussion with patient): "
        },
        {
          "attributes": {},
          "insert": "Drug and/or illness specific education was provided along with reminder on importance of taking medications as prescribed.. \n"
        },
        {
          "attributes": {
            "bold": true
          },
          "insert": "Physician letter (Identified Barrier): "
        },
        {
          "attributes": {},
          "insert": "Patient does not have support at home to assist with managing medication regimen. Adil.. \n"
        },
        {
          "attributes": {
            "bold": true
          },
          "insert": "Member letter (What we talked about?): "
        },
        {
          "attributes": {},
          "insert": "testing. \n"
        },
        {
          "attributes": {
            "bold": true
          },
          "insert": "Member letter (What I need to do?): "
        },
        {
          "attributes": {},
          "insert": "Identify a person you trust to help organize your medications and assist you in taking them on time. Test Adil. \n"
        },
        {
          "attributes": {
            "bold": true
          },
          "insert": "Other Issue: "
        },
        {
          "attributes": {},
          "insert": "Recommendations made by: "
        },
        {
          "attributes": {},
          "insert": "Adil, Pharmacist \n\n"
        },
        {
          "attributes": {},
          "insert": "List of patient current medications\n\n"
        },
        {
          "attributes": {
            "background": "#000000",
            "color": "#ffffff"
          },
          "insert": "RESPONSE REQUESTED WITHIN 2 BUSINESS DAYS "
        },
        {
          "attributes": {},
          "insert": "\n\nAccepted: [ ] The following suggested actions are accepted."
        },
        {
          "attributes": {},
          "insert": "\n\nDeclined: [ ] The following suggested actions are "
        },
        {
          "attributes": {
            "underline": true
          },
          "insert": "not"
        },
        {
          "attributes": {},
          "insert": " accepted."
        },
        {
          "attributes": {},
          "insert": "\n\nRationale for declining recommended actions:"
        },
        {
          "attributes": {
            "background": "#000000",
            "color": "#ffffff"
          },
          "insert": ""
        }
      ],
      "memberLetter": [
        {
          "attributes": {},
          "insert": "18/08/2019, 16:34:15\n\n"
        },
        {
          "attributes": {},
          "insert": "Mike Campell Barnhart,\n"
        },
        {
          "attributes": {},
          "insert": "3670 Reppert Coal Road,\n"
        },
        {
          "attributes": {},
          "insert": "undefined,\n"
        },
        {
          "attributes": {},
          "insert": "West Bloomfield, MI\n      , undefined\n"
        },
        {
          "attributes": {},
          "insert": "Member ID: 1000008\n\n"
        },
        {
          "attributes": {},
          "insert": "Dear Mike,\n\n"
        },
        {
          "attributes": {},
          "insert": "Thank you for talking with me on"
        },
        {
          "attributes": {},
          "insert": " Sun Aug 18 2019 "
        },
        {
          "attributes": {},
          "insert": "about your health and medications. Our medication adherence program helps you understand your medications and use them safely."
        },
        {
          "attributes": {},
          "insert": "\n\nThis letter includes a summary of our discussion and an action plan (Medication Action Plan) and medication list (Personal Medication List)."
        },
        {
          "attributes": {
            "bold": true
          },
          "insert": "The action plan has steps you should take to help you get the best results from your medications.  The medication list will help you keep track of your medications and how to use them the right way.\n\n"
        },
        {
          "attributes": {
            "list": true
          },
          "insert": "Have your action plan and medication list with you when you talk with your doctors, pharmacists, and other health care providers in your care team.\n"
        },
        {
          "attributes": {
            "list": true
          },
          "insert": "Ask your doctors, pharmacists, and other healthcare providers to update the action plan and medication list at every visit.\n"
        },
        {
          "attributes": {
            "list": true
          },
          "insert": "Take your medication list with you if you go to the hospital or emergency room.\n"
        },
        {
          "attributes": {
            "list": true
          },
          "insert": "Give a copy of the action plan and medication list to your family or caregivers.\n"
        },
        {
          "attributes": {},
          "insert": "\nDrug (Name and Strength) || What we talked about || What I need to do\n"
        },
        {
          "attributes": {
            "background": "#000000",
            "color": "#ffffff"
          },
          "insert": "\nOther general concerns\n"
        },
        {
          "attributes": {},
          "insert": "\nConcern || What we talked about || What I need to do\n\n"
        },
        {
          "attributes": {
            "background": "#000000",
            "color": "#ffffff"
          },
          "insert": "\nPersonal Medication List For Mike, DOB: Wed Mar 06 2002\n       as of Sun Aug 18 2019\n\n"
        },
        {
          "attributes": {},
          "insert": "This medication list was made for you after we talked.\n"
        },
        {
          "attributes": {
            "list": true
          },
          "insert": "Use blank rows to add new medications.\n     Then fill in the dates you started using them.\n"
        },
        {
          "attributes": {
            "list": true
          },
          "insert": "Cross out medications when you no longer use them.\n     Then write the date and why you stopped using them.\n"
        },
        {
          "attributes": {
            "list": true
          },
          "insert": "Ask your doctors, pharmacists,\n     and other healthcare providers in your care team to update this list at every visit.\n\n"
        },
        {
          "attributes": {},
          "insert": "If you go to the hospital or emergency room, take this list with you.  Share this with your family or caregivers too.\n\n"
        },
        {
          "attributes": {},
          "insert": "I have included your Personal Medication List (PML). It is important to keep a full list of your medications with you when discussing your healthcare with your doctor, pharmacist and other caregivers.\n\n"
        },
        {
          "attributes": {
            "background": "#000000",
            "color": "#ffffff"
          },
          "insert": "\nPersonal Medication List (PML) of Mike as of 18/08/2019, 16:34:15\n\n"
        },
        {
          "attributes": {
            "bold": true
          },
          "insert": "Sl., Drug Name/Strength, Dosage, Prescriber,\n     Pharmacy, Pharmacy Phone\n\n"
        },
        {
          "attributes": {},
          "insert": "The medication review is not intended to interfere with the care already provided by your physician(s).  We have also shared a summary of the medication review and discussion with your physician(s) to coordinate your healthcare needs.  If you have additional questions or need additional information, please call undefined at 5678798978 between the hours of patientInfo.physicianAvailableTime.\n\n"
        },
        {
          "attributes": {},
          "insert": "Sincerely,\n\n\n"
        },
        {
          "attributes": {},
          "insert": "Adil, Pharmascist\n"
        }
      ],
      "id": "5d5446aa26fb1133ca162020",
      "createdAt": "2019-08-14T17:36:42.232Z",
      "updatedAt": "2019-08-18T12:36:23.239Z",
      "medications": [
        {
          "id": "141fe5a0-beba-11e9-8b9c-df9684cee4ec",
          "ndc": "13395837549",
          "medicationName": "Phenytoin",
          "formulation": "Oral Tablet",
          "dosage": "250 MG",
          "daysSupply": 60,
          "lastFilled": "1990-01-01T00:00:00.000Z",
          "quantity": 30,
          "pharmacyName": "CVS",
          "pharmacyAddress": "10568 San Diego CA, 78657",
          "pharmacyNCPDPNumber": "10001008",
          "prescriberName": "James Smith",
          "prescriberNPINumber": "4.57E+11",
          "prescriberPhoneNumber": "2659151423",
          "prescriberFaxNumber": "7213763332",
          "noLongerTakingFlag": false,
          "deleted": true,
          "adherenceFlag": false,
          "adheranceByPatientFlag": false,
          "drugTherapyProblems": [
            "Untreated indication",
            " Drug-age conflict",
            " Adverse event",
            " L1 drug-drug interaction"
          ],
          "brand": "Humana",
          "provider": "CVS",
          "frequency": "q4h prn",
          "condition": "Headache",
          "refills": 10,
          "prescribedDate": "2000-01-01T00:00:00.000Z",
          "renewedDate": "2019-01-01T00:00:00.000Z",
          "memberId": "1000008",
          "caseId": "42228",
          "adheranceFlag": "0",
          "pharmacyPhoneNumber": "2659151423",
          "pharmacyFaxNumber": "2659151423",
          "clientId": "5ce3b0ab6176a04ece7b4292",
          "therapies": [
            "5d4ea951429c134cf28f6af8",
            "5d4ea9b3429c134cf28f6b09",
            "5d4ea9c0429c134cf28f6b14"
          ],
          "recommendation": {
            "recommendedBy": "5ce7bdfcc6cca95c8535160a",
            "recommendationDate": "2019-08-18T11:04:14.866Z",
            "recommendationStatements": {
              "prescriberletterRAFY": "Please contact pharmacy and authorize refills if applicable.",
              "prescriberletterDWP": "Drug and/or illness specific education was provided along with reminder on importance of taking medications as prescribed.",
              "prescriberletterAssessment": "Patient does not have support at home to assist with managing medication regimen. Adil.",
              "memberLetterWWTA": "testing",
              "memberLetterWINTD": "Identify a person you trust to help organize your medications and assist you in taking them on time. Test Adil"
            }
          }
        },
        {
          "id": "14216c40-beba-11e9-8b9c-df9684cee4ec",
          "ndc": "30993512228",
          "medicationName": "Atorvastatin/ezetimibe",
          "formulation": "Oral Tablet",
          "dosage": "50 MG",
          "daysSupply": 30,
          "lastFilled": "0110-01-01T00:00:00.000Z",
          "quantity": 30,
          "pharmacyName": "CVS",
          "pharmacyAddress": "10569 San Diego CA, 78657",
          "pharmacyNCPDPNumber": "10001008",
          "prescriberName": "Alexander Frank",
          "prescriberNPINumber": "2.34E+11",
          "prescriberPhoneNumber": "8962191640",
          "prescriberFaxNumber": "2457828609",
          "noLongerTakingFlag": false,
          "deleted": true,
          "adherenceFlag": false,
          "adheranceByPatientFlag": false,
          "drugTherapyProblems": [
            "High risk Medication",
            " Drug-age conflict",
            " Adverse event",
            " L1 drug-drug interaction"
          ],
          "brand": "Humana",
          "provider": "CVS",
          "frequency": "q4h prn",
          "condition": "Migraine",
          "refills": 10,
          "prescribedDate": "2000-01-01T00:00:00.000Z",
          "renewedDate": "2019-01-01T00:00:00.000Z",
          "memberId": "1000008",
          "caseId": "45442",
          "adheranceFlag": "0",
          "pharmacyPhoneNumber": "8962191640",
          "pharmacyFaxNumber": "8962191640",
          "clientId": "5ce3b0ab6176a04ece7b4292"
        },
        {
          "id": "14222f90-beba-11e9-8b9c-df9684cee4ec",
          "ndc": "64947189425",
          "medicationName": "Pregabalin",
          "formulation": "Oral Capsule",
          "dosage": "50 MG",
          "daysSupply": 60,
          "lastFilled": "1990-01-01T00:00:00.000Z",
          "quantity": 30,
          "pharmacyName": "CVS",
          "pharmacyAddress": "10570 San Diego CA, 78657",
          "pharmacyNCPDPNumber": "10001008",
          "prescriberName": "Mary Griffith",
          "prescriberNPINumber": "2.36E+11",
          "prescriberPhoneNumber": "9836288082",
          "prescriberFaxNumber": "7958220894",
          "noLongerTakingFlag": false,
          "deleted": true,
          "adherenceFlag": false,
          "adheranceByPatientFlag": false,
          "drugTherapyProblems": [
            "Contraindication",
            " Drug-age conflict",
            " Adverse event",
            " L1 drug-drug interaction"
          ],
          "brand": "Humana",
          "provider": "CVS",
          "frequency": "q4h prn",
          "condition": "Hypertension",
          "refills": 10,
          "prescribedDate": "2000-01-01T00:00:00.000Z",
          "renewedDate": "2019-01-01T00:00:00.000Z",
          "memberId": "1000008",
          "caseId": "43087",
          "adheranceFlag": "0",
          "pharmacyPhoneNumber": "9836288082",
          "pharmacyFaxNumber": "9836288082",
          "clientId": "5ce3b0ab6176a04ece7b4292"
        },
        {
          "id": "142319f0-beba-11e9-8b9c-df9684cee4ec",
          "ndc": "28549285651",
          "medicationName": "Pitavastatin",
          "formulation": "Oral Tablet",
          "dosage": "50 MG",
          "daysSupply": 30,
          "lastFilled": "0110-01-01T00:00:00.000Z",
          "quantity": 30,
          "pharmacyName": "CVS",
          "pharmacyAddress": "10571 San Diego CA, 78657",
          "pharmacyNCPDPNumber": "10001008",
          "prescriberName": "Alexander Frank",
          "prescriberNPINumber": "2.34E+11",
          "prescriberPhoneNumber": "7060611855",
          "prescriberFaxNumber": "8542417941",
          "noLongerTakingFlag": false,
          "deleted": true,
          "adherenceFlag": false,
          "adheranceByPatientFlag": false,
          "drugTherapyProblems": [
            "Untreated indication",
            " Drug-age conflict",
            " Adverse event",
            " L1 drug-drug interaction"
          ],
          "brand": "Humana",
          "provider": "CVS",
          "frequency": "q4h prn",
          "condition": "Blood pressure",
          "refills": 10,
          "prescribedDate": "2000-01-01T00:00:00.000Z",
          "renewedDate": "2019-01-01T00:00:00.000Z",
          "memberId": "1000008",
          "caseId": "36260",
          "adheranceFlag": "0",
          "pharmacyPhoneNumber": "7060611855",
          "pharmacyFaxNumber": "7060611855",
          "clientId": "5ce3b0ab6176a04ece7b4292"
        },
        {
          "id": "14240450-beba-11e9-8b9c-df9684cee4ec",
          "ndc": "1128427888",
          "medicationName": "Enalapril",
          "formulation": "Oral Capsule",
          "dosage": "100 MG",
          "daysSupply": 60,
          "lastFilled": "1990-01-01T00:00:00.000Z",
          "quantity": 30,
          "pharmacyName": "CVS",
          "pharmacyAddress": "10572 San Diego CA, 78657",
          "pharmacyNCPDPNumber": "10001008",
          "prescriberName": "Mary Griffith",
          "prescriberNPINumber": "2.36E+11",
          "prescriberPhoneNumber": "8554983824",
          "prescriberFaxNumber": "5115749364",
          "noLongerTakingFlag": false,
          "deleted": true,
          "adherenceFlag": false,
          "adheranceByPatientFlag": false,
          "drugTherapyProblems": [
            "High risk Medication",
            " Drug-age conflict",
            " Adverse event",
            " L1 drug-drug interaction"
          ],
          "brand": "Humana",
          "provider": "CVS",
          "frequency": "q4h prn",
          "condition": "Headache",
          "refills": 10,
          "prescribedDate": "2000-01-01T00:00:00.000Z",
          "renewedDate": "2019-01-01T00:00:00.000Z",
          "memberId": "1000008",
          "caseId": "51393",
          "adheranceFlag": "0",
          "pharmacyPhoneNumber": "8554983824",
          "pharmacyFaxNumber": "8554983824",
          "clientId": "5ce3b0ab6176a04ece7b4292"
        },
        {
          "id": "1424eeb0-beba-11e9-8b9c-df9684cee4ec",
          "ndc": "1128427888",
          "medicationName": "Enalapril",
          "formulation": "Oral Capsule",
          "dosage": "100 MG",
          "daysSupply": 60,
          "lastFilled": "1990-01-01T00:00:00.000Z",
          "quantity": 30,
          "pharmacyName": "CVS",
          "pharmacyAddress": "10573 San Diego CA, 78657",
          "pharmacyNCPDPNumber": "10001008",
          "prescriberName": "Mary Griffith",
          "prescriberNPINumber": "2.36E+11",
          "prescriberPhoneNumber": "8554983824",
          "prescriberFaxNumber": "5115749364",
          "noLongerTakingFlag": false,
          "deleted": true,
          "adherenceFlag": false,
          "adheranceByPatientFlag": false,
          "drugTherapyProblems": [
            "Contraindication",
            " Drug-age conflict",
            " Adverse event",
            " L1 drug-drug interaction"
          ],
          "brand": "Humana",
          "provider": "CVS",
          "frequency": "q4h prn",
          "condition": "Migraine",
          "refills": 10,
          "prescribedDate": "2000-01-01T00:00:00.000Z",
          "renewedDate": "2019-01-01T00:00:00.000Z",
          "memberId": "1000008",
          "caseId": "42069",
          "adheranceFlag": "0",
          "pharmacyPhoneNumber": "8554983824",
          "pharmacyFaxNumber": "8554983824",
          "clientId": "5ce3b0ab6176a04ece7b4292"
        }
      ],
      "patientInfo": {
        "firstName": "Mike",
        "lastName": "Barnhart",
        "middleName": "Campell",
        "patientCode": "AD8",
        "gender": "Male",
        "addresses": [
          {
            "addressLine1StreetName": "3670 Reppert Coal Road",
            "city": "West Bloomfield",
            "state": "MI",
            "zipCode": "960413"
          },
          {
            "addressLine1StreetName": "",
            "city": "",
            "state": "",
            "zipCode": ""
          }
        ],
        "primaryAddress": {
          "addressLine1StreetName": "3670 Reppert Coal Road",
          "city": "West Bloomfield",
          "state": "MI",
          "zipCode": "960413"
        },
        "secondaryAddress": {
          "addressLine1StreetName": "",
          "city": "",
          "state": "",
          "zipCode": ""
        },
        "primaryPhoneNumber": "3943317041",
        "patientId": "1000008",
        "caseInitiationDate": "2018-05-15T00:00:00.000Z",
        "caseExpirationDate": "2018-05-29T00:00:00.000Z",
        "contractName": "ABC Healthplan",
        "caseId": "22570987032",
        "memberId": "1000008",
        "contractId": "10000008",
        "clientId": "5ce3b0ab6176a04ece7b4292",
        "dob": "2002-03-06T00:00:00.000Z",
        "primaryCarePhysicianNPINumber": "123456",
        "primaryCarePhysicianPhoneNumber": "5678798978",
        "primaryCarePhysicianFaxNumber": "5678956787",
        "primaryCarePhysicianAddress": "124 Main St, San Diego, CA, 98675",
        "id": "5d54469426fb1133ca162014",
        "createdAt": "2019-08-14T17:36:20.592Z",
        "updatedAt": "2019-08-14T17:36:20.594Z",
        "language": "English"
      }
    };
    const configData = [
      {
        "name": "Assessment",
        "clientId": "5ce3b0ab6176a04ece7b4292",
        "type": "assesment",
        "reportingProperties": [
          {
            "propertyName": "memberLetterWWTA",
            "description": "Member letter (What we talked about?)"
          },
          {
            "propertyName": "memberLetterWINTD",
            "description": "Member letter (What I need to do?)"
          },
          {
            "propertyName": "prescriberletterAssessment",
            "description": "Physician letter (Identified Barrier)"
          }
        ],
        "id": "5ce64dde11594b68f2d1f903",
        "createdAt": "2019-05-23T06:15:55.255Z",
        "updatedAt": "2019-08-10T08:26:12.348Z"
      },
      {
        "name": "Physician Actions",
        "clientId": "5ce3b0ab6176a04ece7b4292",
        "type": "physicianaction",
        "reportingProperties": [
          {
            "propertyName": "prescriberletterRAFY",
            "description": "Physician letter (Recommended actions for you)"
          }
        ],
        "id": "5d4e868f429c134cf28f6aaf",
        "createdAt": "2019-08-10T08:55:43.303Z",
        "updatedAt": "2019-08-10T08:55:43.303Z"
      },
      {
        "name": "Interventions",
        "clientId": "5ce3b0ab6176a04ece7b4292",
        "type": "intervention",
        "reportingProperties": [
          {
            "propertyName": "prescriberletterDWP",
            "description": "Physician letter (Discussion with patient)"
          }
        ],
        "id": "5d4e9996429c134cf28f6add",
        "createdAt": "2019-08-10T10:16:54.413Z",
        "updatedAt": "2019-08-10T10:16:54.413Z"
      },
      {
        "name": "Adherent Reasons",
        "clientId": "5ce3b0ab6176a04ece7b4292",
        "type": "intervention",
        "reportingProperties": [
          {
            "propertyName": "prescriberletterPATM",
            "description": "Physician letter (Member adhering to medication)"
          }
        ],
        "id": "5d4ea722429c134cf28f6aea",
        "createdAt": "2019-08-10T11:14:42.296Z",
        "updatedAt": "2019-08-10T11:21:57.129Z"
      }
    ];
    const configs = [];
    configData.map((cd) => cd.reportingProperties && configs.push(...cd.reportingProperties));
    function findPlaceHolder(recKey) {
      const index = configs.findIndex(
        config => config.propertyName === recKey
      );
      if (configs && index > -1) {
        return configs[index].description;
      } else {
        return recKey;
      }
    }
    caseRegistry.medications = caseRegistry.medications.map((medication) => {
      if (medication.recommendation && medication.recommendation.recommendationStatements) {
        medication.recommendations = [];
        for (const key in medication.recommendation.recommendationStatements) {
          if (medication.recommendation.recommendationStatements.hasOwnProperty(key)) {
            const element = medication.recommendation.recommendationStatements[key];
            medication.recommendations.push({
              propertyName: key, value: element, title: findPlaceHolder(key)
            })
          }
        }
      }
      return medication;
    });
    caseRegistry.overallRecommendationArray = [];
    caseRegistry.user = {
      firstName: 'Adil'
    }
    if (caseRegistry.overallRecommendations) {

      for (const key in caseRegistry.overallRecommendations) {
        if (caseRegistry.overallRecommendations.hasOwnProperty(key)) {
          const element = caseRegistry.overallRecommendations[key];
          caseRegistry.overallRecommendationArray.push({
            propertyName: key, value: element, title: findPlaceHolder(key)
          })
        }
      }

    }

    caseRegistry.currentDate = new Date().toDateString();
    caseRegistry.client = {
      name: 'XYZ'
    };

    const bwip = require('bwip-js');
    bwip.toBuffer({
      bcid: 'code128',       // Barcode type
      text: caseRegistry.patientInfo.memberId,    // Text to encode
      scale: 3,               // 3x scaling factor
      height: 10,              // Bar height, in millimeters
      includetext: true,            // Show human-readable text
      textxalign: 'center',        // Always good to set this
    }, async function (err, png) {
      if (err) {
        // Decide how to handle the error
        // `err` may be a string or Error object
      } else {

        caseRegistry.barcode = 'data:image/jpeg;base64, ' + png.toString('base64');
        const { toHTML, toPDF } = require('../utils/pdf-report');
        const prescriberHtml = await toHTML(__dirname + '/../views/prescriber-letter.ejs', caseRegistry);
        const memberHtml = await toHTML(__dirname + '/../views/member-letter.ejs', caseRegistry);
        var options = {
          format: 'A4', orientation: "portrait",
          "border": {
            "top": "1in",            // default is 0, units: mm, cm, in, px

            "bottom": "1in"
          }
        };
        var output = '../../pdf_.pdf'

        const prescriberPDF = await toPDF(prescriberHtml, options, output);
        const memberPDF = await toPDF(memberHtml, options, output);


        // res.setHeader('Content-Type', 'application/pdf');
        // res.setHeader('Content-Disposition', 'attachment; filename=prescriber-letter.pdf');
        // res.send(prescriberPDF);
        res.setHeader('Content-Type', 'application/pdf');
        res.setHeader('Content-Disposition', 'attachment; filename=member-letter.pdf');
        res.send(memberPDF);



        // res.render('prescriber-letter', caseRegistry);
        // console.log(caseRegistry)
        // res.render('member-letter', caseRegistry);
        // if we're using HTTP or another framework
        // res.end('<img src="' + imgsrc + '">');
        // `png` is a Buffer
        // png.length           : PNG file length
        // png.readUInt32BE(16) : PNG image width
        // png.readUInt32BE(20) : PNG image height
      }
    });




  });


  server.use(router);
};




