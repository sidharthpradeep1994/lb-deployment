module.exports = function(app) {
    app.remotes().phases
      .addBefore('invoke', 'options-from-request')
      .use(function(ctx, next) {
        if (!(ctx.args&&ctx.args.options&&ctx.args.options.accessToken)&&(!ctx.req.accessToken)) return next();
        const User = app.models.User;
        const accessToken = ctx.req.accessToken||ctx.args.options.accessToken.userId;
        User.findById(accessToken.userId , function(err, user) {
          if (err) return next(err);
          // ctx.args.currentUser = user;
          // ctx.req.params.currentUser = user;
          ctx.options.currentUser = user;
          next();
        });
      });
  };