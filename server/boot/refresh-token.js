module.exports = function (app) {

    const loopback = require('loopback')
    // ...
    app.use(loopback.token({/* config */ }));
    app.use(function (req, res, next) {
        var token = req.accessToken;
        if (!token) return next();

        var now = new Date();

        // performance optimization:
        // do not update the token more often than once per second
        if (now.getTime() - token.created.getTime() < 60000) return next();

        // update the token and save the changes
        req.accessToken.created = now;
        req.accessToken.ttl = 60 * 30; /* session timeout in seconds */
        req.accessToken.save(next);
    });
    // register other middleware, etc.
}