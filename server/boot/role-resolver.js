module.exports = function (app) {
  var Role = app.models.Role;
  const roles = require('../lib/models/roles');
  function isClientAdminModels(modelName) {
    const clientModels = ['patientCaseRegistry', 'patientMaster', 'client', 'clientConfig', 'category',
      'therapy', 'user'];
    return clientModels.findIndex(model => model === modelName) > -1;

  }


  function isClinicianModels(modelName) {
    const clientModels = ['patientCaseRegistry', 'patientMaster'];
    return clientModels.findIndex(model => model === modelName) > -1;

  }


  Role.registerResolver(roles.SUPERADMIN, function (role, context, cb) {

    //Q: Is the user logged in? (there will be an accessToken with an ID if so)
    var userId = context.accessToken.userId;
    if (!userId) {
      //A: No, user is NOT logged in: callback with FALSE
      return process.nextTick(() => cb(null, false));
    }
    app.models.user.findById(userId, (err, user) => {
      if (err) return cb(err);
      if (user.role == role)
        return cb(null, true);
      else
        return cb(null, false);
    })
  });


  Role.registerResolver(roles.WORKINGCLINICIAN, function (role, context, cb) {
    if (context.modelName !== 'patientCaseRegistry') {
      // A: No. This role is only for projects: callback with FALSE
      return process.nextTick(() => cb(null, false));
    }

    //Q: Is the user logged in? (there will be an accessToken with an ID if so)
    var userId = context.accessToken.userId;
    if (!userId) {
      //A: No, user is NOT logged in: callback with FALSE
      return process.nextTick(() => cb(null, false));
    }

    //find user and clientId of this particular user.
    app.models.user.findById(userId, (err, user) => {

      //if (!user.clientId || user.role !== role) {
      if (!user.clientId) {
        return cb(null, false);

      }

      if (context.modelId) {
        // Q: Is the current logged-in user associated with this Project?
        // Step 1: lookup the requested project and check if clientId matches
        context.model.findById(context.modelId, function (err, project) {
          // A: The datastore produced an error! Pass error to callback
          if (err) return cb(err);
          // A: There's no project by this ID! Pass error to callback
          if (!project) return cb(new Error("Project not found"));

          if (project.clientId == user.clientId && project.workingClinician == userId) {
            return cb(null, true);
          }
          else {
            return cb(null, false);
          }

        });
      }
      else {
        return cb(null, true);
      }

    });
  })

  Role.registerResolver(roles.CLINICIAN, function (role, context, cb) {

    // Q: Is the current request accessing a Project?
    if (!isClinicianModels(context.modelName)) {
      // A: No. This role is only for projects: callback with FALSE
      return process.nextTick(() => cb(null, false));
    }

    //Q: Is the user logged in? (there will be an accessToken with an ID if so)
    var userId = context.accessToken.userId;
    if (!userId) {
      //A: No, user is NOT logged in: callback with FALSE
      return process.nextTick(() => cb(null, false));
    }

    //find user and clientId of this particular user.
    app.models.user.findById(userId, (err, user) => {

      if (!user.clientId || user.role !== role) {
        return cb(null, false);
      }

      if (context.modelId) {
        // Q: Is the current logged-in user associated with this Project?
        // Step 1: lookup the requested project and check if clientId matches
        context.model.findById(context.modelId, function (err, project) {
          // A: The datastore produced an error! Pass error to callback
          if (err) return cb(err);
          // A: There's no project by this ID! Pass error to callback
          if (!project) return cb(new Error("Project not found"));

          if (project.clientId == user.clientId) {
            return cb(null, true);
          }
          else {
            return cb(null, false);
          }

        });
      }
      else {
        return cb(null, true);
      }

    });
  });


  Role.registerResolver(roles.CLIENTADMIN, function (role, context, cb) {

    // Q: Is the current request accessing a Project?
    if (!isClientAdminModels(context.modelName)) {
      // A: No. This role is only for projects: callback with FALSE
      return process.nextTick(() => cb(null, false));
    }

    //Q: Is the user logged in? (there will be an accessToken with an ID if so)
    var userId = context.accessToken.userId;
    if (!userId) {
      //A: No, user is NOT logged in: callback with FALSE
      return process.nextTick(() => cb(null, false));
    }

    //find user and clientId of this particular user.
    app.models.user.findById(userId, (err, user) => {

      if (!user.clientId || user.role !== role) {
        return cb(null, false);
      }

      if (context.modelId) {
        // Q: Is the current logged-in user associated with this Project?
        // Step 1: lookup the requested project and check if clientId matches
        context.model.findById(context.modelId, function (err, project) {
          // A: The datastore produced an error! Pass error to callback
          if (err) return cb(err);
          // A: There's no project by this ID! Pass error to callback
          if (!project) return cb(new Error("Project not found"));

          if (project.clientId == user.clientId) {
            return cb(null, true);
          }
          else {
            return cb(null, false);
          }

        });
      }
      else {
        return cb(null, true);
      }

    });

  });


  Role.registerResolver(roles.ADMIN, function (role, context, cb) {

    // Q: Is the current request accessing a Project?
    if (!isClientAdminModels(context.modelName)) {
      // A: No. This role is only for projects: callback with FALSE
      return process.nextTick(() => cb(null, false));
    }

    //Q: Is the user logged in? (there will be an accessToken with an ID if so)
    var userId = context.accessToken.userId;

    if (!userId) {
      //A: No, user is NOT logged in: callback with FALSE
      return process.nextTick(() => cb(null, false));
    }

    //find user and clientId of this particular user.
    app.models.user.findById(userId, (err, user) => {

      if (!user.clientId || user.role !== role) {
        return cb(null, false);
      }

      if (context.modelId) {
        // Q: Is the current logged-in user associated with this Project?
        // Step 1: lookup the requested project and check if clientId matches
        context.model.findById(context.modelId, function (err, project) {
          // A: The datastore produced an error! Pass error to callback
          if (err) return cb(err);
          // A: There's no project by this ID! Pass error to callback
          if (!project) return cb(new Error("Project not found"));

          if (project.clientId == user.clientId) {
            return cb(null, true);
          }
          else {
            return cb(null, false);
          }

        });
      }
      else {
        return cb(null, true);
      }

    });

  });


};
