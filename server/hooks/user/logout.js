module.exports = function (User) {


    User.afterRemote('logout', async (ctx) => {
        if (ctx && ctx.args && ctx.args.currentUser) {

            User.app.models.LoginLog.create({
                error: false, timestamp: Date.now(), action: 'logout', methodDesc: ctx.methodString, userId: ctx.args.currentUser.id
            });

        }


        return;
    });


}