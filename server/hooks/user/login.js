module.exports = function (User) {

    User.afterRemote('signin', async (ctx) => {
        if (ctx && ctx.result && ctx.result.userId && ctx.result.user) {
            const loginDate = new Date();
            const timeStamp = Date.now();
            if (ctx && ctx.args && ctx.result) {
                const identifierType = ctx.args.uniqueIdentifier && ctx.args.uniqueIdentifier === ctx.result.user.email ? 'email' : 'username';
                User.app.models.LoginLog.create({
                    userId: ctx.result.userId,
                    identifierType: identifierType, identifierValue: ctx.args.uniqueIdentifier, error: false, methodDesc: ctx.methodString, timestamp: timeStamp, clientId: ctx.result.user.clientId, action: 'login'
                });
            }
            User.findById(ctx.result.userId).then(user => user.updateAttribute('lastLogin', loginDate));
        }
        return;
    });


    User.afterRemoteError('signin', async (ctx) => {
        // console.log('signin')
        // console.log(ctx)

        if (ctx && ctx.args) {
            const identifierType = ctx.args.uniqueIdentifier && ctx.args.uniqueIdentifier.includes('@') ? 'email' : 'username';
            User.app.models.LoginLog.create({
                identifierType: identifierType, identifierValue: ctx.args.uniqueIdentifier, error: true, statusCode: ctx.error.statusCode, code: ctx.error.code, timestamp: Date.now(), action: 'login', methodDesc: ctx.methodString
            });
        }
        return;
    }
    )


    // User.afterRemote('login', function (ctx, result, next) {
    //     // the rest of the code
    //     console.log('login')
    //     console.log(ctx)
    //     next();
    // });



    // User.afterRemoteError('login', async (error) => {
    //     console.log('login')
    //     console.log(error)
    //     return;
    // });



}