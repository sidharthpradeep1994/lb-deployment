module.exports = function (User) {

    User.afterRemote('changePassword', function (ctx, result, next) {
        // the rest of the code
        if (ctx && ctx.args) {
            User.findById(ctx.args.id).then(user => user.updateAttribute('passwordResetAt', new Date()));
        }
        next();
    });


}