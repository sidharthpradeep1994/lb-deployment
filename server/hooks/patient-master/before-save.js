module.exports = function (patientMaster) {

    patientMaster.observe('before save', async (ctx, next) => {
        try {
            // Needs to check and update
            if (ctx.instance && !ctx.instance.id && !ctx.instance.patientCode) {


                const client = await patientMaster.app.models.client.findById(ctx.instance.clientId);
                if (!client) {
                    return next(new Error('Invalid client id'));
                }
                else {
                    const count = await patientMaster.count({ clientId: ctx.instance.clientId });
                    ctx.instance.patientCode = client.clientCode+ 'ML' + (count + 1);


                }


                return;
            }
        } catch (error) {
            throw error;
        }



    });
}