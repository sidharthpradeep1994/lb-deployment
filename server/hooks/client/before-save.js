module.exports = function (client) {

    client.observe('before save', (ctx, next) => {
        if ((ctx.instance && !ctx.instance.id) && !ctx.instance.clientId)
            client.count().then((count) => {
                ctx.instance.clientId = 'AD' + (count + 1);
                next();
            }).catch(err => next(err));

    });
}