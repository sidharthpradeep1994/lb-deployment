const sgMail = require('@sendgrid/mail');
const SENDGRID_KEY = require('../config').sendGridApiKey;
module.exports = function sendEmail(to, from, subject, text, html) {

    sgMail.setApiKey(SENDGRID_KEY);
    const msg = {
        to: to,
        from: from,
        subject: subject,
        text: text,
        html: html,
    };
    return sgMail.send(msg);
}