const bunyan = require('bunyan')

exports.loggerInstance = bunyan.createLogger({
    name: 'transaction-notifier',
    serializers: {
        req: bunyan.stdSerializers.req,
        res: bunyan.stdSerializers.res,
        err: bunyan.stdSerializers.err
    },
    level: 'info',
    levelFn: function (status, err /* only will work in error logger */, meta) {
        // return string of level
        if (meta["response-time"] > 30000) {
            return "fatal";
        } else {
            return "info";
        }
    },
    streams: [
        {
            path: './logs/adysas.log',
            period: '30d',   // daily rotation
            count: 30,        // keep 30 back copies
        },
        {
            level: 'error',
            period: '1d',   // daily rotation
            count: 30,        // keep 30 back copies
            path: './logs/adysas-error.log'  // log ERROR and above to a file
        },
        {
            stream: process.stdout
        }
    ]
});

exports.logResponse = function (id, body, statusCode) {
    var log = this.loggerInstance.child({
        id: id,
        body: body,
        statusCode: statusCode
    }, true)
    log.info('response')
}



// var log = bunyan.createLogger({name: "adysas-audit"});