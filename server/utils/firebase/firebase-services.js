const serviceAccount = require('../../config/firebase/foodq-b74aa-firebase-adminsdk-ks7nh-a1dd63f573.json');
const firebaseAdmin = require('firebase-admin');
module.exports = function () {
  const serviceAccount = require('../../config/firebase/foodq-b74aa-firebase-adminsdk-ks7nh-a1dd63f573.json');
  const firebaseAdmin = require('firebase-admin');
  firebaseAdmin.initializeApp({
    credential: firebaseAdmin.credential.cert(serviceAccount),
    databaseURL: 'https://foodq-b74aa.firebaseio.com'
  });
  return {
    firebaseAdmin: firebaseAdmin,
    auth: firebaseAdmin.auth()
  };
}
