module.exports = function sendEmail(toAddress, fromAddress, pdf, subject, content, ccs) {
  //var sendGridApikey = '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';
  var sendGridApikey = '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';
  var helper = require('sendgrid').mail;
  var fromEmail = new helper.Email(fromAddress);
  var toEmail = new helper.Email(toAddress);
  var subject = subject;
  var content = new helper.Content('text/plain', content);
  var files = new helper.Attachment();
  const personalisation = new helper.Personalization({

  });
  ccs.map(cc => {
    let ccMail = new helper.Email(cc);
    personalisation.addCc(ccMail);
  });
  personalisation.addTo(toEmail)
  var mail = new helper.Mail(fromEmail, subject, toEmail, content);
  mail.addPersonalization(personalisation);
  if (pdf) {
    var base64File = new Buffer(pdf).toString('base64');
    files.setContent(base64File);
    files.setType("application/pdf")
    files.setFilename(`${subject}.pdf`)
    files.setDisposition("attachment")
    mail.addAttachment(files)
  }
  const sendGrid = require('sendgrid')(sendGridApikey);

  var request = sendGrid.emptyRequest({
    method: 'POST',
    path: '/v3/mail/send',
    body: mail.toJSON(),
    attachment: files
  });
  sendGrid.API(request, (err, res) => {
    if (err)
      console.log(err)
    console.log(res)
  });
}
