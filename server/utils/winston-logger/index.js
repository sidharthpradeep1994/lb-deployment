const winston = require('winston');
const constants = require('../constants/constants')
require('winston-mongodb');
const logger = winston.createLogger({
    format: winston.format.combine(
        winston.format.timestamp(),
        winston.format.prettyPrint(),
    ),
    level: 'info',
    format: winston.format.json(),
    defaultMeta: { service: 'user-service' },
    transports: [
        //
        // - Write to all logs with level `info` and below to `combined.log` 
        // - Write all logs error (and below) to `error.log`.
        //
        new winston.transports.File({ filename: './logs/error.log', level: 'error' }),
        new winston.transports.File({ filename: './logs/combined.log' }),
        //         new winston.transports.MongoDB({ level: 'info',
        //         db: 'mongodb://loopbackuser01:p15tNl9EHVIC1c1Q@adysasdemo-shard-00-00-s7kmn.mongodb.net:27017,adysasdemo-shard-00-01-s7kmn.mongodb.net:27017,adysasdemo-shard-00-02-s7kmn.mongodb.net:27017/adysasdemo?ssl=true&replicaSet=AdySasDemo-shard-0&authSource=admin&retryWrites=true&w=majority',
        //         collection: 'logDump',
        // options:{ useUnifiedTopology: true }
        //         }),
        //         new winston.transports.MongoDB({ level: 'db-error',
        //         db: 'mongodb://loopbackuser01:p15tNl9EHVIC1c1Q@adysasdemo-shard-00-00-s7kmn.mongodb.net:27017,adysasdemo-shard-00-01-s7kmn.mongodb.net:27017,adysasdemo-shard-00-02-s7kmn.mongodb.net:27017/adysasdemo?ssl=true&replicaSet=AdySasDemo-shard-0&authSource=admin&retryWrites=true&w=majority',
        //         collection: 'errorLogDump',
        //         options:{ useUnifiedTopology: true }

        //         })
    ]
});

const dbLogger = winston.createLogger({
    format: winston.format.combine(
        winston.format.timestamp(),
        winston.format.prettyPrint(),
    ),
    level: 'info',
    format: winston.format.json(),
    defaultMeta: { service: 'user-service' },
    transports: [
        //
        // - Write to all logs with level `info` and below to `combined.log` 
        // - Write all logs error (and below) to `error.log`.
        //
        // new winston.transports.File({ filename: './logs/error.log', level: 'error' }),
        // new winston.transports.File({ filename: './logs/combined.log' }),
        new winston.transports.MongoDB({
            level: 'info',
            db:constants.mongoUrl,
           /*  db: 'mongodb://loopbackuser01:p15tNl9EHVIC1c1Q@adysasdemo-shard-00-00-s7kmn.mongodb.net:27017,adysasdemo-shard-00-01-s7kmn.mongodb.net:27017,adysasdemo-shard-00-02-s7kmn.mongodb.net:27017/adysasdemo?ssl=true&replicaSet=AdySasDemo-shard-0&authSource=admin&retryWrites=true&w=majority', */
            collection: 'logDump',
            options: { useUnifiedTopology: true }
        }),
        new winston.transports.MongoDB({
            level: 'error',
            db:constants.mongoUrl,
           /*  db: 'mongodb://loopbackuser01:p15tNl9EHVIC1c1Q@adysasdemo-shard-00-00-s7kmn.mongodb.net:27017,adysasdemo-shard-00-01-s7kmn.mongodb.net:27017,adysasdemo-shard-00-02-s7kmn.mongodb.net:27017/adysasdemo?ssl=true&replicaSet=AdySasDemo-shard-0&authSource=admin&retryWrites=true&w=majority', */
            collection: 'errorLogDump',
            options: { useUnifiedTopology: true }

        })
    ]
});



module.exports = { logger, dbLogger };