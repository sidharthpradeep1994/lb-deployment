'use strict';

module.exports = function AppError(message, httpStatus, notify) {
  Error.captureStackTrace(this, this.constructor);
  this.name = this.constructor.name;
  this.message = message;
  this.status = httpStatus;
  this.notify = notify | false;
};

require('util').inherits(module.exports, Error);
