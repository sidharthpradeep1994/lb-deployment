var fs = require('fs');
var ejs = require('ejs');

var pdf = require('html-pdf');

function toPDF(html, options, output) {
    return new Promise(function (resolve, reject) {
        pdf.create(html, options).toBuffer(function (error, response) {
            if (error) {
                reject(error);
            }
            else {
                resolve(response);
            }
        });
    });
}

function toHTML(ejsTemplateURL, data) {
    return new Promise(function (resolve, reject) {
        fs.readFile(ejsTemplateURL, 'utf8', function (error, response) {
            if (error) {
                reject(error);
            }
            else {
                var html = ejs.render(response, data);
                resolve(html);
            }
        });
    });
}

module.exports = {
    toHTML: toHTML,
    toPDF: toPDF
}