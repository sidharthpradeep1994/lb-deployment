
const crypto = require('crypto');
const generateRandomPassword = (
    length = 20,
    wishlist = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz~!@-#$"
) => {
    // length = 10;
    var result = '';
    var characters = wishlist;
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}

function createHash(str) {
    return crypto.createHash('md5').update(str).digest('hex');
}

module.exports = { generateRandomPassword, createHash };