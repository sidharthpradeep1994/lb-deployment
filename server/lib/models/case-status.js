const CaseStatus = {
    WORKINPROGRESS: 'inprogress',
    NEW: 'new',
    COMPLETED: 'completed',
    CANCELLED: 'cancelled'
};

module.exports = Object.freeze(CaseStatus);