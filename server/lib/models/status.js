module.exports = function (state, userId) {
  this.status = state.status;
  this.order = state.order;
  this.userId = userId;
  this.createdAt = new Date();
}
