const dispositionOptions = {
    CONNECTED: 'connected',
    VOICEMAIL: 'voicemail',
    NOANSWER: 'noanswer',
    INVALIDNUMBER: 'invalidnumber',
    WRONGNUMBER: 'wrongnumber',
    OPTOUT: 'optout',
    DECLINED: 'declined',
    UNAVAILABLE: 'unavailable',
    DISCONNECTED: 'disconnected',
    SCHEDULECALL: 'schedulecall',
    DISENROLLED: 'disenrolled',
    DISEASED: 'diseased',
    NOTINITIATED: 'notinitiated',
    discontinue: {
        connected: false,
        voicemail: false,
        noanswer: false,
        invalidnumber: true,
        wrongnumber: false,
        optout: true,
        declined: true,
        unavailable: false,
        disconnected: false,
        schedulecall: false,
        disenrolled: true,
        diseased: true
    }
};
module.exports = dispositionOptions;