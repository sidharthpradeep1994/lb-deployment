const roles = {
  SUPERADMIN: 'superAdmin',
  CLIENTADMIN: 'clientAdmin',
  ADMIN: 'admin',
  CLINICIAN: 'clinician',
  WORKINGCLINICIAN: 'workingClinician'
};
Object.freeze(roles);
module.exports = roles;
