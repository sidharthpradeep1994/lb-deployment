let validator = require('validation-engine');
let check = require('../helpers/type-checks'); 
validator.addValidator('equal', function (rule, field_name, data) {
  return new Promise(function (resolve, reject) {
    if (data[field_name] === data[rule.fieldToCompare]) return resolve();
    return reject(new validator.ValidatorException('error message', field_name, rule, data));
  });
});

validator.addValidator('dataType', (rule, fieldName, data) => {
  return new Promise((resolve, reject) => {
    let message = 'Unidentified type error';
    switch (data.type) {
      case 'string':
        if (!check.isString(typedata[fieldName]))
          message = "value is not a string";

        break;
      case 'number':
        if (!check.isNumber(typedata[fieldName]))
          message = "value is not a number";

        break;
      case 'date':
        if (!check.isDate(typedata[fieldName]))
          message = "value is not a date";

        break;
      case 'array':
        if (!check.isArray(typedata[fieldName]))
          message = "value is not a array";

        break;
      case 'object':
        if (!check.isObject(typedata[fieldName]))
          message = "value is not a object";

        break;
      case 'null':
        if (!check.isNull(typedata[fieldName]))
          message = "value is not null";

        break;
      case 'regex':
        if (!check.isRegExp(typedata[fieldName]))
          message = "value is not regex";

        break;
      case 'boolean':
        if (!check.isBoolean(typedata[fieldName]))
          message = "value is not boolean";

        break;
      case 'error':
        if (!check.isError(typedata[fieldName]))
          message = "value is not an error object";

        break;
      case 'function':
        if (!check.isFunction(typedata[fieldName]))
          message = "value is not a function";

        break;
      default:
        break;
    }
    return reject(new validator.ValidatorException('error message', field_name, rule, data));
  });
});
module.exports =  validator;
