/**
 * @param {string} inputString Input String in camel case
 * @returns {string} outputString in dash case
 */
module.exports = function (inputString) {
  return inputString.replace(/\.?([A-Z]+)/g, "-$1").toLowerCase();
}
