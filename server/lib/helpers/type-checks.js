module.exports = {
  isString: function isString(value) {
    return typeof value === 'string' || value instanceof String;
  },
  isNumber: function isNumber(value) {
    return typeof value === 'number' && isFinite(value);
  },
  isArray: function isArray(value) {
    return value && typeof value === 'object' && value.constructor === Array;
  },
  isFunction: function isFunction(value) {
    return typeof value === 'function';
  },
  isObject: function isObject(value) {
    return value && typeof value === 'object' && value.constructor === Object;
  },
  isNull: function isObject(value) {
    return value && typeof value === 'object' && value.constructor === Object;
  },
  isUndefined: function isUndefined(value) {
    return typeof value === 'undefined';
  },
  isBoolean: function isBoolean(value) {
    return typeof value === 'boolean';
  },
  isRegExp: function isRegExp(value) {
    return value && typeof value === 'object' && value.constructor === RegExp;
  },
  isError: function isError(value) {
    return value instanceof Error && typeof value.message !== 'undefined';
  },
  isDate: function isDate(value) {
    return value instanceof Date;
  },
  isSymbol: function isSymbol(value) {
    return typeof value === 'symbol';
  }

}
