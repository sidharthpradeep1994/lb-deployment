module.exports = function addRole(role, userId, ModelObj, options) {
    return new Promise(async (resolve, reject) => {
        const Role = ModelObj.app.models.Role;
        const RoleMapping = ModelObj.app.models.RoleMapping;
        //create the admin role
        let roleData = await Role.findOne({
            where: { name: role }
        });
        if (!roleData) {
            roleData = await Role.create({ name: role }, options);
        }

        const existingRole = await roleData.principals.findOne({
            where: {
                and: [
                    { principalType: RoleMapping.USER },
                    { principalId: userId }
                ]
            }
        });
        if (existingRole) {
            existingRole.destroy(options).then((result) => {
                resolve();
            }).catch((err) => {
                reject(err);
            });
        } else {
            resolve();
        }

    });

}