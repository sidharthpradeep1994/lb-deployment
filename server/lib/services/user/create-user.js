const addRole = require('./add-role');
const roles = require('../../models/roles');
module.exports = function (UserModel) {
  return async function (password, cliId, clientCode, email, role,
    firstName, middleName, lastName, primaryAddress,
    secondaryAddress, primaryPhoneNumber, secondaryPhoneNumber, pharmacyLicenseNumber, gender, dob, ssn, photoId, languages, specialisations, options) {
    return await UserModel.count({
      clientId: cliId
    }).then((count) => UserModel.create({
      password: password,
      clientId: cliId,
      email: email,
      role: role,
      username: clientCode + (count+1),
      firstName, middleName, lastName, primaryAddress, languages, specialisations,
      secondaryAddress, primaryPhoneNumber, secondaryPhoneNumber, pharmacyLicenseNumber, gender, dob, ssn, photoId
    }, options)).then(async (user) => {
      await addRole(role, user.id, UserModel, options);
      return user;
    });
  }
}
