module.exports = function addRole(role, userId, ModelObj, options) {
    return new Promise(async (resolve, reject) => {
        const Role = ModelObj.app.models.Role;
        const RoleMapping = ModelObj.app.models.RoleMapping;
        //create the admin role
        let roleData = await Role.findOne({
            where: { name: role }
        });
        if (!roleData) {
            roleData = await Role.create({ name: role }, options);
        }
        //make bob an admin
        roleData.principals.create({
            principalType: RoleMapping.USER,
            principalId: userId
        },options, function (err, principal) {
            if (err) reject(err);
            else resolve(role);
        });

    });

}