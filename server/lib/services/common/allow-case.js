module.exports = function (caseRegistry, user, accessLevel) {

    const typeCheck = require('../../helpers/type-checks');
    let allowAction = true;
    // if (user.role === roles.SUPERADMIN) {
    //     return allowAction;
    // }

    // allowAction = user && caseRegistry && (user.clientId === caseRegistry.clientId) &&
    //     (user.role === roles.CLIENTADMIN ||
    //         user.role === roles.CLINICIAN ||
    //         user.role === roles.SUPERADMIN);
    // const isCompleted = caseRegistry.status === status.COMPLETED;
    // if (user.role !== roles.SUPERADMIN) {
    //     if (caseRegistry.handledBy) {
    //         allowAction = allowAction && caseRegistry.handledBy === user.id;
    //     }
    //     allowAction = allowAction && caseRegistry.status !== status.COMPLETED;
    // }
    // if (user.role === roles.CLINICIAN) {
    //     allowAction = allowAction &&
    //         caseRegistry.assignedUsers &&
    //         typeCheck.isArray(caseRegistry.assignedUsers) && (caseRegistry.assignedUsers.findIndex((id) => id === user.id) > -1);
    // }
    if (!typeCheck.isUndefined(caseRegistry) && !typeCheck.isNull(caseRegistry) && !typeCheck.isUndefined(user)) {
        return authorizeAccessType(accessLevel, user, caseRegistry);
    } else {
        return { access: false, role: false };
    }


}

const roles = require('../../models/roles');
const status = require('../../models/case-status');

function authorizeAccessType(accessLevel, user, caseRegistry) {
    let allowAccess = true;
    let role = null;
    let message = 'Forbidden Access';
    if (user.role === roles.SUPERADMIN) {
        return { access: allowAccess, role: roles.SUPERADMIN, message: message };
    }
    switch (accessLevel) {
        case 'view':
            if (user.role === roles.CLIENTADMIN) {

                allowAccess = allowAccess && user.clientId === caseRegistry.clientId;
                role = roles.CLIENTADMIN;
            }
            else if (user.role === roles.CLINICIAN) {
                allowAccess = allowAccess && user && caseRegistry.assignedUsers.findIndex((id) => id == user.id) > -1;
                role = roles.CLINICIAN;
                message = "This case is not assigned to you";
            }
            else {
                allowAccess = false;
            }


            break;
        case 'write':
            if (user.role === roles.CLIENTADMIN) {
                allowAccess = allowAccess && user.clientId === caseRegistry.clientId &&
                    caseRegistry.status !== status.COMPLETED;
                role = roles.CLIENTADMIN;
                message = "This case is currently worked by another user";
            }
            else if (user.role === roles.CLINICIAN) {
                allowAccess = allowAccess && user && caseRegistry.assignedUsers.findIndex((id) => id == user.id) > -1;
                role = roles.CLINICIAN;
                message = "This case is not assigned to you";
            }
            else {
                allowAccess = false;
            }

            if (caseRegistry.handledBy) {

                allowAccess = allowAccess && caseRegistry.handledBy == user.id;
                role = roles.WORKINGCLINICIAN
                message = "This case is currently handled by another user";
            }
            break;
        default:
            break;
    }
    return { access: allowAccess, role: role, message: message };
}