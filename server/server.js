'use strict';

var loopback = require('loopback');
var boot = require('loopback-boot');

var app = module.exports = loopback();


const dotenv = require('dotenv');
dotenv.config();

const morgan = require('morgan');
// const logger = require('./utils/logger')

const winstonLogger = require('./utils/winston-logger').logger;
const dbLogger = require('./utils/winston-logger').dbLogger;

const addRequestId = require('express-request-id')();
const deepRegexreplace = require('./utils/deep-regex-replace');

app.use(addRequestId);

const loggerFormat = ':id [:date[web]] ":method :url" :status :response-time';

morgan.token('id', function getId(req) {
  return req.id
});


const excludedKeys = ['password', 'Authorization'];
// app.use(morgan(loggerFormat, {
//   skip: function (req, res) {
//     return res.statusCode < 400
//   },
//   stream: process.stderr
// }));


app.use(morgan(loggerFormat, {
  // skip: function (req, res) {
  //   return res.statusCode < 400
  // },
  stream: process.stderr
}));



// app.use(morgan(loggerFormat, {
//   skip: function (req, res) {
//     return res.statusCode >= 400
//   },
//   stream: process.stdout
// }));

const bodyParser = require('body-parser');
app.use(bodyParser.json());

app.use((req, res, next) => {
  // var log = logger.loggerInstance.child({
  //   id: req.id,
  //   body: req.body,
  //   url: req.url,                                                                                                                                                                                                         
  //   query: req.query,
  //   params: req.params,
  //   headers: req.headers,
  //   remoteAddress: req.remoteAddress,
  //   res: res.body
  // }, true)
  // console.log(req)
  // winstonLogger.log('info',deepRegexreplace({
  //     id: req.id,
  //     body: req.body,
  //     url: req.url,                                                                                                                                                                                                         
  //     query: req.query,
  //     params: req.params,
  //     headers: req.headers,
  //     remoteAddress: req.remoteAddress,
  //     res: res.body
  //   },excludedKeys));

  next();
});

app.use(function (req, res, next) {

  function afterResponse() {
    res.removeListener('finish', afterResponse);
    res.removeListener('close', afterResponse);
    // var log = logger.loggerInstance.child({
    //   id: req.id
    // }, true)

    // log.info({ res: res }, 'response')
    if (res.statusCode > 399) {
      winstonLogger.log('error', deepRegexreplace({
        id: req.id,
        body: req.body,
        url: req.url,
        query: req.query,
        params: req.params,
        headers: req.headers,
        remoteAddress: req.remoteAddress,
        responseBody: res.body,
        statusCode: res.statusCode,
        statusMessage: res.statusMessage,
        error: getError(res)
      }, ['Authorization']));

      dbLogger.log('error', JSON.stringify(deepRegexreplace({
        id: req.id,
        body: req.body,
        url: req.url,
        query: req.query,
        params: req.params,
        headers: req.headers,
        remoteAddress: req.remoteAddress,
        responseBody: res.body,
        statusCode: res.statusCode,
        statusMessage: res.statusMessage,
        error: getError(res)
      }, ['Authorization'])));
    }
    else {
      winstonLogger.log('info', deepRegexreplace({
        id: req.id,
        body: req.body,
        url: req.url,
        query: req.query,
        params: req.params,
        headers: req.headers,
        remoteAddress: req.remoteAddress,
        statusCode: res.statusCode,
        statusMessage: res.statusMessage,
        responseBody: getResponseresult(res)
      }, excludedKeys));
      dbLogger.log('info', JSON.stringify(deepRegexreplace({
        id: req.id,
        body: req.body,
        url: req.url,
        query: req.query,
        params: req.params,
        headers: req.headers,
        remoteAddress: req.remoteAddress,
        statusCode: res.statusCode,
        statusMessage: res.statusMessage,
        responseBody: getResponseresult(res)
      }, excludedKeys)));
    }
  }

  res.on('finish', afterResponse);
  res.on('close', afterResponse);
  next();


  function getResponseresult(res) {
    try {
      return res.req.remotingContext.result;
    }
    catch (ex) {
      return null;
    }
  }

  function getError(res) {
    try {
      return res.req.remotingContext.error;
    }
    catch (ex) {
      return null;
    }
  }
});



var path = require('path');
app.set('view engine', 'ejs'); // LoopBack comes with EJS out-of-box
app.set('json spaces', 2); // format json responses for easier viewing

// must be set to serve views properly when starting the app via `slc run` from
// the project root
app.set('views', path.resolve(__dirname, 'views'));


app.start = function () {
  // start the web server
  return app.listen(function () {
    app.emit('started');
    var baseUrl = app.get('url').replace(/\/$/, '');
    console.log('Web server listening at: %s', baseUrl);
    if (app.get('loopback-component-explorer')) {
      var explorerPath = app.get('loopback-component-explorer').mountPath;
      console.log('Browse your REST API at %s%s', baseUrl, explorerPath);
    }
  });
};

// Bootstrap the application, configure models, datasources and middleware.
// Sub-apps like REST API are mounted via boot scripts.
boot(app, __dirname, function (err) {
  if (err) throw err;

  // start the server if `$ node server.js`
  if (require.main === module)
    app.start();
});
