module.exports = {
  endpoint: process.env.API_URL,
  masterKey: process.env.API_KEY,
  sendGridApiKey: process.env.SEND_GRID_API_KEY,
  port: process.env.PORT
};