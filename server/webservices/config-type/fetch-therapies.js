module.exports = function (ConfigType) {
  ConfigType.fetchTherapies = async function (options, id) {
    try {
      const token = options && options.accessToken;
      const userId = token && token.userId;

      return await ConfigType.app.models.therapy.find({
        where: {
          and: [{ configId: id }]
        }, skip: 0, limit: 0, fields: [], order: ['index ASC', 'name ASC']
      });


    } catch (error) {
      throw error;
    }

  }

  ConfigType.remoteMethod('fetchTherapies', {
    accepts: [{
      arg: 'options',
      type: 'object',
      http: 'optionsFromRequest'
    }, {
      arg: 'id',
      type: 'string',
      required: true
    }], returns: {
      arg: 'data',
      type: 'object',
      root: true
    },
    http: {
      verb: 'get',
      path: '/:id/therapies'
    }
  });
}
