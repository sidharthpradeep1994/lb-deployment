module.exports = function (patientCaseRegistry) {
	patientCaseRegistry.fetchSummary = async function (options, registryId) {
		try {
			const token = options && options.accessToken;
			const userId = token && token.userId;
			const registry = await patientCaseRegistry.findById(registryId);
			const config = await patientCaseRegistry.app.models.configType.findOne({
				where: { clientId: patientCaseRegistry.clientId }
			});
			const therapiesAvailable = await patientCaseRegistry.app.models.therapy.find({
				where: {
					clientId: patientCaseRegistry.clientId
				}
			});
			const patient = await patientCaseRegistry.app.models.patientMaster.findOne({ where: { id: registry.patientUid } });
			return { patientRegistry: registry, patient: patient, config: config, therapiesAvailable: therapiesAvailable };

		} catch (error) {
			throw error;
		}

	}

	patientCaseRegistry.remoteMethod('fetchSummary', {
		accepts: [{
			arg: 'options',
			type: 'object',
			http: 'optionsFromRequest'
		}, {
			arg: 'registryId',
			type: 'string',
			required: true
		}], returns: {
			arg: 'data',
			type: 'object',
			root: true
		},
		http: {
			verb: 'get',
			path: '/:registryId/summary'
		}
	});
}
