module.exports = function (patientCaseRegistry) {
	const Roles = require('../../lib/models/roles');
	patientCaseRegistry.filterRegistries = async function (options, searchString, assignedTo, status, skip, limit, searchField, programId, accountId, from, to) {
		try {
			const token = options && options.accessToken;
			const userId = token && token.userId;
			const user = await patientCaseRegistry.app.models.user.findById(userId);
			if ((!user || !user.clientId) && user.role !== Roles.SUPERADMIN) {
				throw new Error('Invalid user');
			}
			const filter = {
				where: {
					and: [{
						clientId: user.clientId
					}]
				}, skip: skip, limit: limit
				// fields: ['firstName', 'middleName', 'lastName', 'primaryLanguage', 'contractName', '']
			};
			if (user.role === Roles.CLINICIAN) {
				filter.where.and.push({ assignedUsers: { inq: [user.id] } });
			}

			accountId && accountId.trim() !== '' && accountId !== 'undefined' && filter.where.and.push({ accountId: accountId });

			programId && programId.trim() !== '' && programId !== 'undefined' && filter.where.and.push({ programId: programId });

			from && from.trim() !== '' && from !== 'null' && filter.where.and.push({ createdAt: { gte: new Date(from) } });

			to && to.trim() !== '' && to !== 'null' && filter.where.and.push({ createdAt: { lte: new Date(to) } });

			status && status !== 'undefined' && filter.where.and.push({ status: status });

			searchString && searchString.trim() !== '' && searchString.trim() !== 'undefined' && searchField.trim() != 'undefined'
				&& (() => {
					const searchFilter = {};
					searchFilter[searchField] = { like: new RegExp(searchString.trim() + '$') };
					filter.where.and.push(searchFilter);

				})();
			const data = await patientCaseRegistry.find(filter);
			const count = await patientCaseRegistry.count(filter.where);
			return { data, count };


		} catch (error) {
			throw error;
		}

	}

	patientCaseRegistry.remoteMethod('filterRegistries', {
		accepts: [{
			arg: 'options',
			type: 'object',
			http: 'optionsFromRequest'
		}, {
			arg: 'searchString',
			type: 'string',
			description: 'text to search by phone number',
			required: false
		},
		{
			arg: 'assignedTo',
			type: 'string',
			description: 'assigned to',
			required: false
		},
		{
			arg: 'status',
			type: 'string',
			description: 'assigned to',
			required: false
		},
		{
			arg: 'skip',
			type: 'number',
			description: 'Number of records to skip',
			default: 0,
			required: false
		}, {
			arg: 'limit',
			type: 'number',
			description: 'Max number of records to return',
			default: 20,
			required: false
		},
		{
			arg: 'searchField',
			type: 'string',
			description: 'Search Field',
			default: '',
			required: false
		},
		{
			arg: 'programId',
			type: 'string',
			description: 'programId',
			// default: '',
			required: false
		},
		{
			arg: 'accountId',
			type: 'string',
			description: 'accountId',
			// default: '',
			required: false
		},
		{
			arg: 'from',
			type: 'string',
			description: 'created from',
			// default: '',
			required: false
		},
		{
			arg: 'to',
			type: 'string',
			description: 'created to',
			// default: '',
			required: false
		}], returns: {
			arg: 'data',
			type: 'object',
			root: true
		},
		http: {
			verb: 'get',
			path: '/filter'
		}
	});


	function doesExist(property) {
		return property && property.trim() != '' && property != 'undefined';
	}
}
