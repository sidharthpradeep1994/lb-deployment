module.exports = function (patientCaseRegistry) {
	patientCaseRegistry.updateOverallRecommendation = async function (options, caseId, overallRecommendations) {
		try {
			const { isArray } = require('../../lib/helpers/type-checks');
			const token = options && options.accessToken;
			const userId = token && token.userId;
			if (!isArray(overallRecommendations)) {
				throw new Error('Overall recommendations should be an array');
			}
			const patientCase = await patientCaseRegistry.findById(caseId);
	
			return await patientCase.updateAttribute('overallRecommendations', overallRecommendations, options);


		} catch (error) {
			throw error;
		}

	}

	patientCaseRegistry.remoteMethod('updateOverallRecommendation', {
		accepts: [{
			arg: 'options',
			type: 'object',
			http: 'optionsFromRequest'
		}, {
			arg: 'caseId',
			type: 'string',
			required: true
		}, {
			arg: 'overallRecommendations',
			type: 'array',
			required: true
		}], returns: {
			arg: 'data',
			type: 'object',
			root: true
		},
		http: {
			verb: 'post',
			path: '/:caseId/recommendations'
		}
	});
}
