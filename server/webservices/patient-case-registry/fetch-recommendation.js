module.exports = function (patientCaseRegistry) {
	patientCaseRegistry.fetchRecommendation = async function (options, caseId, medicineId) {
		try {
			const token = options && options.accessToken;
			const userId = token && token.userId;
			const patientCase = await patientCaseRegistry.findById(caseId);
			const medication = await patientCase.medicationRegistry.findById(medicineId);
			const configs = await patientCaseRegistry.app.models.reportingProperty.find({ where: { and: [{ clientId: patientCase.clientId }] }, skip: 0, limit: 0, fields: [], order: '' })
			// const configs = [];
			// configData.map((cg) => cg.reportingProperties && configs.push(...cg.reportingProperties))
			return { recommendation: medication.recommendation, caseId: caseId, configs: configs };

		} catch (error) {
			throw error;
		}

	}

	patientCaseRegistry.remoteMethod('fetchRecommendation', {
		accepts: [{
			arg: 'options',
			type: 'object',
			http: 'optionsFromRequest'
		}, {
			arg: 'caseId',
			type: 'string',
			required: true
		},
		{
			arg: 'medicineId',
			type: 'string',
			required: true
		}], returns: {
			arg: 'data',
			type: 'object',
			root: true
		},
		http: {
			verb: 'get',
			path: '/:caseId/recommendation/:medicineId'
		}
	});
}
