module.exports = function (patientCaseRegistry) {

    const Joi = require('joi');

    const schema = Joi.object().keys({
        caseId: Joi.string().required(),
        medicationId: Joi.string().required(),
        medication: Joi.object().keys({
            ndc: Joi.string().required(),
            medicationName: Joi.string().required(),
            formulation: Joi.string().required(),
            dosage: Joi.string().required(),
            daysSupply: Joi.string().required(),
            lastFilled: Joi.string().required(),
            quantity: Joi.required(),
            pharmacyName: Joi.string().required(),
            pharmacyAddress: Joi.string().required(),
            pharmacyPhone: Joi.string().required(),
            pharmacyFax: Joi.string().required(),
            pharmacyNCPDPNumber: Joi.string().required(),
            sig: Joi.string().required(),
            indication: Joi.string().required(),
            deleted: Joi.string().required()
        })
    });

    patientCaseRegistry.updateMedication = async function (options, caseId, medicationId, medication) {
        try {
            await schema.validate({
                caseId, medicationId, medication
            });
            const token = options && options.accessToken;
            const userId = token && token.userId;
            const patientCase = await patientCaseRegistry.findById(caseId);
            const medicationData = await patientCase.medicationRegistry.findById(medicationId)

            medicationData.ndc = medication.ndc;
            medicationData.medicationName = medication.medicationName;
            medicationData.formulation = medication.formulation;
            medicationData.dosage = medication.dosage;
            medicationData.daysSupply = daysSupply;
            medicationData.lastFilled = medication.lastFilled;
            medicationData.quantity = medication.quantity;
            medicationData.pharmacyName = medication.pharmacyName;
            medicationData.pharmacyAddress = medication.pharmacyAddress;
            medicationData.pharmacyPhone = medication.pharmacyPhone;
            medicationData.pharmacyFax = medication.pharmacyFax;
            medicationData.pharmacyNCPDPNumber = medication.pharmacyNCPDPNumber;
            medicationData.noLongerTakingFlag = true;
            medicationData.sig = medication.sig;
            medicationData.indication = medication.indication;
            medicationData.deleted = medication.deleted;
            return await medicationData.save(options);

        } catch (error) {
            throw error;
        }

    }

    patientCaseRegistry.remoteMethod('updateMedication', {
        accepts: [{
            arg: 'options',
            type: 'object',
            http: 'optionsFromRequest'
        }, {
            arg: 'caseId',
            type: 'string',
            required: true
        },
        {
            arg: 'medicationId',
            type: 'string',
            required: true
        },
        {
            arg: 'medication',
            type: 'object',
            required: true
        }], returns: {
            arg: 'data',
            type: 'object',
            root: true
        },
        http: {
            verb: 'put',
            path: '/:caseId/medication/:medicationId'
        }
    });
}
