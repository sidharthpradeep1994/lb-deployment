module.exports = function (patientCaseRegistry) {
	patientCaseRegistry.updateCaseNotes = async function (options, caseId, caseNotes) {
		try {
			const token = options && options.accessToken;
			const userId = token && token.userId;
			const caseRegistry = await patientCaseRegistry.findById(caseId);
			return await caseRegistry.updateAttribute('caseNotes', caseNotes, options);


		} catch (error) {
			throw error;
		}

	}

	patientCaseRegistry.remoteMethod('updateCaseNotes', {
		accepts: [{
			arg: 'options',
			type: 'object',
			http: 'optionsFromRequest'
		}, {
			arg: 'caseId',
			type: 'string',
			required: true
		}, {
			arg: 'caseNotes',
			type: 'string',
			required: true
		}], returns: {
			arg: 'data',
			type: 'object',
			root: true
		},
		http: {
			verb: 'post',
			path: '/:caseId/casenotes'
		}
	});
}
