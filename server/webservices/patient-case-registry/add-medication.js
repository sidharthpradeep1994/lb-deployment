module.exports = function (patientCaseRegistry) {
	const Joi = require('joi');

	const schema = Joi.object().keys({
		caseId: Joi.string().required(),
		medication: Joi.object().keys({
			ndc: Joi.string().required(),
			medicationName: Joi.string().required(),
			formulation: Joi.string().required(),
			dosage: Joi.string().required(),
			daysSupply: Joi.number().required(),
			lastFilled: Joi.date().required(),
			quantity: Joi.number().required(),
			prescriberName: Joi.string().required(),
			prescriberNPINumber: Joi.string().required(),
			prescriberPhoneNumber: Joi.string().required(),
			pharmacyName: Joi.string().required(),
			pharmacyAddress: Joi.string(),
			pharmacyPhone: Joi.string(),
			pharmacyFax: Joi.string(),
			pharmacyNCPDPNumber: Joi.string().required(),
			sig: Joi.string().required(),
			indication: Joi.string().required()
		})
	});
	patientCaseRegistry.addMedication = async function (options, caseId, medication) {
		try {
			// const valid = await schema.validate({ caseId: caseId, medication: medication });
			const token = options && options.accessToken;
			const userId = token && token.userId;
			const patientCase = await patientCaseRegistry.findById(caseId);
			return await patientCase.medicationRegistry.create({
				ndc: medication.ndc, medicationName: medication.medicationName,
				formulation: medication.formulation, dosage: medication.dosage,
				daysSupply: medication.daysSupply,
				lastFilled: medication.lastFilled,
				quantity: medication.quantity,
				prescriberName: medication.prescriberName,
				prescriberNPINumber: medication.prescriberNPINumber,
				prescriberPhoneNumber: medication.prescriberPhoneNumber,
				pharmacyName: medication.pharmacyName,
				pharmacyAddress: medication.pharmacyAddress,
				pharmacyPhone: medication.pharmacyPhone,
				pharmacyFax: medication.pharmacyFax,
				pharmacyNCPDPNumber: medication.pharmacyNCPDPNumber, noLongerTakingFlag: false,
				sig: medication.sig, indication: medication.indication, nonETL: true, updatedAt: new Date(), caseId: caseId
			}, options);
		} catch (error) {
			throw error;
		}

	}

	patientCaseRegistry.remoteMethod('addMedication', {
		accepts: [{
			arg: 'options',
			type: 'object',
			http: 'optionsFromRequest'
		}, {
			arg: 'caseId',
			type: 'string',
			required: true
		},
		{
			arg: 'medication',
			type: 'medication',
			description: 'medication object',
			required: true
		}], returns: {
			arg: 'data',
			type: 'object',
			root: true
		},
		http: {
			verb: 'post',
			path: '/:caseId/medication'
		}
	});
}
