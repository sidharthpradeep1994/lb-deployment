module.exports = function (patientCaseRegistry) {
	const statuses = require('../../lib/models/case-status');
	const dispositionOptions = require('../../lib/models/disposition-options');
	const allowOperation = require('../../lib/services/common/allow-case');
	const AppError = require('../../utils/app-error');
	const Joi = require('joi');

	const inputSchema = Joi.object().keys({
		registryId: Joi.string().required(),
		connectedTo: Joi.string().required(),
		callType: Joi.string().required(),
		dispositionOption: Joi.string(),
		notes: Joi.string().allow('').optional(),
		scheduleDate: Joi.date()
	});
	patientCaseRegistry.addCaseLog = async function (options, registryId,
		connectedTo, callType, dispositionOption, notes, scheduleDate) {
		try {

			const valid = await inputSchema.validate({
				registryId, connectedTo, callType, dispositionOption, notes, scheduleDate
			});
			const token = options && options.accessToken;
			const userId = token && token.userId;
			const user = await patientCaseRegistry.app.models.user.findById(userId);
			const patientReg = await patientCaseRegistry.findById(registryId);
			const operationAccess = allowOperation(patientReg, user, 'write');
			if (!operationAccess.access) {
				const error = new AppError(operationAccess.message, 403);
				throw error;
			}
			const statusData = mapDispositionToStatus(dispositionOption);
			await patientReg.caseLog.create({
				connectedTo: connectedTo, callType: callType, dispositionOption: statusData.dispositionOption, status: statusData.status,
				notes: notes, scheduleDate, attemptedOn: new Date(), attemptedBy: userId, createdAt: new Date(), username: user.username, updatedAt: new Date()
			}, options);
			return patientReg.updateAttributes({ status: statusData.status, handledBy: user.id, discontinued: statusData.discontinued || false, scheduleDate }, options);
		} catch (error) {
			throw error;
		}

	}


	function mapDispositionToStatus(dispositionOption) {
		const discontinued = dispositionOptions.discontinue[dispositionOption];
		if (dispositionOption === dispositionOptions.CONNECTED) {
			return {
				status: statuses.WORKINPROGRESS, dispositionOption: dispositionOptions.CONNECTED,
				discontinued: discontinued
			};
		}
		else {
			return { status: statuses.NEW, dispositionOption: dispositionOptions[dispositionOption.toUpperCase()], discontinued: discontinued }
		}

	}

	patientCaseRegistry.remoteMethod('addCaseLog', {
		description: 'Add case log for a patient case',
		accepts: [{
			arg: 'options',
			type: 'object',
			http: 'optionsFromRequest'
		}, {
			arg: 'registryId',
			type: 'string',
			description: 'patientCaseId',
			required: true
		},
		{
			arg: 'connectedTo',
			type: 'string',
			required: true
		},
		{
			arg: 'callType',
			type: 'string',
			required: true
		},
		{
			arg: 'dispositionOption',
			type: 'string',
			required: true
		},
		{
			arg: 'notes',
			type: 'string',
			required: false
		},
		{
			arg: 'scheduleDate',
			type: 'date',
			required: false
		}], returns: {
			arg: 'data',
			type: 'object',
			root: true
		},
		http: {
			verb: 'post',
			path: '/:registryId/case-logs'
		}
	});



}
