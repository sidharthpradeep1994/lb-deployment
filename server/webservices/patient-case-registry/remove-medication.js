module.exports = function (patientCaseRegistry) {
	patientCaseRegistry.removeMedication = async function (options, caseId, medicineId, noLongerTakingFlag, noLongerTakingReason) {
		try {
			const token = options && options.accessToken;
			const userId = token && token.userId;
			const patientCase = await patientCaseRegistry.findById(caseId);
			const medication = await patientCase.medicationRegistry.findById(medicineId);
			medication.noLongerTakingFlag = noLongerTakingFlag;
			medication.noLongerTakingReason = noLongerTakingReason;
			return await medication.save();
		} catch (error) {
			throw error;
		}

	}

	patientCaseRegistry.remoteMethod('removeMedication', {
		accepts: [{
			arg: 'options',
			type: 'object',
			http: 'optionsFromRequest'
		}, {
			arg: 'caseId',
			type: 'string',
			required: true
		},
		{
			arg: 'medicineId',
			type: 'string',
			required: true
		},
		{
			arg: 'noLongerTakingFlag',
			type: 'boolean',
			required: true
		},
		{
			arg: 'noLongerTakingReason',
			type: 'string',
			required: true
		}], returns: {
			arg: 'data',
			type: 'object',
			root: true
		},
		http: {
			verb: 'patch',
			path: '/:caseId/medication/:medicineId/discontinue'
		}
	});
}
