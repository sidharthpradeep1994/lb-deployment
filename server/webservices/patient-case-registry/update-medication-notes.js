module.exports = function (patientCaseRegistry) {
	const Joi = require('joi');

	const schema = Joi.object().keys({
		caseId: Joi.string().required(),
		medicationId: Joi.string().required(),
		medication: Joi.object()
	});

	patientCaseRegistry.updateMedicationNotes = async function (options, caseId, medicationId, medication) {
		try {
			await schema.validate({
				caseId, medicationId, medication
			});
			const token = options && options.accessToken;
			const userId = token && token.userId;
			const patientCase = await patientCaseRegistry.findById(caseId);

			const medicationData = await patientCase.medicationRegistry.findById(medicationId)


			medicationData.sig = medication.sig;
			medicationData.indication = medication.indication;
			medicationData.deleted = medication.deleted;
			medicationData.notes = medication.notes;
			medicationData.noLongerTakingFlag = medication.noLongerTakingFlag;
			return await medicationData.save(options);

		} catch (error) {
			throw error;
		}

	}


	patientCaseRegistry.remoteMethod('updateMedicationNotes', {
		accepts: [{
			arg: 'options',
			type: 'object',
			http: 'optionsFromRequest'
		}, {
			arg: 'caseId',
			type: 'string',
			required: true
		},
		{
			arg: 'medicationId',
			type: 'string',
			required: true
		},
		{
			arg: 'medication',
			type: 'object',
			required: true
		}], returns: {
			arg: 'data',
			type: 'object',
			root: true
		},
		http: {
			verb: 'patch',
			path: '/:caseId/medication-notes/:medicationId'
		}
	});
}
