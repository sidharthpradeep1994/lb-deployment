module.exports = function (patientCaseRegistry) {
	const roles = require('../../lib/models/roles');
	const statuses = require('../../lib/models/case-status');
	const dispositionOptions = require('../../lib/models/disposition-options');
	const AppError = require('../../utils/app-error');
	patientCaseRegistry.suspendCase = async function (options, registryId) {
		try {
			const token = options && options.accessToken;
			const userId = token && token.userId;
			const user = await patientCaseRegistry.app.models.user.findById(userId);
			const patientCase = await patientCaseRegistry.findById(registryId);
			if (user.role === roles.CLINICIAN && user.clientId != patientCase.clientId) {
				throw new AppError('Not authorized', 500);
			}
			return await patientCase.updateAttribute('handledBy', null, options);

		} catch (error) {
			throw error;
		}

	}

	patientCaseRegistry.remoteMethod('suspendCase', {
		accepts: [{
			arg: 'options',
			type: 'object',
			http: 'optionsFromRequest'
		}, {
			arg: 'registryId',
			type: 'string',
			description: 'patientCaseId',
			required: true
		}], returns: {
			arg: 'data',
			type: 'object',
			root: true
		},
		http: {
			verb: 'get',
			path: '/suspend/:registryId'
		}
	});
}
