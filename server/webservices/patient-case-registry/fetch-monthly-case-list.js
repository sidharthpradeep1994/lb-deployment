module.exports = function (PatientCaseRegistry) {
	PatientCaseRegistry.fetchMonthlyCaseList = function (options, status, from, to, callback) {

		const token = options && options.accessToken;
		const userId = token && token.userId;
		PatientCaseRegistry.getDataSource()
			.connector.collection(
				PatientCaseRegistry.modelName
			).
			aggregate(
				[{
					$match: {
						$and: [{
							status: status
						},
							// {
							// 	createdAt: {
							// 		$gte: from
							// 	}
							// },
							// {
							// 	createdAt: {
							// 		$lte: to
							// 	}
							// }
						]
					}
				},
				{
					$group: {
						_id: {
							year: { $year: "$createdAt" },
							month: { $month: "$createdAt" },
							day: { $dayOfMonth: "$createdAt" }
						},
						count: { $sum: 1 }
					}
				}
				], (err, caseData) => {
					//if (err) callback(err);
					//else callback(null, caseData); /*commented this temporary. error:Callback is not function */
				});



	}

	PatientCaseRegistry.remoteMethod('fetchMonthlyCaseList', {
		accepts: [{
			arg: 'options',
			type: 'object',
			http: 'optionsFromRequest'
		}, {
			arg: 'status',
			type: 'string',
			required: true
		},
		{
			arg: 'from',
			type: 'date',
			required: true
		},
		{
			arg: 'to',
			type: 'date',
			required: true
		}
		], returns: {
			arg: 'data',
			type: 'object',
			root: true
		},
		http: {
			verb: 'get',
			path: '/:status/case-count/group-day'
		}
	});
}
