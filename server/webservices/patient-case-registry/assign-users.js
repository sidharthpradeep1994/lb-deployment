module.exports = function(patientCaseRegistry) {
  const roles = require('../../lib/models/roles');
  const AppError = require('../../utils/app-error');
  const Joi = require('joi');

  const schema = Joi.object().keys({
    caseId: Joi.string().required(),
    assignedUsers: Joi.array().items(Joi.string())
  });
  patientCaseRegistry.assignUsers = async function(
    options,
    caseId,
    assignedUsers
  ) {
    try {
      //We have to fix this later
      //const valid = await schema.validate(assignedUsers);
      const token = options && options.accessToken;
      const userId = token && token.userId;
      const userData = await patientCaseRegistry.app.models.user.findById(
        userId
      );
      if (
        userData.role !== roles.SUPERADMIN &&
        userData.role !== roles.CLIENTADMIN
      ) {
        throw new AppError('Unauthorized', 401);
      }

      const patientCase = await patientCaseRegistry.findById(caseId);
      if (
        userData.role === roles.CLIENTADMIN &&
        patientCase.clientId !== userData.clientId
      ) {
        throw new AppError('Unauthorized, 401');
      }
      return await patientCase.updateAttribute('assignedUsers', assignedUsers, options);
    } catch (error) {
      throw error;
    }
  };

  patientCaseRegistry.remoteMethod('assignUsers', {
    accepts: [
      {
        arg: 'options',
        type: 'object',
        http: 'optionsFromRequest'
      },
      {
        arg: 'caseId',
        type: 'string',
        required: true
      },
      {
        arg: 'assignedUsers',
        type: 'array',
        required: true
      }
    ],
    returns: {
      arg: 'data',
      type: 'object',
      root: true
    },
    http: {
      verb: 'post',
      path: '/:caseId/assign-users'
    }
  });
};
