module.exports = function (patientCaseRegistry) {
	patientCaseRegistry.updateRecommendation = async function (options, caseId, medicineId, recommendationStatements) {
		try {
			const token = options && options.accessToken;
			const userId = token && token.userId;
			const patientCase = await patientCaseRegistry.findById(caseId);
			const medication = await patientCase.medicationRegistry.findById(medicineId);
			medication.recommendation.recommendationStatements = recommendationStatements;
			medication.recommendation.recommendationDate = new Date();
			medication.recommendation.recommendedBy = userId;
			return await medication.save(options);
		} catch (error) {
			throw error;
		}

	}

	patientCaseRegistry.remoteMethod('updateRecommendation', {
		accepts: [{
			arg: 'options',
			type: 'object',
			http: 'optionsFromRequest'
		}, {
			arg: 'caseId',
			type: 'string',
			required: true
		},
		{
			arg: 'medicineId',
			type: 'string',
			required: true
		}, {
			arg: 'recommendationStatements',
			type: 'object',
			required: true
		}], returns: {
			arg: 'data',
			type: 'object',
			root: true
		},
		http: {
			verb: 'patch',
			path: '/:caseId/recommendation/:medicineId'
		}
	});
}
