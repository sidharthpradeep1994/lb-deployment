module.exports = function (patientCaseRegistry) {
	const Joi = require('joi');
	const fs = require('fs');
	const bwip = require('bwip-js');
	async function generatePdf(type, caseRegistry) {
		const { toHTML, toPDF } = require('../../utils/pdf-report');
		const html = type === 'prescriber' ? await toHTML(__dirname + '/../../views/prescriber-letter.ejs', caseRegistry) :
			await toHTML(__dirname + '/../../views/member-letter.ejs', caseRegistry);

		var options = {
			format: 'A4', orientation: "portrait",
			"border": {
				"top": "0.5in",            // default is 0, units: mm, cm, in, px

				"bottom": "0.5in"
			},
			// phantomPath: './node_modules/phantomjs-prebuilt/bin/phantomjs'
		};
		var output = '../../pdf_.pdf'

		const pdf = await toPDF(html, options, output);
		return pdf;

	}


	const schema = Joi.string().required();
	patientCaseRegistry.completePatientCase = async function (options, caseId) {
		try {
			await schema.validate(caseId);
			const token = options && options.accessToken;
			const userId = token && token.userId;
			const userData = await patientCaseRegistry.app.models.user.findById(userId);

			const patientCase = await patientCaseRegistry.findById(caseId);

			if (patientCase.clientId !== userData.clientId) {
				throw new Error('Unauthorized access');
			}

			savePdf(userData, patientCase, 'member', options);
			savePdf(userData, patientCase, 'prescriber', options);


			return await patientCase.updateAttribute('status', 'completed', options);


		} catch (error) {
			throw error;
		}

	}


	function findPlaceHolder(configs, recKey) {
		const index = configs.findIndex(
			config => config.propertyName === recKey
		);
		if (configs && index > -1) {
			return configs[index].description;
		} else {
			return recKey;
		}
	}


	function bwipToPromise(data) {
		return new Promise((resolve, reject) => {
			bwip.toBuffer({
				bcid: 'code128',       // Barcode type
				text: data,    // Text to encode
				scale: 3,               // 3x scaling factor
				height: 10,              // Bar height, in millimeters
				includetext: true,            // Show human-readable text
				textxalign: 'center',        // Always good to set this
			}, async function (err, png) {
				if (err) {
					// Decide how to handle the error
					// `err` may be a string or Error object
					reject(err);
				} else {
					resolve(png);
				}
			});
		});
	}


	async function savePdf(user, caseRegistry, letterType, options) {

		const clientData = await patientCaseRegistry.app.models.client.findById(user.clientId);


		caseRegistry.client = clientData;
		const configs = await patientCaseRegistry.app.models.reportingProperty
			.find({ where: { and: [{ clientId: user.clientId }, { programId: caseRegistry.programId }] }, skip: 0, limit: 0, fields: [], order: `priorityInLetter.${letterType}` });
		// const configs = [];
		// configData.map((cd) => cd.reportingProperties && configs.push(...cd.reportingProperties));
		caseRegistry.user = user;
		caseRegistry.patientInfo.dob = new Date(caseRegistry.patientInfo.dob).toDateString();
		caseRegistry.currentDate = new Date().toDateString();
		caseRegistry.configs = configs;
		caseRegistry.medications = caseRegistry.medications.map((medication) => {

			if (medication.recommendation && medication.recommendation.recommendationStatements) {
				medication.recommendations = [];
				for (const key in medication.recommendation.recommendationStatements) {
					if (medication.recommendation.recommendationStatements.hasOwnProperty(key)) {

						const element = medication.recommendation.recommendationStatements[key];
						const assignedLetters = configs.findIndex(cg => cg.propertyName === key) > -1 ? configs[configs.findIndex(cg => cg.propertyName === key)].assignedLetters || [] :
							[];
						medication.recommendations.push({
							propertyName: key, value: element, title: findPlaceHolder(configs, key), assignedLetters: assignedLetters
						});
						console.log(medication.recommendations)

					}
				}
			}
			return medication;
		});

		caseRegistry.overallRecommendationArray = [];

		if (caseRegistry.overallRecommendations) {

			for (const key in caseRegistry.overallRecommendations) {
				if (caseRegistry.overallRecommendations.hasOwnProperty(key)) {
					const element = caseRegistry.overallRecommendations[key];
					const assignedLetters = configs.findIndex(cg => cg.propertyName === key) > -1 ? configs[configs.findIndex(cg => cg.propertyName === key)].assignedLetters || [] :
						[];

					caseRegistry.overallRecommendationArray.push({
						propertyName: key, value: element, title: findPlaceHolder(configs, key), assignedLetters: assignedLetters
					})
				}
			}

		}

		const png = await bwipToPromise(caseRegistry.patientInfo.memberId);
		caseRegistry.barcode = 'data:image/jpeg;base64, ' + png.toString('base64');

		const pdf = await generatePdf(letterType, caseRegistry);


		const timeSTamp = Date.now();
		fs.writeFile(`${__dirname}/../../../pdfs/${timeSTamp}-${letterType}.pdf`, pdf, (err) => {
			if (err) console.log(err);
		});


		caseRegistry.letters.push({ id: require('uuid/v1')(), letterType: letterType, location: `/../../../pdfs/${timeSTamp}-${letterType}.pdf`, createdAt: new Date(), timeStamp: timeSTamp });

		caseRegistry.updateAttribute('letters', caseRegistry.letters, options);


	}




	patientCaseRegistry.remoteMethod('completePatientCase', {
		accepts: [{
			arg: 'options',
			type: 'object',
			http: 'optionsFromRequest'
		}, {
			arg: 'caseId',
			type: 'string',
			required: true
		}], returns: {
			arg: 'data',
			type: 'object',
			root: true
		},
		http: {
			verb: 'post',
			path: '/:caseId/complete'
		}
	});
}
