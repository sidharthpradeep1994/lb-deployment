module.exports = function (patientCaseRegistry) {
	patientCaseRegistry.resetMedication = async function (options, caseId, medicationId) {
		try {
			const token = options && options.accessToken;
			const userId = token && token.userId;
			const patientCase = await patientCaseRegistry.findById(caseId);
			const medication = await patientCase.medicationRegistry.findById(medicationId);
			medication.therapies = [];
			medication.recommendation = null;
			return await medication.save();

		} catch (error) {
			throw error;
		}

	}

	patientCaseRegistry.remoteMethod('resetMedication', {
		accepts: [{
			arg: 'options',
			type: 'object',
			http: 'optionsFromRequest'
		}, {
			arg: 'caseId',
			type: 'string',
			required: true
		},
		{
			arg: 'medicationId',
			type: 'string',
			required: true
		}], returns: {
			arg: 'data',
			type: 'object',
			root: true
		},
		http: {
			verb: 'get',
			path: '/:caseId/medication/:medicationId/reset'
		}
	});
}
