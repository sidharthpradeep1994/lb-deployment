module.exports = function (patientCaseRegistry) {
	patientCaseRegistry.fetchAllRecommendations = function (options, caseId) {
		try {
			const token = options && options.accessToken;
			const userId = token && token.userId;

			const caseRegistry = patientCaseRegistry.findById(caseId);
			const patientCase = await patientCaseRegistry.findById(caseId);
			const medicines = patientCaseRegistry.medicines.map((md) => md);

			medicines.map(async md => {
				if (md.therapies) {
					md.therapies = await patientCaseRegistry.app.models.therapy.find({ where: { id: { inq: md.therapies } } });
				}
				return md;
			});



		} catch (error) {
			throw error;
		}

	}

	patientCaseRegistry.remoteMethod('fetchAllRecommendations', {
		accepts: [{
			arg: 'options',
			type: 'object',
			http: 'optionsFromRequest'
		}, {
			arg: 'caseId',
			type: 'string',
			description: 'patient case id',
			required: true
		}], returns: {
			arg: 'data',
			type: 'object',
			root: true
		},
		http: {
			verb: 'post',
			path: '/:caseId/recommendations/all'
		}
	});
}
