module.exports = function (PatientCaseRegistry) {

    PatientCaseRegistry.caseStatistics = async function (options, from, to, searchField, searchString, fixedFilters, aggregator, aggregatorName) {
        try {
            const accessToken = options && options.accessToken;
            const userId = accessToken && accessToken.userId;
            const userData = await PatientCaseRegistry.app.models.user.findById(userId);

            let PatientCaseRegistryCollection = PatientCaseRegistry.getDataSource().connector.collection(
                PatientCaseRegistry.modelName
            );
            return new Promise((resolve, reject) => {
                const andConditions = [{
                    clientId: userData.clientId
                }];
                from && from.trim() !== '' && from !== 'null' && andConditions.push({ createdAt: { $gte: new Date(from) } });

                to && to.trim() !== '' && to !== 'null' && andConditions.push({ createdAt: { $lte: new Date(to) } });

                fixedFilters && fixedFilters.length && andConditions.conact(fixedFilters);
                searchString && searchString.trim() !== '' && searchString.trim() !== 'undefined' && searchField.trim() != 'undefined'
				&& (() => {
					const searchFilter = {};
					searchFilter[searchField] =  new RegExp(searchString.trim() + '$') ;
					andConditions.push(searchFilter);

				})();
                const filter = [];
                filter.push({
                    $match: {
                        $and: andConditions
                    }
                });


                filter.push({
                    $group: {
                        _id: `$${aggregator}`,
                        name: { $last: `$${aggregatorName}` },
                        count: {
                            $sum: 1
                        }
                    }
                });
                PatientCaseRegistryCollection.aggregate(
                    filter, function (err, aggregateResults) {
                        console.log(aggregateResults)
                        if (err) {
                            reject(err);
                        }
                        else resolve(aggregateResults);
                    });
            });
        }
        catch (error) {
            throw error;
        }



    }

    PatientCaseRegistry.remoteMethod('caseStatistics', {
        accepts: [
            { arg: 'options', type: 'object', http: 'optionsFromRequest' },
            { arg: 'from', type: 'string', required: false },
            { arg: 'to', type: 'string', required: false },
            {
                arg: 'searchField',
                type: 'string',
                description: 'Search Field',
                default: '',
                required: false
            },
            {
                arg: 'searchString',
                type: 'string',
                description: 'Search String',
                default: '',
                required: false
            },
            { arg: 'fixedFilters', type: 'array', required: false },
            { arg: 'aggregator', type: 'string', required: true },
            { arg: 'aggregatorName', type: 'string', required: true }
        ], returns: {
            arg: 'data', type: 'object', root: true
        },
        http: {
            verb: 'POST', path: '/statistics'
        }
    })

}