module.exports = function (patientCaseRegistry) {
	patientCaseRegistry.updateAllergy = async function (options, caseId, allergies) {
		const { isArray } = require('../../lib/helpers/type-checks');
		try {
			const token = options && options.accessToken;
			const userId = token && token.userId;
			const caseRegistry = await patientCaseRegistry.findById(caseId);
			if (!isArray(allergies)) {
				throw new Error('Invalid data for allergies');
			}
			caseRegistry.allergyInfo = allergies;
			return await caseRegistry.save(options)
		} catch (error) {
			throw error;
		}

	}

	patientCaseRegistry.remoteMethod('updateAllergy', {
		accepts: [{
			arg: 'options',
			type: 'object',
			http: 'optionsFromRequest'
		}, {
			arg: 'caseId',
			type: 'string',
			required: true
		},
		{
			arg: 'allergies',
			type: 'array',
			description: 'An array string of allergies ',
			required: true
		}], returns: {
			arg: 'data',
			type: 'object',
			root: true
		},
		http: {
			verb: 'post',
			path: '/:caseId/allergy'
		}
	});
}
