module.exports = function (patientCaseRegistry) {
	patientCaseRegistry.scheduledCases = async function (options, from, to) {
		try {
			const token = options && options.accessToken;
			const userId = token && token.userId;
			const user = await patientCaseRegistry.app.models.user.findById(userId);

			return await patientCaseRegistry.find({
				where: {
					and: [{ clientId: user.clientId }, { scheduleDate: { gt: new Date(from) } },
					{ scheduleDate: { lt: new Date(to) } }]
				}, skip: 0, limit: 0, fields: [], order: ''
			});


		} catch (error) {
			throw error;
		}

	}

	patientCaseRegistry.remoteMethod('scheduledCases', {
		accepts: [{
			arg: 'options',
			type: 'object',
			http: 'optionsFromRequest'
		}, {
			arg: 'from',
			type: 'date',
			required: true
		},
		{
			arg: 'to',
			type: 'date',
			required: true
		}], returns: {
			arg: 'data',
			type: 'object',
			root: true
		},
		http: {
			verb: 'get',
			path: '/schedules'
		}
	});
}
