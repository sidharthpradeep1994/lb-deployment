module.exports = function (PatientCaseRegistry) {

    const archiver = require('archiver');
    const fs = require('fs');
    const zip = require('express-zip');

    PatientCaseRegistry.downloadReports = async function (options, caseId, res) {
        try {

            const accessToken = options && options.accessToken;
            const userId = accessToken && accessToken.userId;

            const patientCase = await PatientCaseRegistry.findById(caseId);
            const timestamp = Date.now();
            const archivePath = `${__dirname}/../../../archives/${timestamp}.zip`;
            var output = fs.createWriteStream(`${archivePath}`);
            var archive = archiver('zip', {
                gzip: true,
                zlib: { level: 9 } // Sets the compression level.
            });

            archive.on('error', function (err) {
                throw err;
            });

            // pipe archive data to the output file
            archive.pipe(output);
            console.log(patientCase)
            patientCase.letters.map(letter => archive.file(`${__dirname}${letter.location}`, { name: `${letter.letterType}-${letter.timeStamp}.pdf` }));
            // const letters =[];
            // patientCase.letters.map(letter => letters.push({ path:`${__dirname}${letter.location}`, name: `${letter.letterType}-${letter.createdAt}.pdf` }));
            // res.zip(letters);
            // // append files
            // archive.file('/path/to/file0.txt', { name: 'file0-or-change-this-whatever.txt' });
            // archive.file('/path/to/README.md', { name: 'foobar.md' });

            //
            await archive.finalize();

            const data = await readFile(archivePath);
            fs.access(archivePath, fs.F_OK, (err) => {
                if (err) {
                    console.error(err)
                    return
                }
                console.log('exists')
                //file exists
            })
            console.log(data);
            res.setHeader('Content-Disposition', `attachment; filename=${timestamp}-letters.zip`);
            res.set('Content-Type', 'application/force-download');
            res.set('Content-Type', 'application/octet-stream');
            res.set('Content-Type', 'application/zip');
            res.set('Content-Disposition', `attachment; filename=${timestamp}-letters.zip`);
            res.set('Content-Transfer-Encoding', 'binary');
            res.send(data);




        }
        catch (err) {
            throw err;
        }



    }



    function readFile(path) {
        return new Promise((resolve, reject) => {
            fs.readFile(path, (err, data) => {
                if (err) reject(err);
                else resolve(data);
            });
        })
    }


    PatientCaseRegistry.remoteMethod('downloadReports', {
        accepts: [{ arg: 'options', type: 'object', http: 'optionsFromRequest' }, { arg: 'caseId', type: 'string', required: true },
        { arg: 'res', type: 'object', 'http': { source: 'res' } }],
        returns: { arg: 'data', type: 'object', root: true },
        http: { path: '/download/case-reports/:caseId', verb: 'post' }
    });


}