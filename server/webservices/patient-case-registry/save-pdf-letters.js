module.exports = function (patientCaseRegistry) {
	patientCaseRegistry.savePDFLetters = async function (options, caseId, caseRegistry) {
		try {
			const token = options && options.accessToken;
			const userId = token && token.userId;
			if (!userId) throw new Error('Unauthorized');
			const caseReg = await patientCaseRegistry.findById(caseId);
			return await caseReg.updateAttributes({
				physicianLetter: caseRegistry.physicianLetter,
				memberLetter: caseRegistry.memberLetter
			}, options);


		} catch (error) {
			throw error;
		}

	}

	patientCaseRegistry.remoteMethod('savePDFLetters', {
		accepts: [{
			arg: 'options',
			type: 'object',
			http: 'optionsFromRequest'
		}, {
			arg: 'caseId',
			type: 'string',
			required: true
		},
		{
			arg: 'caseRegistry',
			type: 'object',
			required: true
		}], returns: {
			arg: 'data',
			type: 'object',
			root: true
		},
		http: {
			verb: 'post',
			path: '/:caseId/pdf'
		}
	});
}
