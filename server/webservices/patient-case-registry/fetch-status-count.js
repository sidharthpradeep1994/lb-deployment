module.exports = function (patientCaseRegistry) {

	patientCaseRegistry.fetchStatusCount = async function (options) {
		try {
			const token = options && options.accessToken;
			const userId = token && token.userId;

			const user = await patientCaseRegistry.app.models.user.findById(userId);

			const patientModel = patientCaseRegistry.getDataSource().connector.collection(
				patientCaseRegistry.modelName
			);
			const statusQuery = new Promise((resolve, reject) => {

				patientModel.aggregate([{
					$match: {
						$and: [{ clientId: user.clientId }]
					}
				}, {
					$group: {
						_id: '$status',
						count: { $sum: 1 }
					}
				}]).toArray((err, data) => {
					if (err) reject(err);
					else resolve(data);
				});
			});

			return (async (statusResult) => {
				(await statusQuery).forEach(status => {
					statusResult[status._id || 'new'] = status.count
					statusResult['total'] += status.count;
				}); return statusResult;
			})({ total : 0});
		} catch (error) {
			throw error;
		}

	}

	patientCaseRegistry.remoteMethod('fetchStatusCount', {
		accepts: [{
			arg: 'options',
			type: 'object',
			http: 'optionsFromRequest'
		}], returns: {
			arg: 'data',
			type: 'object',
			root: true
		},
		http: {
			verb: 'get',
			path: '/status/count'
		}
	});
}
