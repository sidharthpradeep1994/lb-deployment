module.exports = function (patientCaseRegistry) {
	const typeChecks = require('../../lib/helpers/type-checks');
	patientCaseRegistry.updateMedicineOrder = async function (options, caseId, medications) {
		try {
			const token = options && options.accessToken;
			const userId = token && token.userId;
			if (!caseId) throw new Error('Unauthorized');
			const caseRegistry = await patientCaseRegistry.findById(caseId);
			if (!typeChecks.isArray(medications)) {
				throw new Error('Medications should be an array');
			}
			caseRegistry.medications = medications;
			return await caseRegistry.save(options);

		} catch (error) {
			throw error;
		}

	}

	patientCaseRegistry.remoteMethod('updateMedicineOrder', {
		accepts: [{
			arg: 'options',
			type: 'object',
			http: 'optionsFromRequest'
		}, {
			arg: 'caseId',
			type: 'string',
			required: true
		},
		{
			arg: 'medications',
			type: 'array',
			required: true
		}], returns: {
			arg: 'data',
			type: 'object',
			root: true
		},
		http: {
			verb: 'patch',
			path: '/:caseId/medications/reorder'
		}
	});
}
