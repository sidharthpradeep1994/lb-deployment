module.exports = function (PatientCaseRegistry) {

    PatientCaseRegistry.filterToSelect = async function (options, clientId, from, to, fixedFilters, variableFilters, strictFilter) {
        try {
            const token = options && options.accessToken;
            const userId = token && token.userId;
            const userData = await PatientCaseRegistry.app.models.user.findById(userId);
            const filters = [{ clientId: userData.clientId}];
            const filterType = strictFilter ? 'and' : 'or';
            allowedPatientFilters = ['firstName', 'lastName', 'middleName',
                'email', 'primaryPhoneNumber', 'secondaryPhoneNumber'];
            const allowedVariableFilters = ['contractName', 'status', 'patientCode', 'memberId'];
            if (from && to) {
                filters.push(...[{ createdAt: { gt: from } }, { createdAt: { lt: to } }]);
                console.log(filters)
            }
            fixedFilters && fixedFilters.languages && filters.push({ 'patientInfo.language': { inq: fixedFilters.languages } });
            fixedFilters && fixedFilters.statuses && filters.push({ status: { inq: fixedFilters.statuses } });

            variableFilters.map(filter => (() => {
                const filterObj = {};
                if (allowedVariableFilters.includes(filter.property)) {
                    filterObj[filter.property] = filter.value;
                } else if (allowedPatientFilters.includes(filter.property)) {
                    filterObj[`patientInfo.${filter.property}`] = filter.value;
                }
                filters.push(filterObj)
            })());
            console.log(filters)
            const selectedCases = await PatientCaseRegistry.find({ where: { and: filters }, fields: ['id', 'patientCode'] });
            const totalCases = await PatientCaseRegistry.count({ clientId: clientId });
            return { selectedCases, totalCases };

        } catch (error) {
            throw error;
        }

    }

    PatientCaseRegistry.remoteMethod('filterToSelect', {
        accepts: [{
            arg: 'options',
            type: 'object',
            http: 'optionsFromRequest'
        }, {
            arg: 'clientId',
            type: 'string',
            required: true
        },
        {
            arg: 'from',
            type: 'date',
            required: false
        },
        {
            arg: 'to',
            type: 'date',
            required: false
        }, {
            arg: 'fixedFilters',
            type: 'object',
            required: true
        }, {
            arg: 'variableFilters',
            type: 'array',
            required: true
        },
        {
            arg: 'strictFilter',
            type: 'boolean',
            required: true
        }],
        returns: [{
            arg: 'data',
            root: true,
            type: 'array'
        }],
        http: {
            verb: 'post',
            path: '/:clientId/select'
        }
    })

}