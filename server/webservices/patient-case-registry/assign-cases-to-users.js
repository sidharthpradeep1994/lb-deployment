module.exports = function (PatientCaseRegistry) {

    const roles = require('../../lib/models/roles');
    const AppError = require('../../utils/app-error');

    PatientCaseRegistry.assignCasesToUsers = async function (options, clientId, caseIds, userIds) {

        try {

            const token = options && options.accessToken;
            const userId = token && token.userId;

            const userData = await PatientCaseRegistry.app.models.user.findById(userId);
            if (
                userData.role !== roles.SUPERADMIN &&
                userData.role !== roles.CLIENTADMIN
            ) {
                throw new AppError('Unauthorized', 401);
            }

            const clientData = await PatientCaseRegistry.app.models.client.findById(clientId);
            if (!userData || !clientData || (userData.clientId != clientData.id)) {
                throw new Error('Unauthorized action');
            }

            const patientCaseList = await PatientCaseRegistry.find({ where: { and: [{ clientId: clientId }, { id: { inq: caseIds } }] }, skip: 0, limit: 0, fields: [], order: '' });

            const userList = await PatientCaseRegistry.app.models.user.find({ where: { and: [{ clientId: clientId }, { id: { inq: userIds } }] }, fields: ['id'] });

            const userIdList = userList.map(usr => usr.id);

            patientCaseList.map(pCase => userIdList.map(userId => pCase.assignedUsers.findIndex(aUser => aUser == userId) < 0 && pCase.assignedUsers.push(userId) && pCase.save(options)));

            return patientCaseList;



        } catch (error) {
            throw error;
        }

    }

    PatientCaseRegistry.remoteMethod('assignCasesToUsers', {
        accepts: [{ arg: 'options', type: 'object', http: 'optionsFromRequest' },
        { arg: 'clientId', type: 'string', required: true },
        { arg: 'caseIds', type: 'array', required: true }, {
            arg: 'userIds', type: 'array', required: true
        }],
        returns: {
            arg: 'data', root: true, type: 'object'
        },
        http: {
            path: '/:clientId/assign-cases',
            verb: 'post'
        }
    });
}