module.exports = function (patientCaseRegistry) {
	patientCaseRegistry.fetchOverallRecommendations = async function (options, caseId) {
		try {
			const token = options && options.accessToken;
			const userId = token && token.userId;
			const caseRegistry = await patientCaseRegistry.findById(caseId);

			if (caseRegistry.overallRecommendations) {
				return { overallRecommendations: caseRegistry.overallRecommendations };
			}
			else {

				const configTypes = await patientCaseRegistry.app.models.configType.find({ where: { and: [{ clientId: caseRegistry.clientId }, { programId: caseRegistry.programId }] }, skip: 0, limit: 0, order: 'createdAt DESC' });
				const overallRecommendations = [];
				configTypes.map((cnfig) => {
					let reportingProperties = cnfig.reportingProperties;
					reportingProperties.map(rp => {
						let recommendation = {};
						recommendation[rp.propertyName] = '';
						recommendation.propertyName = rp.propertyName;
						recommendation.description = rp.description;
						overallRecommendations.push(recommendation);
					});
				});
				caseRegistry.overallRecommendations = overallRecommendations;
				caseRegistry.save();

				return caseRegistry;

			}

		} catch (error) {
			throw error;
		}

	}




	patientCaseRegistry.remoteMethod('fetchOverallRecommendations', {
		accepts: [{
			arg: 'options',
			type: 'object',
			http: 'optionsFromRequest'
		}, {
			arg: 'caseId',
			type: 'string',
			required: true
		}], returns: {
			arg: 'data',
			type: 'object',
			root: true
		},
		http: {
			verb: 'get',
			path: '/:caseId/recommendations'
		}
	});
}
