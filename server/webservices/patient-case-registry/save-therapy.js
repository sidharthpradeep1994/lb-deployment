module.exports = function (patientCaseRegistry) {
	const allowOperation = require('../../lib/services/common/allow-case');
	const AppError = require('../../utils/app-error');
	const Joi = require('joi');

	const schema = Joi.object().keys({
		caseId: Joi.string().required(),
		medicineId: Joi.string().required(),
		therapyIds: Joi.array().items(Joi.string()),
		adherenceFlag: Joi.boolean()
	});
	patientCaseRegistry.saveTherapy = async function (options, caseId, medicineId, therapyIds, adherenceFlag) {
		try {
			// await schema.validate({
			// 	caseId, medicineId, therapyIds, adherenceFlag
			// });
			const token = options && options.accessToken;
			const userId = token && token.userId;
			const user = await patientCaseRegistry.app.models.user.findById(userId);
			const patientCase = await patientCaseRegistry.findById(caseId);
			if (!allowOperation(patientCase, user, 'write').access) {
				const error = new AppError('unauthorized', 401);
				throw error;
			}
			const medication = await patientCase.medicationRegistry.findById(medicineId);
			const therapies = await patientCaseRegistry.app.models.therapy.find({
				where: {
					and: [{
						id: { inq: therapyIds }
					}]
				}, skip: 0, limit: 0
			});

			const recommendationStatements = {};
			therapyIds = therapies.map((therapy) => {
				let reportingStatements = therapy.reportingStatements;
				for (const key in reportingStatements) {
					if (reportingStatements.hasOwnProperty(key)) {
						if (recommendationStatements[key]) {
							recommendationStatements[key] += ` ${reportingStatements[key]}.`;
						} else {
							recommendationStatements[key] = `${reportingStatements[key]}.`;
						}

					}
				}
				return therapy.id;
			});
			const recommendation = {
				recommendedBy: userId,
				recommendationDate: new Date(),
				recommendationStatements: recommendationStatements
			};
			medication.therapies = therapyIds;
			medication.recommendation = recommendation;
			medication.adherenceFlag = adherenceFlag

			return await medication.save(options);


		} catch (error) {
			throw error;
		}

	}

	patientCaseRegistry.remoteMethod('saveTherapy', {
		accepts: [{
			arg: 'options',
			type: 'object',
			http: 'optionsFromRequest'
		}, {
			arg: 'caseId',
			type: 'string',
			required: true
		},
		{
			arg: 'medicineId',
			type: 'string',
			required: true
		},
		{
			arg: 'therapyIds',
			type: 'array',
			description: 'ids of therapies',
			required: false
		},
		{
			arg: 'adherenceFlag',
			type: 'boolean',
			required: true
		}], returns: {
			arg: 'data',
			type: 'object',
			root: true
		},
		http: {
			verb: 'post',
			path: '/:caseId/therapy/save'
		}
	});
}
