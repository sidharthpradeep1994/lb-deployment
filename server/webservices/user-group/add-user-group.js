module.exports = function (UserGroup) {
    UserGroup.addUserGroup = async function (options, clientId, groupName) {
        try {
            const token = options && options.accessToken;
            const userId = token && token.userId;
            const userData = await UserGroup.app.models.user.findById(userId);
            if (userData.clientId != clientId) {
                throw new Error('Unauthorized action');
            }
            return await UserGroup.create({
                name: groupName,
                clientId: clientId,
                usersCount: 0,
                users: [],
                admins: []
            }, options);

        }
        catch (error) {
            throw error;
        }
    }

    UserGroup.remoteMethod('addUserGroup', {
        accepts: [{
            arg: 'options',
            type: 'object',
            http: 'optionsFromRequest'
        },
        {
            arg: 'clientId',
            type: 'string',
            required: true
        }, {
            arg: 'groupName',
            type: 'string',
            required: true
        }],
        returns: {
            arg: 'data',
            root: true,
            type: 'object'
        },
        http: {
            verb: 'post',
            path: '/:clientId/add'
        }
    })
}