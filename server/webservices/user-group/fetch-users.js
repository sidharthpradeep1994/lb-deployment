module.exports = function (UserGroup) {
    
    UserGroup.fetchUsers = async function (options, groupId, skip, limit) {
       
        try {

            const token = options && options.accessToken;
            const userId = token && token.userId;

            const userData = await UserGroup.app.models.user.findById(userId);

            const groupMembers = await UserGroup.app.models.groupMember.find({
                where: {
                    and: [{
                        groupId: groupId
                    },
                     { clientId: userData.clientId }
                ]
                }, skip: skip, limit: limit, fields: [], order: ''
            });

            const userGroup = await UserGroup.findById(groupId);


            return { groupMembers, count: userGroup.usersCount };

        } catch (error) {
            throw error;
        }


    }


    UserGroup.remoteMethod('fetchUsers', {
        accepts: [
            { arg: 'options', type: 'object', http: 'optionsFromRequest' },
            { arg: 'groupId', type: 'string', required: true },
            { arg: 'skip', type: 'number', default: 0, required: true },
            { arg: 'limit', type: 'number', default: 100, required: true }

        ],
        returns: {
            arg: 'data',
            type: 'object',
            root: true
        },
        http: {
            verb: 'get',
            path: '/:groupId/users'
        }
    });


}