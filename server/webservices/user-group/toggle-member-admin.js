module.exports = function (UserGroup) {
    UserGroup.toggleMemberAdmin = async function (options, memberId, admin) {
        try {
            const token = options && options.accessToken;
            const userId = token && token.userId;
            const userData = await UserGroup.app.models.user.findById(userId);
            const member = await UserGroup.app.models.groupMember.findById(memberId);
            return await member.updateAttribute('admin', admin, options);
        } catch (error) {
            throw error;
        }
    }

    UserGroup.remoteMethod('toggleMemberAdmin', {
        accepts: [{ arg: 'options', type: 'object', http: 'optionsFromRequest' }, { arg: 'memberId', type: 'string', required: true },
        { arg: 'admin', type: 'boolean', required: true }],
        returns: {
            arg: 'data', root: true, type: 'object'
        },
        http: {
            verb: 'patch', path: '/members/:memberId/admin'
        }
    })

}