module.exports = function (UserGroup) {
    
    
    UserGroup.addUsers = async function (options, clientId, groupId, userIds) {
        try {
            const token = options && options.accessToken;
            const userId = token && token.userId;
            const userData = await UserGroup.app.models.user.findById(userId);
            if (!userData || userData.clientId !== clientId) {
                throw new Error('Unauthorized access');
            }
            const clientData = await UserGroup.app.models.client.findById(clientId);
            const userGroup = await UserGroup.findById(groupId);
            if (!userGroup) {
                throw new Error('User group instance not found');
            }
            const userList = await UserGroup.app.models.user.find({
                where: { and: [{ id: { inq: userIds } }, { clientId: clientData.id }] },
                skip: 0, limit: 0, fields: ['id', 'username', 'firstName', 'middleName', 'lastName', 'role']
            });

            userIdList = userList.map(user => user.id);

            userIdList.map(userId => userGroup.users.findIndex((id) => userId == id) < 0
             && userGroup.users.push(userId));

            const groupMembers = userList.map(async user => await UserGroup.app.models.groupMember.findOrCreate({
                where: {
                    and: [{ clientId: clientData.id }, {
                        groupId: userGroup.id
                    }, {
                        userId: user.id
                    }]
                }
            }, {
                userId: user.id, groupId: userGroup.id, groupName: userGroup.name, userData: user, admin: false, clientId: clientData.id
            }));
            userGroup.usersCount = userGroup.users.length;
            userGroup.save(options);

            return groupMembers;

        } catch (error) {
            throw error;
        }
    }

    
    UserGroup.remoteMethod('addUsers', {
        accepts: [{
            arg: 'options',
            type: 'object',
            http: 'optionsFromRequest'
        },
        {
            arg: 'clientId',
            type: 'string',
            required: true
        }, {
            arg: 'groupId',
            type: 'string',
            required: true
        }, {
            arg: 'userIds',
            type: 'array'
        }],
        returns: {
            arg: 'data',
            type: 'array',
            root: true
        },
        http: {
            verb: 'post',
            path: '/:clientId/add-users/:groupId'
        }
    });


}