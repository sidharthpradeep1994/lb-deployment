module.exports = function (UserGroup) {

    UserGroup.fetchUserGroups = async function (options, clientId, skip, limit,searchField,searchString,from,to) {
        try {
            const token = options && options.accessToken;
            const userId = token && token.userId;

            const userData = await UserGroup.app.models.user.findById(userId);
            if (userData.clientId != clientId) {
                throw new Error('Unauthorized access');
            }
            const filter = {
                where: {
                    and: [{
                        clientId: clientId 
                    }]
                }, skip: skip, limit: limit
                // fields: ['firstName', 'middleName', 'lastName', 'primaryLanguage', 'contractName', '']
            };
            from && from.trim() !== '' && from !== 'null' && filter.where.and.push({ createdAt: { gte: new Date(from) } });
            to && to.trim() !== '' && to !== 'null' && filter.where.and.push({ createdAt: { lte: new Date(to) } });
            searchString && searchString.trim() !== '' && searchString.trim() !== 'undefined' && searchField.trim() != 'undefined'
            && (() => {
                const searchFilter = {};
                searchFilter[searchField] = { like: new RegExp(searchString.trim() + '$') };
                filter.where.and.push(searchFilter);
            })();
            return UserGroup.find(filter);

        } catch (error) {

        }
    }


    UserGroup.remoteMethod('fetchUserGroups', {
        accepts: [{ arg: 'options', type: 'object', http: 'optionsFromRequest' }, { arg: 'clientId', type: 'string', required: true },
        { arg: 'skip', type: 'number', required: false, default: 0 }, 
        { arg: 'limit', type: 'number', required: false, default: 100 },
        { arg: 'searchField', type: 'string', required: false, default: 0 },
        { arg: 'searchString', type: 'string', required: false, default: 0 },
        { arg: 'from', type: 'string', required: false, default: 0 },
        { arg: 'to', type: 'string', required: false, default: 0 }],
        returns: {
            arg: 'data', type: 'array', root: true
        },
        http: {
            verb: 'get',
            path: '/:clientId/filter'
        }
    })

}