module.exports = function (Audit) {


    Audit.filterAudits = async function (options, skip, limit, clientId, from, to, requestId) {

        try {
            const accessToken = options && options.accessToken;
            const userId = accessToken && accessToken.userId;
            const filter = {
                where: {
                    and: [
                         { createdAt: { gte: from } }, { createdAt: { lte: to } }
                    ]
                }, skip: skip, limit: limit
            };
            clientId&&clientId.trim() !== '' && clientId !== 'undefined' && filter.where.and.push({clientId});
            requestId&&requestId.trim() !== '' && requestId !== 'undefined' && filter.where.and.push({requestId});
            const audits = await Audit.find(filter);
            const count = await Audit.count(filter.where);

            return { audits: audits, count: count };
            
        } catch (error) {
            throw error;
        }

    }


    Audit.remoteMethod('filterAudits', {
        accepts: [{
            arg: 'options', type: 'object', http: 'optionsFromRequest'
        },
        {
            arg: 'skip', type: 'number', default: 0
        },
        {
            arg: 'limit', type: 'number', default: 0
        },
        {
            arg: 'clientId', type: 'number', default: 0
        },
        {
            arg: 'from', type: 'date'
        },
        {
            arg: 'to', type: 'date'
        },
        {
            arg: 'requestId', type: 'string'
        }], returns: { arg: 'data', type: 'object', root: true },
        http: { path: '/filter', verb: 'GET' }
    });

}