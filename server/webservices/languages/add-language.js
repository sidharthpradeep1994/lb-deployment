module.exports = function (Languages) {
	
	Languages.addLanguage = async function (options,name) {
		try {
			const token = options && options.accessToken;
			const userId = token && token.userId;
			const user = await Languages.app.models.user.findById(userId);
			if(user.role=='clientAdmin')
			{
				return await Languages.create({
					clientId:user.clientId,
					name:name,
				});
			}
		} catch (error) {
			throw error;
		}

	}

	Languages.remoteMethod('addLanguage', {
		accepts: [{
			arg: 'options',
			type: 'object',
			http: 'optionsFromRequest'
		},
		{
			arg: 'name',
			type: 'string',
			required: true
		}
		], returns: {
			arg: 'data',
			type: 'object',
			root: true
		},
		http: {
			verb: 'post',
			path: '/add'
		}
	});
}