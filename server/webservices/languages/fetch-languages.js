module.exports = function (Languages) {
	
	Languages.fetchLanguages = async function (options) {
		try {
            const token = options && options.accessToken;
			const userId = token && token.userId;
			const user = await Languages.app.models.user.findById(userId);
            return await Languages.app.models.Languages.find({ where: { and: [{ clientId: user.clientId }] }, skip: 0, limit: 0, fields: [], order: '' })
		} catch (error) {
			throw error;
		}

	}

	Languages.remoteMethod('fetchLanguages', {
		accepts: [{
			arg: 'options',
			type: 'object',
			http: 'optionsFromRequest'
		}
		], returns: {
			arg: 'data',
			type: 'object',
			root: true
		},
		http: {
			verb: 'post',
			path: '/fetch'
		}
	});
}