module.exports = function (ticket) {
	
	ticket.updateTicketStatus = async function (options,ticketId,status) {
		try {
			const token = options && options.accessToken;
            const userId = token && token.userId;
            const ticketSystem = await ticket.findById(ticketId);
			return await ticketSystem.updateAttribute('status', status);
		} catch (error) {
			throw error;
		}

	}

	ticket.remoteMethod('updateTicketStatus', {
		accepts: [{
			arg: 'options',
			type: 'object',
			http: 'optionsFromRequest'
		},
		{
			arg: 'ticketId',
			type: 'string',
			required: true
        },
        {
			arg: 'status',
			type: 'string',
			required: true
		}
		], returns: {
			arg: 'data',
			type: 'object',
			root: true
		},
		http: {
			verb: 'post',
			path: '/updatestatus'
		}
	});
}