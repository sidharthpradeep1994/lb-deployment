module.exports = function (ticket) {
	
	ticket.deleteTiket = async function (options,ticketId) {
		try {
			const token = options && options.accessToken;
			const userId = token && token.userId;
            return await ticket.destroyById(ticketId);
		} catch (error) {
			throw error;
		}

	}

	ticket.remoteMethod('deleteTiket', {
		accepts: [{
			arg: 'options',
			type: 'object',
			http: 'optionsFromRequest'
		},
		{
			arg: 'ticketId',
			type: 'string',
			required: true
		}
		], returns: {
			arg: 'data',
			type: 'object',
			root: true
		},
		http: {
			verb: 'post',
			path: '/delete'
		}
	});
}