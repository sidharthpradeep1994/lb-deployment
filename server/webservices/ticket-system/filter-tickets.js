module.exports = function (ticket) {
    const Roles = require('../../lib/models/roles');
    ticket.filterTickets = async function (options, ticketCategory, status, from, to, skip, limit ) {
        try {
            const token = options && options.accessToken;
            const userId = token && token.userId;
            const user = await ticket.app.models.user.findById(userId);
			/* if ((!user || !user.clientId) && user.role !== Roles.SUPERADMIN) {
				throw new Error('Invalid user');
            } */
            let filter={};
            if(user.role=='clientAdmin')
            {
                 filter = {
                    where: {
                        and: [{
                            clientId: user.clientId
                        }]
                    }, skip: skip, limit: limit
                    // fields: ['firstName', 'middleName', 'lastName', 'primaryLanguage', 'contractName', '']
                };
            }
            else if(user.role=='superAdmin')
            {
                filter = {
                    where: {
                        and: [{
                        }]
                    }, skip: skip, limit: limit
                };  
            }
            
            ticketCategory && ticketCategory.trim() !== '' && ticketCategory !== 'undefined' && filter.where.and.push({ category: ticketCategory });

            from && from.trim() !== '' && from !== 'null' && filter.where.and.push({ createdAt: { gte: new Date(from) } });

            to && to.trim() !== '' && to !== 'null' && filter.where.and.push({ createdAt: { lte: new Date(to) } });

            status && status !== 'undefined' && filter.where.and.push({ status: status });

            const data = await ticket.find(filter);
            const count = await ticket.count(filter.where);
            return { data, count };


        } catch (error) {
            throw error;
        }

    }

    ticket.remoteMethod('filterTickets', {
        accepts: [{
            arg: 'options',
            type: 'object',
            http: 'optionsFromRequest'
        }, 
        {
            arg: 'ticketCategory',
            type: 'string',
            description: 'assigned to',
            required: false
        },
        {
            arg: 'status',
            type: 'string',
            description: 'assigned to',
            required: false
        },
        {
            arg: 'from',
            type: 'string',
            description: 'created from',
            // default: '',
            required: false
        },
        {
            arg: 'to',
            type: 'string',
            description: 'created to',
            // default: '',
            required: false
        },
        {
            arg: 'skip',
            type: 'number',
            description: 'Number of records to skip',
            default: 0,
            required: false
        },
        {
            arg: 'limit',
            type: 'number',
            description: 'Max number of records to return',
            default: 100,
            required: false
        }], returns: {
            arg: 'data',
            type: 'object',
            root: true
        },
        http: {
            verb: 'get',
            path: '/filter'
        }
    });
}
