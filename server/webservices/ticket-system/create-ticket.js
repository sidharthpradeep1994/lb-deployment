module.exports = function (ticket) {
	
	ticket.createTicket = async function (options,ticketCategory,priority,subject,description,status) {
		try {
			const token = options && options.accessToken;
			const userId = token && token.userId;
			const user = await ticket.app.models.user.findById(userId);
			const client = await ticket.app.models.client.findById(user.clientId);
			let dateNow = new Date();
			return await ticket.create({
				clientId:user.clientId,
				clientName:client.name,
				userId:userId,
				userName:user.username,
				status:status,
				description:description,
				subject:subject,
				category:ticketCategory,
				priority:priority,
				createdAt:dateNow
			});
		} catch (error) {
			throw error;
		}

	}

	ticket.remoteMethod('createTicket', {
		accepts: [{
			arg: 'options',
			type: 'object',
			http: 'optionsFromRequest'
		},
		{
			arg: 'ticketCategory',
			type: 'string',
			required: true
		},
		{
			arg:'priority',
			type :'string',
			required:true
		},
		{
			arg: 'subject',
			type: 'string',
			required: true
		},
		{
			arg: 'description',
			type: 'string',
			required: true
		},
		{
			arg:'status',
			type:'string',
			required:'true'
		}
		], returns: {
			arg: 'data',
			type: 'object',
			root: true
		},
		http: {
			verb: 'post',
			path: '/create'
		}
	});
}