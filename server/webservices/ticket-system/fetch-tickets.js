module.exports = function (ticket) {
	
	ticket.fetchTickets = async function (options) {
		try {
			const token = options && options.accessToken;
			const userId = token && token.userId;
			const user = await ticket.app.models.user.findById(userId); 
			if(user.role=='clientAdmin')
			{
				ticketOutput = await ticket.app.models.ticketSystem.find({ where: { and: [{ clientId: user.clientId }] }, skip: 0, limit: 0, fields: [], order: '' })
			}  
			else if(user.role=='superAdmin')
			{
				ticketOutput = await ticket.app.models.ticketSystem.find({ skip: 0, limit: 0, fields: [], order: '' })
			} 
			console.log(ticketOutput);
			return ticketOutput;
            
		} catch (error) {
			throw error;
		}

	}

	ticket.remoteMethod('fetchTickets', {
		accepts: [{
			arg: 'options',
			type: 'object',
			http: 'optionsFromRequest'
		}
		], returns: {
			arg: 'data',
			type: 'object',
			root: true
		},
		http: {
			verb: 'get',
			path: '/fetchtickets'
		}
	});
}