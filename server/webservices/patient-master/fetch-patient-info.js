module.exports = function (patientMaster) {
	patientMaster.fetchPatientInfo = async function (options, id) {
		try {
			const token = options && options.accessToken;
			const userId = token && token.userId;
			const patient = await patientMaster.findOne({
				where: { and: [{ id: id }] }, skip: 0, limit: 0, fields: [
					'id', 'patientCode', 'firstName', 'middleName', 'lastName', 'gender', 'dob', 'primaryAddress', 'secondaryAddress',
					'primaryPhoneNumber', 'secondaryPhoneNumber'
				]
			});
			return patient;


		} catch (error) {
			throw error;
		}

	}

	patientMaster.remoteMethod('fetchPatientInfo', {
		accepts: [{
			arg: 'options',
			type: 'object',
			http: 'optionsFromRequest'
		}, {
			arg: 'id',
			type: 'string',
			required: true
		}], returns: {
			arg: 'data',
			type: 'object',
			root: true
		},
		http: {
			verb: 'get',
			path: '/:id/info'
		}
	});
}
