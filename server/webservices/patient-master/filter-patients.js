module.exports = function (patientMaster) {
	patientMaster.filterPatients = async function (options, searchString, skip, limit) {
		try {
			const token = options && options.accessToken;
			const userId = token && token.userId;
			const user = await patientMaster.app.models.user.findById(userId);
			if (!user || !user.clientId) {
				throw new Error('Invalid user');
			}
			const filter = {
				where: {
					and: [{
						clientId: user.clientId
					}]
				}, skip: skip, limit: limit,
				fields: ['firstName', 'middleName', 'lastName', 'primaryLanguage', 'contractName', '']
			};

			searchString && searchString.trim() !== ''
				&& filter.where.and.push({ primaryPhoneNumber: { like: new RegExp(searchString.trim() + "$") } });
			const data = await patientMaster.find(filter);
			const count = await patientMaster.count(filter.where);
			return { data, count };
			

		} catch (error) {
			throw error;
		}

	}

	patientMaster.remoteMethod('filterPatients', {
		accepts: [{
			arg: 'options',
			type: 'object',
			http: 'optionsFromRequest'
		}, {
			arg: 'searchString',
			type: 'string',
			required: false
		},
		{
			arg: 'skip',
			type: 'number',
			default: 0,
			required: false
		}, {
			arg: 'limit',
			type: 'number',
			default: 20,
			required: false
		}], returns: {
			arg: 'data',
			type: 'object',
			root: true
		},
		http: {
			verb: 'get',
			path: '/filter'
		}
	});
}
