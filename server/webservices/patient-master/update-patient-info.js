module.exports = function (PatientMaster) {
	PatientMaster.updatePatientInfo = async function (options, id, primaryAddress, secondaryAddress, primaryPhoneNumber, secondaryPhoneNumber) {
		try {
			const token = options && options.accessToken;
			const userId = token && token.userId;
			const user = await PatientMaster.app.models.user.findById(userId);

			const patientMaster = await PatientMaster.findById(id);

			patientMaster.addresses.push(patientMaster.primaryAddress);

			patientMaster.primaryAddress.addressLine1StreetName = primaryAddress.addressLine1StreetName;
			patientMaster.primaryAddress.addressLine2AptSuiteUnit = primaryAddress.addressLine2AptSuiteUnit;
			patientMaster.primaryAddress.city = primaryAddress.city;
			patientMaster.primaryAddress.state = primaryAddress.state;
			patientMaster.zipCode = primaryAddress.zipCode;

			patientMaster.secondaryAddress.addressLine1StreetName = secondaryAddress.addressLine1StreetName;
			patientMaster.secondaryAddress.addressLine2AptSuiteUnit = secondaryAddress.addressLine2AptSuiteUnit;
			patientMaster.secondaryAddress.city = secondaryAddress.city;
			patientMaster.secondaryAddress.state = secondaryAddress.state;
			patientMaster.secondaryAddress.zipCode = secondaryAddress.zipCode;

			patientMaster.primaryPhoneNumber = primaryPhoneNumber;

			patientMaster.secondaryPhoneNumber = secondaryPhoneNumber;

			const patientCase = await PatientMaster.app.models.patientCaseRegistry.findOne({
				where: {
					and: [{ patientUid: patientMaster.id },
					{ clientid: user.clientid }]
				}, order: 'id DESC'
			});
			await patientCase.updateAttribute('patientInfo', patientMaster, options);

			//Need some clarification here
			return await patientMaster.save(options);

		} catch (error) {
			throw error;
		}

	}

	PatientMaster.remoteMethod('updatePatientInfo', {
		accepts: [{
			arg: 'options',
			type: 'object',
			http: 'optionsFromRequest'
		}, {
			arg: 'id',
			type: 'string',
			required: true
		}, {
			arg: 'primaryAddress',
			type: 'Address',
			required: true
		}, {
			arg: 'secondaryAddress',
			type: 'Address',
			required: true
		}, {
			arg: 'primaryPhoneNumber',
			type: 'string',
			required: true
		}, {
			arg: 'secondaryPhoneNumber',
			type: 'string',
			required: false
		}], returns: {
			arg: 'data',
			type: 'object',
			root: true
		},
		http: {
			verb: 'patch',
			path: '/:id/info'
		}
	});
}
