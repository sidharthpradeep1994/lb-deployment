module.exports = function (program) {
	program.fetchProgram = async function (options, programId) {
		try {
			const token = options && options.accessToken;
			const userId = token && token.userId;
			const programData = await program.findById(programId);

			const configData = await program.app.models.configType.find({
				where: { and: [{ programId: programData.id }] }
				, skip: 0, limit: 0, fields: [], order: 'createdAt ASC'
			});

			programData.adherenceSections = configData.filter(config => config.category === 'adherence');
			programData.nonAdherenceSections = configData.filter(config => config.category === 'non-adherence');
			return programData;
		} catch (error) {
			throw error;
		}

	}

	program.remoteMethod('fetchProgram', {
		accepts: [{
			arg: 'options',
			type: 'object',
			http: 'optionsFromRequest'
		}, {
			arg: 'programId',
			type: 'string',
			required: true
		}], returns: {
			arg: 'data',
			type: 'object',
			root: true
		},
		http: {
			verb: 'get',
			path: '/:programId/fetch'
		}
	});
}
