module.exports = function (program) {
	program.saveProgram = async function (options, id, name, description) {
		try {
			const token = options && options.accessToken;
			const userId = token && token.userId;
			const user = await program.app.models.user.findById(userId);

			if (id) {
				const programData = await program.findById(id);
				return await programData.updateAttributes({ name, description }, options);

			} else {
				return await program.create({ name, description, clientId: user.clientId }, options);

			}

		} catch (error) {
			throw error;
		}

	}

	program.remoteMethod('saveProgram', {
		accepts: [{
			arg: 'options',
			type: 'object',
			http: 'optionsFromRequest'
		}, {
			arg: 'id',
			type: 'string',
			required: false
		},
		{
			arg: 'name',
			type: 'string',
			required: true
		},
		{
			arg: 'description',
			type: 'string',
			required: false
		}], returns: {
			arg: 'data',
			type: 'object',
			root: true
		},
		http: {
			verb: 'post',
			path: '/save'
		}
	});
}
