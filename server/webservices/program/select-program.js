module.exports = function (program) {
  program.selectProgram = async function (options, programId) {
    try {
      const token = options && options.accessToken;
      const userId = token && token.userId;
      const user = await program.app.models.user.findById(userId);
      const clientData = await program.app.models.client.findById(user.clientId);
      return await clientData.updateAttribute('selectedProgram', programId, options);

    } catch (error) {
      throw error;
    }

  }

  program.remoteMethod('selectProgram', {
    accepts: [{
      arg: 'options',
      type: 'object',
      http: 'optionsFromRequest'
    }, {
      arg: 'programId',
      type: 'string',
      required: true
    }], returns: {
      arg: 'data',
      type: 'object',
      root: true
    },
    http: {
      verb: 'get',
      path: '/:programId/select'
    }
  });
}
