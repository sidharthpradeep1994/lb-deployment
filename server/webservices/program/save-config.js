module.exports = function (program) {
	program.saveConfig = async function (options, programId, config) {
		try {
			const token = options && options.accessToken;
			const userId = token && token.userId;
			const user = await program.app.models.user.findById(userId);
			if (config.id) {
				const configData = await program.app.models.configType.findById(config.id);
				configData.reportingProperties = configData.reportingProperties.map(rs => {
					rs.programId = programId;
					rs.clientId = configData.clientId;
					rs.configName = configData.name;
					rs.configId = configData.id;
					return rs;
				});
				configData.reportingProperties.map(async rs => await program.app.models.reportingProperty.replaceOrCreate(rs));
				return await configData.updateAttributes({ name: config.name, category: config.category, type: config.type, reportingProperties: config.reportingProperties }, options);

			} else {

				config.clientId = user.clientId;
				config.programId = programId;
				const configData = await program.app.models.configType.create(config, options);
				config.reportingProperties = config.reportingProperties.map(rs => {
					rs.programId = programId;
					rs.clientId = configData.clientId;
					rs.configName = configData.name;
					rs.configId = configData.id;
					return rs;
				});
				config.reportingProperties.map(async rs => await program.app.models.reportingProperty.create(rs, options));
				return configData;
				
			}


		} catch (error) {
			throw error;
		}

	}

	program.remoteMethod('saveConfig', {
		accepts: [{
			arg: 'options',
			type: 'object',
			http: 'optionsFromRequest'
		}, {
			arg: 'programId',
			type: 'string',
			required: true
		}, {
			arg: 'config',
			type: 'configType',
			required: true
		}], returns: {
			arg: 'data',
			type: 'object',
			root: true
		},
		http: {
			verb: 'post',
			path: '/:programId/config'
		}
	});
}
