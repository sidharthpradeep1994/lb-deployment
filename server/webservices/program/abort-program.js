module.exports = function (Program) {

    const Role = require('../../lib/models/roles');
    const AppError = require('../../utils/app-error');


    Program.abortProgram = async function (options, programId) {
        try {
            const accessToken = options && options.accessToken;
            const userId = accessToken && accessToken.userId;

            const userData = await Program.app.models.user.findById(userId);

            const programData = await Program.findById(programId);

            const therapies = await Program.app.models.therapy.find({ where: { and: [{ programId: programId }, { clientId: programData.clientId }] }, skip: 0, limit: 0, fields: [], order: '' })
            if (therapies.length > 0) {
                throw new AppError('Forbidden action', 403)
            }

            if ((userData.role !== Role.CLIENTADMIN && userData.role !== Role.SUPERADMIN) || programData.clientId !== userData.clientId) {
                throw new AppError('Forbidden operation', 403);
            }

            await Program.app.models.configType.destroyAll({ programId: programId }, options);
            if (programData) {
                await programData.destroy(options);
            }

            return { abort: true };

        } catch (error) {
            throw error;
        }
    }



    Program.remoteMethod('abortProgram', {
        accepts: [{
            arg: 'options', type: 'object', http: 'optionsFromRequest'
        },
        {
            arg: 'programId', type: 'string', required: true
        }], returns: { arg: 'data', type: 'object', root: true },
        http: { path: '/abort/:programId', verb: 'POST' }
    });



}