module.exports = function (ConfigType) {
	ConfigType.removeConfigType = async function (options, id) {
		try {
			const token = options && options.accessToken;
			const userId = token && token.userId;
			return await ConfigType.destroyById(id, options);
		} catch (error) {
			throw error;
		}

	}

	ConfigType.remoteMethod('removeConfigType', {
		accepts: [{
			arg: 'options',
			type: 'object',
			http: 'optionsFromRequest'
		}, {
			arg: 'id',
			type: 'string',
			required: true
		}], returns: {
			arg: 'data',
			type: 'object',
			root: true
		},
		http: {
			verb: 'delete',
			path: '/section/:id'
		}
	});
}
