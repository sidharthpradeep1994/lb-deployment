module.exports = function intialize(Program) {
    Program.saveReportingProperties = async function saveReportingProperties(options, programId, reportingProperties) {
        try {
            const token = options && options.accessToken;
            const userId = token && token.userId;
            console.log(reportingProperties)
            reportingProperties = reportingProperties.map(async rp => await Program.app.models.reportingProperty.updateAll({
                id: rp.id
            }, rp, options));
            return reportingProperties;
        } catch (error) {
            throw error;
        }
    }

    Program.remoteMethod('saveReportingProperties', {
        accepts: [{
            arg: 'options',
            type: 'object',
            http: 'optionsFromRequest'
        }, { arg: 'programId', type: 'string', required: true }, { arg: 'reportingProperties', type: 'array', required: true }],
        returns: {
            arg: 'data',
            type: 'array',
            root: true
        },
        http: {
            verb: 'patch',
            path: '/:programId/reporting-properties'
        }
    })
}