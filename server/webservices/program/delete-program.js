module.exports = function (program) {
	program.deleteProgram = async function (options, programId) {
		try {
			const token = options && options.accessToken;
			const userId = token && token.userId;
			await program.app.models.configType.destroyAll({ programId: programId }, options);
			return await program.destroyById(programId, options);
		} catch (error) {
			throw error;
		}

	}

	program.remoteMethod('deleteProgram', {
		accepts: [{
			arg: 'options',
			type: 'object',
			http: 'optionsFromRequest'
		}, {
			arg: 'programId',
			type: 'string',
			required: true
		}], returns: {
			arg: 'data',
			type: 'object',
			root: true
		},
		http: {
			verb: 'delete',
			path: '/:id/delete'
		}
	});
}
