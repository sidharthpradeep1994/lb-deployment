module.exports = function (program) {
	program.fetchPrograms = async function (options, skip, limit, searchString, searchField, from, to) {
		try {
			const token = options && options.accessToken;
			const userId = token && token.userId;
			skip = skip ? skip : 0;
			limit = limit ? limit : 0;
			const user = await program.app.models.user.findById(userId);
			let filter = {};
			if (user.role == 'clientAdmin') {
				filter = {
					where: {
						and: [{
							clientId: user.clientId
						}]
					}, skip: skip, limit: limit
					// fields: ['firstName', 'middleName', 'lastName', 'primaryLanguage', 'contractName', '']
				};
			}
			else if (user.role == 'superAdmin') {
				filter = {
					where: {
						and: [{
						}]
					}, skip: skip, limit: limit
				};
			}
			/*  searchString && searchString !== 'undefined' && searchString.trim() !== '' && filter.where.and.push({
			 name: {
			 like: searchString
			 }
		 }); */
			searchString && searchString.trim() !== '' && searchString.trim() !== 'undefined' && searchField.trim() != 'undefined'
				&& (() => {
					const searchFilter = {};
					searchFilter[searchField] = { like: new RegExp(searchString.trim() + '$') };
					filter.where.and.push(searchFilter);

				})();
			from && from.trim() !== '' && from !== 'null' && filter.where.and.push({ createdAt: { gte: new Date(from) } });
			to && to.trim() !== '' && to !== 'null' && filter.where.and.push({ createdAt: { lte: new Date(to) } });
			//console.log(filter.where.and)
			const programs = await program.find(filter);
			const count = await program.count(filter.where);
			return { programs, count };
		} catch (error) {
			throw error;
		}

	}

	program.remoteMethod('fetchPrograms', {
		accepts: [{
			arg: 'options',
			type: 'object',
			http: 'optionsFromRequest'
		},
		{ arg: 'skip', type: 'number', required: false },
		{ arg: 'limit', type: 'number', required: false },
		{ arg: 'searchString', type: 'string', required: false },
		{ arg: 'searchField', type: 'string', required: false },
		{ arg: 'from', type: 'string', description: 'created from', required: false },
		{ arg: 'to', type: 'string', description: 'created to', required: false }
		], returns: {
			arg: 'data',
			type: 'object',
			root: true
		},
		http: {
			verb: 'get',
			path: '/fetch'
		}
	});
}
