module.exports = function (Program) {
	Program.getProgramProperties = async function (options, programId) {
		try {
			const letterTypes = ['prescriber', 'member'];
			const token = options && options.accessToken;
			const userId = token && token.userId;
			const reportingProperties = await Program.app.models.reportingProperty.find({
				where: {
					and: [{
						programId: programId
					}]
				}, skip: 0, limit: 0, fields: [], order: ''
			});
			console.log(reportingProperties)
			const sortedProperties = [];

			letterTypes.map(lt => {
				sortedProperties.push({
					letter: lt,
					reportingProperties: reportingProperties.filter(rp => rp.assignedLetters && rp.assignedLetters.includes(lt))
				})
			});

			return sortedProperties;

		} catch (error) {
			throw error;
		}

	}

	Program.remoteMethod('getProgramProperties', {
		accepts: [{
			arg: 'options',
			type: 'object',
			http: 'optionsFromRequest'
		}, {
			arg: 'programId',
			type: 'string',
			required: true
		}], returns: {
			arg: 'data',
			type: 'object',
			root: true
		},
		http: {
			verb: 'get',
			path: '/:programId/reporting-properties'
		}
	});
}
