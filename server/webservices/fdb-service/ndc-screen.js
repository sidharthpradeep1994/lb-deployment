module.exports = function (fdbService) {
	const AppError = require('../../utils/app-error');
	const roles = require('../../lib/models/roles');
	const request = require('request');
	fdbService.ndcScreen = async function (options, screenProfile, clientId) {
		try {
			const token = options && options.accessToken;
			const userId = token && token.userId;
			if (!userId) {
				throw new AppError('Authorization Error', 401);
			}
			const user = await fdbService.app.models.user.findById(userId);
			if (![roles.SUPERADMIN, roles.CLIENTADMIN, roles.CLINICIAN].includes(user.role)) {
				throw new AppError('Authorization Error', 401);
			}
			const clientData = await fdbService.app.models.client.findById(clientId);
			const response = await sendRequest(screenProfile, clientData.name);
			fdbService.create({ requestMethod: 'ndcScreen', inputParams: screenProfile, clientId: clientId, inputType: 'body', response: response });

			return response;


		} catch (error) {
			throw error;
		}

	}


	function sendRequest(screenProfile, callSystemName) {
		return new Promise((resolve, reject) => {
			var options = {
				method: 'POST',
				url: 'https://api.fdbcloudconnector.com/CC/api/v1_4/Screen',
				headers:
				{
					'Content-Type': 'application/json',
					Accept: 'application/json',
					Authorization: 'SHAREDKEY 1601:7xIMo3PMT74uhklJnz9+CRZJibn/MHp12P7TP7K6vqM='
				},
				body:
				{
					DDIScreenRequest: {
						ProspectiveOnly: false,
						SeverityFilter: "3"
					},
					CallContext: { CallSystemName: callSystemName },
					DAScreenRequest: { PerformInactiveScreening: true },
					ScreenProfile: screenProfile
					// {
					// 	ScreenDrugs:
					// 		[{
					// 			Prospective: true,
					// 			DrugID: '152861',
					// 			DrugConceptType: 'DispensableDrug'
					// 		},
					// 		{
					// 			Prospective: true,
					// 			DrugID: '154320',
					// 			DrugConceptType: 'DispensableDrug'
					// 		}],
					// 	ScreenAllergens:
					// 		[{
					// 			AllergenID: '47703008',
					// 			AllergenConceptType: 'SNOMED',
					// 			Reaction: 'Rash'
					// 		}]
					// }
				}
				,
				json: true
			};

			request(options, function (error, response, body) {
				if (error) reject(error);
				else
					resolve(body);
			});
		});
	}

	fdbService.remoteMethod('ndcScreen', {
		accepts: [{
			arg: 'options',
			type: 'object',
			http: 'optionsFromRequest'
		}, {
			arg: 'screenProfile',
			type: 'object',
			required: true
		},
		{
			arg: 'clientId',
			type: 'string',
			required: true
		}], returns: {
			arg: 'data',
			type: 'object',
			root: true
		},
		http: {
			verb: 'post',
			path: '/screen/:clientId'
		}
	});
}
