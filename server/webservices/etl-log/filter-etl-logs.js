module.exports = function (EtlLog) {

    EtlLog.filterEtlLogs = async function (options, skip, limit,accountId,programId,searchString,searchField,from,to) {
        try {
            const accessToken = options && options.accessToken;
            const userId = accessToken && accessToken.userId;
            const userData = await EtlLog.app.models.user.findById(userId);
            const filter = {
                where: { and: [{ clientId: userData.clientId }] }, skip, limit
              };
            accountId && accountId.trim() !== '' && accountId !== 'null' && filter.where.and.push({ planId:accountId});
            programId && programId.trim() !== '' && programId !== 'null' && filter.where.and.push({ programId:programId});
            searchString && searchString.trim() !== '' && searchString.trim() !== 'undefined' && searchField.trim() != 'undefined'
				&& (() => {
					const searchFilter = {};
                    searchFilter[searchField] = { like: new RegExp(searchString.trim() + '$') };
					filter.where.and.push(searchFilter);
				})();
            from && from.trim() !== '' && from !== 'null' && filter.where.and.push({ createdAt: { gte: new Date(from) } });
            to && to.trim() !== '' && to !== 'null' && filter.where.and.push({ createdAt: { lte: new Date(to) } });
            const etlLogs = await EtlLog.find(filter)
            //const etlLogs = await EtlLog.find({ where: { and: [{ clientId: userData.clientId }] } }, skip, limit);
            const count = await EtlLog.count({ clientId: userData.clientId });
            return { etlLogs, count };
        }
        catch (error) {
            throw error;
        }

    }

    EtlLog.remoteMethod('filterEtlLogs', {
        accepts: [{ arg: 'options', type: 'object', http: 'optionsFromRequest' }, 
        { arg: 'skip', type: 'number', required: true },
        { arg: 'limit', type: 'number', required: true },
        { arg: 'accountId', type: 'string', required: false }, 
        { arg: 'programId', type: 'string', required: false },
        { arg: 'searchString', type: 'string', required: false },
        { arg: 'searchField', type: 'string', required: false },
        { arg: 'from', type: 'string',description: 'created from',required: false},
        { arg: 'to',type: 'string',description: 'created to',required: false}],
        returns: { arg: 'data', type: 'object', root: true },
        http: { path: '/filter', verb: 'GET' }
    })

}