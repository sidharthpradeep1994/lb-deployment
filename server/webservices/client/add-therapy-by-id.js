module.exports = function (client) {
	client.addTherapyById = async function (options, clientId, therapyId) {
		try {
			const token = options && options.accessToken;
			const userId = token && token.userId;
			const clientIns = await client.findById(clientId);
			const index = clientIns.therapies.findIndex((tId) => tId === therapyId);
			index > -1 ? clientIns.therapies.splice(index, 1) : clientIns.therapies.push(therapyId);
			return clientIns.updateAttributes({ therapies: clientIns.therapies }, options);



		} catch (error) {
			throw error;
		}

	}

	client.remoteMethod('addTherapyById', {
		accepts: [{
			arg: 'options',
			type: 'object',
			http: 'optionsFromRequest'
		}, {
			arg: 'clientId',
			type: 'string',
			description: 'primary id of client',
			required: true
		},
		{
			arg: 'therapyId',
			type: 'string',
			description: 'primary id of therapy',
			required: true
		}], returns: {
			arg: 'data',
			type: 'object',
			root: true
		},
		http: {
			verb: 'patch',
			path: '/:clientId/therapy/:therapyId'
		}
	});
}
