module.exports = function (Client) {
    const Roles = require('../../lib/models/roles');
    const AppError = require('../../utils/app-error');
    Client.filterClients = async function (options, searchString, skip, limit) {
        try {
            const accessToken = options && options.accessToken;
            const userId = accessToken && accessToken.userId;
            const userData = await Client.app.models.user.findById(userId);

            if (userData.role !== Roles.SUPERADMIN) {
                throw new AppError('Forbidden access', 403)
            }
            const filter = { where: { }, skip: skip, limit: limit };
            searchString && searchString.trim() && searchString.trim() !== 'undefined' && (filter.where = { name: { like: searchString } });
            const clients = await Client.find(filter);
            const count = await Client.count(filter.where);
            return { clients, count };
        }
        catch (error) {
            throw error;
        }
    }


    Client.remoteMethod('filterClients', {
        accepts: [
            { arg: 'options', type: 'object', http: 'optionsFromRequest' },
            { arg: 'searchString', type: 'string', required: false },
            { arg: 'skip', type: 'number', required: false },
            { arg: 'limit', type: 'number', required: false }
        ], returns: {
            arg: 'data', type: 'object', root: true
        },
        http: {
            path: '/filter', verb: 'GET',
        }
    });


}