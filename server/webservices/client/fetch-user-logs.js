module.exports = function (Client) {
    const Roles = require('../../lib/models/roles');
    Client.fetchUserLogs = async function (options) {
        try {
            /* const token = options && options.accessToken; */
            let clientData = await Client.find();
            //console.log(data);
            let userClientData=[];
            for(let i=0;i<clientData.length;i++)
            {
                const filter = { where: {clientId:clientData[i].id}};
                console.log('client id ',clientData[i].id);
                const userData= await Client.app.models.user.find(filter);
                //console.log(userData);
                for(let j=0;j<userData.length;j++)
                {
                        userClientData.push({userName:`${userData[j].firstName} ${userData[j].lastName}`,email:userData[j].email,clientName:clientData[i].name});         
                }                
            }
            console.log(userClientData);
            return { userClientData,count:userClientData.length};
        } catch (error) {
            throw error;
        }

    }

    Client.remoteMethod('fetchUserLogs', {
        accepts: [{
            arg: 'options',
            type: 'object',
            http: 'optionsFromRequest'
        }], returns: {
            arg: 'data',
            type: 'object',
            root: true
        },
        http: {
            verb: 'get',
            path: '/filterlogs'
        }
    });
}
