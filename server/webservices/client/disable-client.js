module.exports = function (Client) {
    const Roles = require('../../lib/models/roles');
    const AppError = require('../../utils/app-error');
    Client.disableClient = async function (options, clientId, disabled, comment) {
        try {
            const accessToken = options && options.accessToken;
            const userId = accessToken && accessToken.userId;
            const userData = await Client.app.models.user.findById(userId);
            if (userData.role !== Roles.SUPERADMIN) {
                throw new AppError('Forbidden action', 403, true)
            }
            const clientData = await Client.findById(clientId);
            return await clientData.updateAttributes({ disabled, comment },options);
        } catch (error) {
            throw error;
        }


    }

    Client.remoteMethod('disableClient', {
        accepts: [
            { arg: 'options', type: 'object', http: 'optionsFromRequest' },
            { arg: 'clientId', type: 'string', required: true },
            { arg: 'disabled', type: 'boolean', required: true },
            { arg: 'comment', type: 'string', required: true },
        ], returns: { arg: 'data', type: 'object', root: true },
        http: { path: '/activate', verb: 'POST' }
    })

}