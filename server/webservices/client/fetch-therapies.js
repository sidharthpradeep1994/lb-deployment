module.exports = function (client) {
	client.fetchTherapies = async function (options, clientId) {
		try {
			const token = options && options.accessToken;
			const userId = token && token.userId;
			const clientIns = await client.findById(clientId);
			return await client.app.models.therapy.find({ where: { and: [{ id: { inq: clientIns.therapies } }] }, skip: 0, limit: 0 });


		} catch (error) {
			throw error;
		}

	}

	client.remoteMethod('fetchTherapies', {
		accepts: [{
			arg: 'options',
			type: 'object',
			http: 'optionsFromRequest'
		}, {
			arg: 'clientId',
			type: 'string',
			description: 'primary id of client',
			required: true
		}], returns: {
			arg: 'data',
			type: 'object',
			root: true
		},
		http: {
			verb: 'get',
			path: '/:clientId/therapies'
		}
	});
}
