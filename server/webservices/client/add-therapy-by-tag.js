module.exports = function (client) {
	const removeDup = require('../../lib/helpers/remove-array-duplicates');
	client.addTherapyByTag = async function (options, clientId, tagId) {
		try {
			const token = options && options.accessToken;
			const userId = token && token.userId;
			const therapies = await client.app.models.therapy.find({ tags: { inq: tagId }, fields: ['id'] });
			let therapyIds = therapies.map(tp => tp.id);
			therapyIds = removeDup(therapyIds);
			const clientIns = await client.findById(clientId);
			return await clientIns.updateAttributes({ therapies: therapyIds }, options);

		} catch (error) {
			throw error;
		}

	}




	client.remoteMethod('addTherapyByTag', {
		accepts: [{
			arg: 'options',
			type: 'object',
			http: 'optionsFromRequest'
		}, {
			arg: 'clientId',
			type: 'string',
			description: 'primary id of the client',
			required: true
		}, {
			arg: 'tagId',
			type: 'string',
			description: 'primary id of tag, all the therapies assigned to this tag will be added to the client',
			required: true
		}], returns: {
			arg: 'data',
			type: 'object',
			root: true
		},
		http: {
			verb: 'post',
			path: '/:clientid/therapy/tags/:tagId'
		}
	});
}
