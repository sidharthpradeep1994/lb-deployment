module.exports = function (LogDump) {
    

    LogDump.filterLogs = async function (options, type, skip, limit, from, to) {
        try {
            const accessToken = options && options.accessToken;
            const userId = accessToken && accessToken.userId;
            const filter = {
                where: {
                    and: [
                        { timestamp: { gte: from } },
                        { timestamp: { lte: to } }
                    ]
                }, skip: skip, limit: limit
            };
            let logs = [];
            let count = 0;
            switch (type) {
                case 'logs':
                    logs = await LogDump.find(filter);
                    count = await LogDump.count(filter.where);
                    return { logs, count };
                    break;
                case 'error':
                    logs = await LogDump.app.models.errorLogDump.find(filter);
                    count = await LogDump.app.models.errorLogDump.count(filter.where);
                    return { logs, count };
                    break;
                default:
                    throw new Error('Invalid option')
                    break;
            }
        } catch (error) {
            throw error;

        }
    }


    LogDump.remoteMethod('filterLogs', {
        accepts: [
            { arg: 'options', type: 'object', http: 'optionsFromRequest' },
            { arg: 'type', type: 'string' },
            { arg: 'skip', type: 'number' },
            { arg: 'limit', type: 'number' },
            { arg: 'from', type: 'date' },
            { arg: 'to', type: 'date' }
        ],
        returns: { arg: 'data', type: 'object', root: true },
        http: {
            path: '/filter/:type',
            verb: 'GET'
        }
    })

}