module.exports = function (taskBoard) {
	
	taskBoard.assignTask = async function (options,assignedUserId,title,description) {
		try {
            console.log('assignedUserId ',assignedUserId);
            console.log('title ',title);
            console.log('description ',description);
			const token = options && options.accessToken;
			const userId = token && token.userId;
            const user = await taskBoard.app.models.user.findById(userId);
            const taskInput={title:title,description:description}
            if(user.role=='clientAdmin')
            {
              const userTaskboard=await taskBoard.app.models.taskBoard.findOne({where: {userId:assignedUserId}});
              console.log(userTaskboard);
              let taskboardOut;  
              if(userTaskboard==null)
              {
               let taskBoardStructure= {
					userId:assignedUserId,
					clientId:user.clientId,
					taskboard:[
						{
							"title": "To Dos",
							"class": "todos",
							"tasks":[]
						},
						{
							"title": "In Progress",
							"class": "inprogress",
							"tasks":[]
						},
						{
							"title": "Completed",
							"class": "completed",
							"tasks":[]
						},
						{
							"title": "On Hold",
							"class": "onhold",
							"tasks":[]
						},
						{
							"title": "Trash",
							"class": "trash",
							"tasks":[]
						}
						
					]
				}
                  for(let i=0;i<taskBoardStructure['taskboard'].length;i++)
                  {
                      if(taskBoardStructure['taskboard'][i].title=='To Dos')
                      {
                        taskBoardStructure['taskboard'][i]['tasks'].push(taskInput);
                      }
                  }
                 taskboardOut  = await taskBoard.create(taskBoardStructure);
              }
              else{
                for(let i=0;i<userTaskboard['taskboard'].length;i++)
                {
                    if(userTaskboard['taskboard'][i].title=='To Dos')
                    {
                        userTaskboard['taskboard'][i]['tasks'].push(taskInput);
                    }
                }
                 taskboardOut= await userTaskboard.updateAttribute('taskboard', userTaskboard.taskboard)
              }
                return  taskboardOut;
            }
            else {
				return {
					success: false, message: 'Cannot assign task'
                } 
            }
			
		} catch (error) {
			throw error;
		}

	}

	taskBoard.remoteMethod('assignTask', {
		accepts: [{
			arg: 'options',
			type: 'object',
			http: 'optionsFromRequest'
		},
		{
			arg: 'assignedUserId',
			type: 'string',
			required: true
		},
		{
			arg:'title',
			type :'string',
			required:true
		},
		{
			arg: 'description',
			type: 'string',
			required: true
		}
		], returns: {
			arg: 'data',
			type: 'object',
			root: true
		},
		http: {
			verb: 'post',
			path: '/assign'
		}
	});
}