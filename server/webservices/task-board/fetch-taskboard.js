module.exports = function (taskBoard) {
	
	taskBoard.fetchtaskBoards = async function (options,assignedUserId) {
		try {
			const token = options && options.accessToken;
			const userId = token && token.userId; 
			const user = await taskBoard.app.models.user.findById(userId);
			let taskBoardOutput=null;
			if(assignedUserId && assignedUserId.trim() !== '' && assignedUserId !== 'undefined')
			{
				taskBoardOutput = await taskBoard.findOne({where:{userId:assignedUserId}})
			}
			else{
				taskBoardOutput = await taskBoard.findOne({where:{userId:userId}})
			}
			
			if(taskBoardOutput==null)
			{
				console.log('user doesnot have taskboard');
				return {
					userId:userId,
					clientId:user.clientId,
					taskboard:[
						{
							"title": "To Dos",
							"class": "todos",
							"tasks":[]
						},
						{
							"title": "In Progress",
							"class": "inprogress",
							"tasks":[]
						},
						{
							"title": "Completed",
							"class": "completed",
							"tasks":[]
						},
						{
							"title": "On Hold",
							"class": "onhold",
							"tasks":[]
						},
						{
							"title": "Trash",
							"class": "trash",
							"tasks":[]
						}
						
					]
				}
			}
			console.log(taskBoardOutput);
			return taskBoardOutput;
            
		} catch (error) {
			throw error;
		}

	}

	taskBoard.remoteMethod('fetchtaskBoards', {
		accepts: [{
			arg: 'options',
			type: 'object',
			http: 'optionsFromRequest'
		},
		{
			arg: 'assignedUserId',
			type: 'string',
			required: false
		}
		], returns: {
			arg: 'data',
			type: 'object',
			root: true
		},
		http: {
			verb: 'get',
			path: '/fetch'
		}
	});
}