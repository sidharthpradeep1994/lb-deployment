module.exports = function (taskBoard) {
	
	taskBoard.createUpdateTaskboard = async function (options,taskboard) {
		try {
			const token = options && options.accessToken;
			const userId = token && token.userId;
			const user = await taskBoard.app.models.user.findById(userId);
			const userTaskboard = await taskBoard.findOne({where:{userId:userId}});
			if(userTaskboard==null)
			{
				console.log('user doesnot have taskboard');
				return await taskBoard.create({
					userId:userId,
					clientId:user.clientId,
					taskboard:taskboard
				});
			}
			else
			{
				console.log(userTaskboard);
				return await userTaskboard.updateAttribute('taskboard', taskboard);
			}
            
		} catch (error) {
			throw error;
		}
	}

	taskBoard.remoteMethod('createUpdateTaskboard', {
		accepts: [{
			arg: 'options',
			type: 'object',
			http: 'optionsFromRequest'
		},
		{
			arg: 'taskboard',
			type:"array",
			required: true
		}
		], returns: {
			arg: 'data',
			type: 'object',
			root: true
		},
		http: {
			verb: 'post',
			path: '/updateboard'
		}
	});
}