module.exports = function (user) {
    const Roles = require('../../lib/models/roles');
    const AppError = require('../../utils/app-error');
    user.filterUsersForLogs = async function (
      options,
      skip,
      limit,
      searchField,
      searchString,
      clientId
    ) {
      try {
        const token = options && options.accessToken;
        const userId = token && token.userId;
        const userData = await user.findById(userId);
        const filter = {
          where: {
            and: [
              { deleted: { neq: true } }
            ]
          },
          skip: skip,
          limit: limit
          // fields: ['firstName', 'middleName', 'lastName', 'primaryLanguage', 'contractName', '']
        };
        if (
          userData.role !== Roles.SUPERADMIN &&
          userData.role !== Roles.CLIENTADMIN && userData.role !== Roles.ADMIN
        ) {
          return { data: [], count: 0, message: 'Not Authorized' };
        }
        if (userData.role == Roles.CLIENTADMIN) {
          filter.where.and.push({
            //clientId: user.clientId
            clientId: userData.clientId
          });
  
          filter.where.and.push({
            role: { inq: [Roles.ADMIN, Roles.CLINICIAN, Roles.WORKINGCLINICIAN] }
          })
  
        }
        if (userData.role === Roles.ADMIN) {
  
          filter.where.and.push({
            role: { inq: [Roles.CLINICIAN, Roles.WORKINGCLINICIAN] }
          });
  
        }
  
        if (userData.role === Roles.SUPERADMIN) {
          if (clientId && clientId.trim() !== '' && clientId !== 'null') filter.where.and.push(...[{ clientId: clientId }])
          else filter.where.and.push({ role: Roles.SUPERADMIN });
        }
        searchString &&
          searchString.trim() !== '' &&
          searchString.trim() !== 'undefined' &&
          searchField.trim() != 'undefined' &&
          searchString != 'null' &&
          (() => {
            const searchFilter = {};
            searchFilter[searchField] = {
              like: new RegExp(searchString.trim() + '$')
            };
            filter.where.and.push(searchFilter);
          })();
        filter.where.and.length === 0 && (filter.where.and = undefined);
        const data = await user.find(filter);
        const count = await user.count(filter.where);
        return { data: data, count: count };
      } catch (error) {
        throw error;
      }
    };
  
    user.remoteMethod('filterUsersForLogs', {
      accepts: [
        {
          arg: 'options',
          type: 'object',
          http: 'optionsFromRequest'
        },
        {
          arg: 'skip',
          type: 'number',
          default: 0,
          required: false
        },
        {
          arg: 'limit',
          type: 'number',
          default: 20,
          required: false
        },
        {
          arg: 'searchField',
          type: 'string',
          default: 'firstName',
          required: false
        },
        {
          arg: 'searchString',
          type: 'string',
          default: '',
          required: false
        },
        {
          arg: 'clientId',
          type: 'string',
          default: '',
          required: false
        }
      ],
      returns: {
        arg: 'data',
        type: 'object',
        root: true
      },
      http: {
        verb: 'get',
        path: '/filterForLogs'
      }
    });
  };
  