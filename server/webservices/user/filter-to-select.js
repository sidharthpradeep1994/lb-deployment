module.exports = function (User) {
    User.filterToSelect = async function (options, clientId, fixedFilters, variableFilters, strictFilter) {

        try {
            const token = options && options.accessToken;
            const userId = token && token.userId;

            const filters = [{ clientId: clientId }, { active: { neq: false } }, { deleted: { neq: true } }];
            const filterType = strictFilter ? 'and' : 'or';
            const allowedVariableFilters = ['firstName', 'lastName', 'middleName',
                'email', 'username', 'primaryPhoneNumber', 'secondaryPhoneNumber'];

            fixedFilters && fixedFilters.languages && fixedFilters.languages.length && filters.push({ languages: { inq: fixedFilters.languages } });
            fixedFilters && fixedFilters.roles && fixedFilters.roles.length && filters.push({ role: { inq: fixedFilters.roles } });

            if (fixedFilters.userGroups && fixedFilters.userGroups.length) {
                const members = await User.app.models.groupMember.find({ where: { groupId: { inq: fixedFilters.userGroups } }, filter: ['userId'] });
                const memberIds = members.map(member => member.userId);
                filters.push({ id: { inq: memberIds } });
            }
            variableFilters.map(filter => allowedVariableFilters.includes(filter.property) && (() => {
                const filterObj = {};
                filterObj[filter.property] = filter.value;
                filters.push(filterObj)
            })());
            console.log(filters)
            const selectedUsers = await User.find({ where: { and: filters }, fields: ['id', 'username', 'firstName', 'lastName'] });
            const totalUsers = await User.count({ clientId: clientId ,  active: { neq: false } , deleted: { neq: true }});
            return { selectedUsers, totalUsers };
        } catch (error) {
            throw error;
        }

    }

    User.remoteMethod('filterToSelect', {
        accepts: [{
            arg: 'options',
            type: 'object',
            http: 'optionsFromRequest'
        },
        {
            arg: 'clientId',
            type: 'string',
            required: true
        },
        {
            arg: 'fixedFilters',
            type: 'object',
            required: true
        }, {
            arg: 'variableFilters',
            type: 'array',
            required: true
        },
        {
            arg: 'strictFilter',
            type: 'boolean',
            required: true
        }],
        returns: {
            arg: 'data',
            type: 'object',
            root: true
        },
        http: {
            verb: 'post',
            path: '/:clientId/select'
        }
    })
}