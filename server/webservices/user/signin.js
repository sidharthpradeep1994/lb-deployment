module.exports = function (User) {

	const { createHash, generateRandomPassword } = require('../../utils/password-generation');
	const AppError = require('../../utils/app-error');
	const Roles = require('../../lib/models/roles');
	User.signin = async function (options, uniqueIdentifier, password) {
		try {
			const ttl = 60 * 30;
			const userDetails = await User.findOne({ where: { or: [{ username: uniqueIdentifier }, { email: uniqueIdentifier }], and: [{ active: { neq: false } }, { deleted: { neq: true } }] }, skip: 0, limit: 0, fields: ['id', 'username', 'email', 'clientId', 'role'], order: '' });
			if (!userDetails) {
				throw new AppError('Login Failed', 401);
			}
	
			if (userDetails && userDetails.id) {
				if (userDetails.role !== Roles.SUPERADMIN) {
					const clientData = await User.app.models.client.findById(userDetails.clientId);
					console.log(clientData)
					if (clientData.disabled) {
						throw new AppError('Unauthorized access', 401, true);
					}
				}
				const otpData = await User.app.models.oneTimePassword.
					findOne({ where: { and: [{ active: true }, { used: false }, { accessType: 'setPassword' }, { userId: userDetails.id }, { otp: createHash(password) }] }, skip: 0, limit: 0, fields: [], order: '' });

				if (otpData && otpData.otp) {
					return {
						id: password,
						accessType: 'setPassword',
						userId: userDetails.id
					};
				}
			}
			if (userDetails && userDetails.username) {
				uniqueIdentifier = userDetails.username;
				return await User.login({ username: uniqueIdentifier, password, ttl }, 'user');
			} else if (userDetails && user.email) {
				return await User.login({ email: uniqueIdentifier, password, ttl }, 'user');
			}

		} catch (error) {
			throw error;
		}

	}

	User.remoteMethod('signin', {
		accepts: [{
			arg: 'options',
			type: 'object',
			http: 'optionsFromRequest'
		}, {
			arg: 'uniqueIdentifier',
			type: 'string',
			required: true
		},
		{
			arg: 'password',
			type: 'string',
			required: true
		}], returns: {
			arg: 'data',
			type: 'object',
			root: true
		},
		http: {
			verb: 'post',
			path: '/signin'
		}
	});
}
