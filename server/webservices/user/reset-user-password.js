module.exports = function (User) {
	User.resetUserPassword = async function (options, username, otp, password) {
		try {
			const userData = await User.findOne({ where: { and: [{ username: username }] }, skip: 0, limit: 0, fields: [], order: '' });
			const otpData = await User.app.models.oneTimePassword.findOne({
				where: {
					and: [
						{ userId: userData.id },
						{ active: true }, {
							used: false
						}, {
							accessType: 'resetPassword'
						}]
				}, skip: 0, limit: 0, fields: [], order: ''
			});
			if (otpData.otp) {
				await userData.updateAttributes({ password: password, passwordResetAt: new Date() }, options);
				await otpData.updateAttributes({ active: false, used: true }, options);

				return { success: true, message: 'Your password is updated. Please login' }
			} else {
				return {
					success: false, message: 'Password reset failed. Try again'
				}
			}

		} catch (error) {
			throw error;
		}

	}

	User.remoteMethod('resetUserPassword', {
		accepts: [{
			arg: 'options',
			type: 'object',
			http: 'optionsFromRequest'
		}, {
			arg: 'username',
			type: 'string',
			required: true
		},
		{
			arg: 'otp',
			type: 'string',
			required: true
		},
		{
			arg: 'password',
			type: 'string',
			required: true
		}
		], returns: {
			arg: 'data',
			type: 'object',
			root: true
		},
		http: {
			verb: 'post',
			path: '/forgot-password'
		}
	});
}
