module.exports = function (User) {
	User.fetchResetQuestions = async function (options, username) {
		try {
			const userData = await User.findOne({ where: { and: [{ username: username }] }, skip: 0, limit: 0, fields: [], order: '' });
			if (userData && userData.securityQuestions) {
				const securityQuestions = userData.securityQuestions.map(sq => {
					sq.answer = null;
					return sq;
				});
				return { securityQuestions: securityQuestions };
			} else {
				return { message: 'No account found with this username' };
			}
		} catch (error) {
			throw error;
		}

	}

	User.remoteMethod('fetchResetQuestions', {
		accepts: [{
			arg: 'options',
			type: 'object',
			http: 'optionsFromRequest'
		}, {
			arg: 'username',
			type: 'string',
			required: true
		}], returns: {
			arg: 'data',
			type: 'object',
			root: true
		},
		http: {
			verb: 'get',
			path: '/reset/questions'
		}
	});
}
