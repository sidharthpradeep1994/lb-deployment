module.exports = function (LoginLog) {
    const AppError = require('../../utils/app-error');
    LoginLog.filterLoginLogs = async function (options, userId, skip, limit) {
        try {
            const accessToken = options && options.accessToken;
            const id = accessToken && accessToken.userId;
            const userData = await LoginLog.app.models.user.findById(id);
            if (userData.role !== roles.SUPERADMIN && userData.role !== roles.CLIENTADMIN) {
                throw new AppError('Action not permitted by user with this role', 401);
            }

            const loginLogs = await LoginLog.find({
                where: { and: [{userId: userId}] }, skip: skip, limit: limit, order: 'createdAt DESC'
            });
            const count = await LoginLog.count({ clientId: userData.clientId });
            return { loginLogs, count };
        }
        catch (error) {
            throw error;
        }
    }

    LoginLog.remoteMethod('filterLoginLogs', {
        accepts: [{
            arg: 'options', type: 'object', http: 'optionsFromRequest'
        },
        {
            arg: 'userId', type: 'string', required: false
        },
        {
            arg: 'skip', type: 'number', required: true
        },
        {
            arg: 'limit', type: 'number', required: true
        }], returns: { arg: 'data', root: true, type: 'array' },
        http: {
            path: '/:userId/filter', verb: 'get'
        }
    })
}