module.exports = function (User) {

    const Roles = require('../../lib/models/roles');

    User.validatePasswordReset = async function (options) {

        try {

            const accessToken = options && options.accessToken;
            const userId = accessToken && accessToken.userId;

            const userData = await User.findById(userId);


            switch (userData.role) {
                case Roles.SUPERADMIN:
                case Roles.CLIENTADMIN:
                    if (!userData.passwordResetAt || ((new Date().getTime() - new Date(userData.passwordResetAt).getTime()) > 2592000000) /*2592000000*/) {
                        return {
                            forceReset: true,
                            userId: userData.id,
                            lastResetAt: userData.passwordResetAt
                        };
                    }
                    break;

                default:
                    if (!userData.passwordResetAt || (new Date().getTime() - new Date(userData.passwordResetAt).getTime()) > 5184000000) {
                        return {
                            forceReset: true,
                            userId: userData.id,
                            lastResetAt: userData.passwordResetAt
                        };
                    }
                    break;
            }

            return {
                forceReset: false,
                userId: userData.id,
                lastResetAt: userData.passwordResetAt
            };


        }
        catch (error) {
            throw error;
        }

    }


    User.remoteMethod('validatePasswordReset', {
        accepts: [
            { arg: 'options', type: 'object', http: 'optionsFromRequest' }
        ], returns: {
            arg: 'data', type: 'object', root: true
        },
        http: {
            path: '/verify-reset', verb: 'get'
        }
    });



}