module.exports = function (user) {
  const Roles = require('../../lib/models/roles');
  const AppError = require('../../utils/app-error');
  user.filterUsers = async function (
    options,
    skip,
    limit,
    userRole,
    status,
    from,
    to,
    searchField,
    searchString,
    clientId
  ) {
    try {
      const token = options && options.accessToken;
      const userId = token && token.userId;
      const userData = await user.findById(userId);
      const filter = {
        where: {
          and: [
            { deleted: { neq: true } }
          ]
        },
        skip: skip,
        limit: limit
        // fields: ['firstName', 'middleName', 'lastName', 'primaryLanguage', 'contractName', '']
      };
      if (
        userData.role !== Roles.SUPERADMIN &&
        userData.role !== Roles.CLIENTADMIN && userData.role !== Roles.ADMIN
      ) {
        return { data: [], count: 0, message: 'Not Authorized' };
      }
      if (userData.role == Roles.CLIENTADMIN) {
        filter.where.and.push({
          //clientId: user.clientId
          clientId: userData.clientId
        });

        filter.where.and.push({
          role: { inq: [Roles.ADMIN, Roles.CLINICIAN, Roles.WORKINGCLINICIAN] }
        })

      }
      if (userData.role === Roles.ADMIN) {

        filter.where.and.push({
          role: { inq: [Roles.CLINICIAN, Roles.WORKINGCLINICIAN] }
        });

      }

      if (userData.role === Roles.SUPERADMIN) {
        if (clientId && clientId.trim() !== '' && clientId !== 'null') filter.where.and.push(...[{ clientId: clientId }, { role: Roles.CLIENTADMIN }])
        else filter.where.and.push({ role: Roles.SUPERADMIN });
      }
      searchString &&
        searchString.trim() !== '' &&
        searchString.trim() !== 'undefined' &&
        searchField.trim() != 'undefined' &&
        searchString != 'null' &&
        (() => {
          const searchFilter = {};
          searchFilter[searchField] = {
            like: new RegExp(searchString.trim() + '$')
          };
          filter.where.and.push(searchFilter);
        })();
        if(status=='active')
        {
        filter.where.and.push({ active:true});
        }
        else if(status=='deactive')
        {
        filter.where.and.push({ active:false});
        }
        userRole && userRole.trim() !== '' && userRole !== 'null' && filter.where.and.push({ role:userRole});
        from && from.trim() !== '' && from !== 'null' && filter.where.and.push({ createdAt: { gte: new Date(from) } });
        to && to.trim() !== '' && to !== 'null' && filter.where.and.push({ createdAt: { lte: new Date(to) } });
      filter.where.and.length === 0 && (filter.where.and = undefined);
      const data = await user.find(filter);
      const count = await user.count(filter.where);
      return { data: data, count: count };
    } catch (error) {
      throw error;
    }
  };

  user.remoteMethod('filterUsers', {
    accepts: [
      {
        arg: 'options',
        type: 'object',
        http: 'optionsFromRequest'
      },
      {
        arg: 'skip',
        type: 'number',
        default: 0,
        required: false
      },
      {
        arg: 'limit',
        type: 'number',
        default: 20,
        required: false
      },
      {
        arg: 'userRole',
        type: 'string',
        default: '',
        required: false
      },
      {
        arg: 'status',
        type: 'string',
        default: '',
        required: false
      },
      { 
        arg: 'from',
        type: 'string',
        description: 'created from',
        required: false
      },
      { 
        arg: 'to',
        type: 'string',
        description: 'created to',
        required: false
      },
      {
        arg: 'searchField',
        type: 'string',
        default: 'firstName',
        required: false
      },
      {
        arg: 'searchString',
        type: 'string',
        default: '',
        required: false
      },
      {
        arg: 'clientId',
        type: 'string',
        default: '',
        required: false
      }
    ],
    returns: {
      arg: 'data',
      type: 'object',
      root: true
    },
    http: {
      verb: 'get',
      path: '/filter'
    }
  });
};
