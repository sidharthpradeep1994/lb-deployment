module.exports = function (User) {
	const { generateRandomPassword, createHash } = require('../../utils/password-generation');
	User.userCreateOneTimePassword = async function (options, username, securityQuestions) {
		try {
			const userData = await User.findOne({
				where: { and: [{ username: username }] },
				skip: 0, limit: 0, fields: [], order: ''
			});
			let isValid = true;
			userData.securityQuestions.map((sq, index) => {
				isValid && sq.question === securityQuestions[index].question
					&& sq.answer === securityQuestions[index].answer;
			});
			const otp = generateRandomPassword();
			await User.app.models.oneTimePassword.create({ userId: userData.id, active: true, used: false, accessType: 'resetPassword', accessLevel: ['settings'], otp: createHash(otp) }, options);
			return {
				success: true,
				otp: otp,
				userId: userData.id,
				username: username
			};
		} catch (error) {
			throw error;
		}

	}

	User.remoteMethod('userCreateOneTimePassword', {
		accepts: [{
			arg: 'options',
			type: 'object',
			http: 'optionsFromRequest'
		}, {
			arg: 'username',
			type: 'string',
			required: true
		},
		{
			arg: 'securityQuestions',
			type: 'array',
			required: true
		}], returns: {
			arg: 'data',
			type: 'object',
			root: true
		},
		http: {
			verb: 'post',
			path: '/otp/reset'
		}
	});
}
