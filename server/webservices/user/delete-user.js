module.exports = function (User) {
    const Roles = require('../../lib/models/roles');
    const AppError = require('../../utils/app-error');

    User.deleteUser = async function (options, id, reasonToDelete) {
        try {
            const accessToken = options && options.accessToken;
            const userId = accessToken && accessToken.userId;

            const adminData = await User.findById(userId);

            const userData = await User.findById(id);

            switch (adminData.role) {
                case Roles.SUPERADMIN:

                    break;
                case Roles.CLIENTADMIN:
                    if (![Roles.ADMIN, Roles.CLINICIAN, Roles.WORKINGCLINICIAN].includes(userData.role)) {
                        throw new AppError('Unauthorized action for this user', 403)
                    }
                    break;
                case Roles.ADMIN:
                    if (![Roles.CLINICIAN, Roles.WORKINGCLINICIAN].includes(userData.role)) {
                        throw new AppError('Unauthorized action for this user', 403)
                    }
                    break;
                case Roles.CLINICIAN:

                    throw new AppError('Unauthorized action for this user', 403);
                    
                default:
                    throw new AppError('Unauthorized action for this user', 403)
            }
            return userData.updateAttributes({ deleted: true, reasonToDelete: reasonToDelete }, options);
        } catch (error) {
            throw error;
        }
    }

    User.remoteMethod('deleteUser',
        {
            accepts: [
                { arg: 'options', type: 'object', http: 'optionsFromRequest' },
                { arg: 'id', type: 'string', required: true },
                { arg: 'reasonToDelete', type: 'string', required: false }
            ], returns: {
                arg: 'data', type: 'object', root: true
            },
            http: {
                path: '/:id/delete',
                verb: 'patch'
            }
        }
    )
}