module.exports = function (user) {
	const roles = require('../../lib/models/roles');
	const removeRole = require('../../lib/services/user/remove-role');
	const createRole = require('../../lib/services/user/add-role');
	const AppError = require('../../utils/app-error');
	const sendEmail = require('../../utils/send-email');
	user.updateUser = async function (options, id, password, email, roleId, firstName, middleName, lastName, primaryAddress,
		secondaryAddress, primaryPhoneNumber, secondaryPhoneNumber, pharmacyLicenseNumber, gender, dob, ssn, photoId, clientId) {
		try {
			const token = options && options.accessToken;
			const userId = token && token.userId;
			const userData = await user.findById(userId);
			if (userData.role !== roles.SUPERADMIN && userData.role !== roles.CLIENTADMIN) {
				throw new AppError('Unauthorized', 401);
			}
			const role = selectRole(roleId);
			let userInstance = await user.findById(id);
			if (userInstance.role !== role) {
				await removeRole(role, userInstance.id, user, options);
				await createRole(role, userInstance.id, user, options);
				sendEmail(userData.email, 'support@adysas.com', 'Role Change', ' ', '<div>Hi,</div>  <div>Your request to update ' + userInstance.username + ' from ' + userInstance.role + ' to ' + role + ' is successfully processed.</div>')
					.then(send => console.log(send)).catch(err => console.log(err));

				sendEmail(userInstance.email, 'support@adysas.com', 'Change in User Role', ' ', '<div>Hi ' + userInstance.firstName + ',</div>  <div>Your role has been changed from ' + userInstance.role + ' to ' + role + '.</div>')
					.then(send => console.log(send)).catch(err => console.log(err));

			}
			userInstance = await userInstance.updateAttributes({

				role, primaryAddress,
				secondaryAddress, primaryPhoneNumber, secondaryPhoneNumber, pharmacyLicenseNumber, gender, ssn, photoId
			}, options)


			// userInstance = password && password != 'undefined' ? userInstance.updateAttribute('password', user.hashPassword(password)) : userInstance;

			return userInstance;
		} catch (error) {
			throw error;
		}

	}


	function isSuperAdmin(roleId) {
		return roleId == '1';
	}



	function selectRole(roleId) {
		switch (roleId) {
			case '1':
				return roles.SUPERADMIN;
				break;
			case '2':
				return roles.CLIENTADMIN
				break;
			case '3':
				return roles.CLINICIAN;
				break;

			default:
				return roles.CLINICIAN;
				break;
		}
	}



	user.remoteMethod('updateUser', {
		accepts: [{
			arg: 'options',
			type: 'object',
			http: 'optionsFromRequest'
		}, {
			arg: 'id',
			type: 'string',
			required: true
		}, {
			arg: 'password',
			type: 'string',
			required: false
		},
		{
			arg: 'email',
			type: 'string',
			required: true
		},
		{
			arg: 'roleId',
			type: 'string',
			required: true
		},
		{
			arg: 'firstName',
			type: 'string',
			required: true
		},
		{
			arg: 'middleName',
			type: 'string',
			required: true
		}, {
			arg: 'lastName',
			type: 'string',
			required: true
		},
		{
			arg: 'primaryAddress',
			type: 'address',
			required: true
		},
		{
			arg: 'secondaryAddress',
			type: 'address',
			required: false
		},
		{
			arg: 'primaryPhoneNumber',
			type: 'string',
			required: true
		},
		{
			arg: 'secondaryPhoneNumber',
			type: 'string',
			required: true
		},
		{
			arg: 'pharmacyLicenseNumber',
			type: 'string',
			required: false
		},
		{
			arg: 'gender',
			type: 'string',
			required: true
		},
		{
			arg: 'dob',
			type: 'string',
			required: true
		},
		{
			arg: 'ssn',
			type: 'string',
			required: false
		},
		{
			arg: 'photoId',
			type: 'string',
			required: false
		},
		{
			arg: 'clientId',
			type: 'string',
			required: false
		}], returns: {
			arg: 'data',
			type: 'object',
			root: true
		},
		http: {
			verb: 'patch',
			path: '/update'
		}
	});
}
