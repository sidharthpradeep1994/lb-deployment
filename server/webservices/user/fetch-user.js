module.exports = function (user) {
	const roles = require('../../lib/models/roles');
	const AppError = require('../../utils/app-error');
	user.fetchUser = async function (options, userId) {
		try {
			const token = options && options.accessToken;
			const uId = token && token.userId;
			const userData = await user.findById(uId);
			if (userData.role !== roles.SUPERADMIN && userData.role !== roles.ADMIN && userData.role !== roles.CLIENTADMIN && userData.id !== userId) {
				throw new AppError('Unauthorized', 401);
			}
			const responseData = await user.findById(userId);

			if (userData.role == roles.CLIENTADMIN) {
				if (![roles.ADMIN, roles.CLINICIAN, roles.WORKINGCLINICIAN].includes(responseData.role)) {
					throw new AppError('Unauthorized', 403);
				}			}
			if (userData.role == roles.ADMIN) {
				if (![roles.CLINICIAN, roles.WORKINGCLINICIAN].includes(responseData.role)) {
					throw new AppError('Unauthorized', 403);
				}
			}
			return responseData;;

		} catch (error) {
			throw error;
		}

	}

	user.remoteMethod('fetchUser', {
		accepts: [{
			arg: 'options',
			type: 'object',
			http: 'optionsFromRequest'
		}, {
			arg: 'userId',
			type: 'string',
			required: true
		}], returns: {
			arg: 'data',
			type: 'object',
			root: true
		},
		http: {
			verb: 'get',
			path: '/:userId/fetch'
		}
	});
}
