module.exports = function (User) {
	const roles = require('../../lib/models/roles');
	const sendEmail = require('../../utils/send-email');
	const { generateRandomPassword, createHash } = require('../../utils/password-generation');
	User.resetOneTimePassword = async function (options, userId) {
		try {
			const token = options && options.accessToken;
			const id = token && token.userId;
			const adminUserData = await User.findById(id);
			if (adminUserData.role == roles.SUPERADMIN || adminUserData.role == roles.CLIENTADMIN) {
				const otp = generateRandomPassword();
				await User.app.models.oneTimePassword.create({ userId: userId, active: true, used: false, accessType: 'setPassword', accessLevel: ['settings'], otp: createHash(otp) }, options);
				const userData = await User.findById(userId);
				sendEmail(userData.email, 'support@adysas.com', 'Reset OTP', ' ', '<div>Hi,</div>  <div>Your New One Time Password is ' + otp + '</div>')
					.then(send => console.log(send)).catch(err => console.log(err));
				return {
					success: true,
					otp: otp,
					userId: userId,
				};
			} else {
				throw new Error('Unauthorized action');
			}

		} catch (error) {
			throw error;
		}

	}

	User.remoteMethod('resetOneTimePassword', {
		accepts: [{
			arg: 'options',
			type: 'object',
			http: 'optionsFromRequest'
		}, {
			arg: 'userId',
			type: 'string',
			required: true
		}], returns: {
			arg: 'data',
			type: 'object',
			root: true
		},
		http: {
			verb: 'post',
			path: '/reset-otp'
		}
	});
}
