module.exports = function (User) {
	const { createHash } = require('../../utils/password-generation');
	User.setPassword = async function (options, userId, password, otp, securityQuestions) {
		try {
			const otpData = await User.app.models.oneTimePassword.findOne({
				where: {
					and: [
						{ userId: userId }, { otp: createHash(otp) }, { accessType: 'setPassword' }
					]
				}, skip: 0, limit: 0, fields: [], order: ''
			});
			if (!otpData || !otpData.otp) {
				return { message: 'Invalid password', success: false };
			}
			const userData = await User.findById(userId);
			securityQuestions = securityQuestions.map((sq) => {
				sq.answer = createHash(sq.answer.toLowerCase());
				return sq;
			});
			await userData.updateAttributes({ password: password, securityQuestions: securityQuestions, passwordResetAt: new Date() }, options);
			await otpData.updateAttributes({ active: false, used: true }, options);

			return { success: true, userData };



		} catch (error) {
			throw error;
		}

	}

	User.remoteMethod('setPassword', {
		accepts: [{
			arg: 'options',
			type: 'object',
			http: 'optionsFromRequest'
		}, {
			arg: 'userId',
			type: 'string',
			required: true
		},
		{
			arg: 'password',
			type: 'string',
			required: true
		},
		{
			arg: 'otp',
			type: 'string',
			required: true
		},
		{
			arg: 'securityQuestions',
			type: 'array',
			required: true
		}], returns: {
			arg: 'data',
			type: 'object',
			root: true
		},
		http: {
			verb: 'post',
			path: '/security/set'
		}
	});
}
