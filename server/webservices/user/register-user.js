module.exports = function (User) {

	const createUser = require('../../lib/services/user/create-user')(User);
	const addRole = require('../../lib/services/user/add-role');
	const roles = require('../../lib/models/roles');

	const AppError = require('../../utils/app-error');

	const sendEmail = require('../../utils/send-email');
	const { generateRandomPassword, createHash } = require('../../utils/password-generation');
	User.registerUser = async function (options, password, email, roleId, firstName, middleName, lastName, primaryAddress,
		secondaryAddress, primaryPhoneNumber, secondaryPhoneNumber, pharmacyLicenseNumber, gender, dob, ssn, photoId, clientId, languages, specialisations, userGroup) {
		try {

			const token = options && options.accessToken;
			const userId = token && token.userId;
			const userData = await User.findById(userId);
			if (userData.role !== roles.SUPERADMIN || !clientId) {
				clientId = userData.clientId;
			}
			const client = await User.app.models.client.findById(clientId);
			const role = selectRole(roleId);
			if (role === roles.SUPERADMIN || role === roles.CLIENTADMIN) {
				if (userData.role !== roles.SUPERADMIN) {
					throw new AppError('Action not permitted by user with this role', 401);
				}
			} else if (role === roles.ADMIN) {
				if (userData.role !== roles.SUPERADMIN && userData.role !== roles.CLIENTADMIN) {
					throw new AppError('Action not permitted by user with this role', 401);
				}
			} else {
				if (userData.role !== roles.SUPERADMIN && userData.role !== roles.CLIENTADMIN && userData.role !== roles.ADMIN) {
					throw new AppError('Action not permitted by user with this role', 401);
				}
			}
			if (!isSuperAdmin(roleId)) {
				password = null;
			} else {
				if (password == null) {
					throw new Error('Password is required');
				}
			}
			const user = await createUser(password, client.id, client.clientCode, email, role, firstName,
				middleName, lastName,
				primaryAddress,
				secondaryAddress, primaryPhoneNumber, secondaryPhoneNumber, pharmacyLicenseNumber, gender, dob, ssn, photoId, languages, specialisations);

			const otp = generateRandomPassword();
			const oneTimePassword = await User.app.models.oneTimePassword.create({
				userId: user.id, active: true, used: false, accessType: 'setPassword', accessLevel: ['settings'], otp: createHash(otp),
			// deleted	: true
			}, options);
			user.otp = otp;
			userGroup && addToUsergroup(userGroup, user.id, options);
			sendEmail(user.email, 'support@adysas.com', 'One Time Password', ' ', '<div>Welcome,</div>  <div>Your One Time Password is ' + otp + '</div>')
				.then(send => console.log(send)).catch(err => console.log(err));
			return user;
		} catch (error) {
			throw error;
		}


	}


	async function addToUsergroup(groupId, userId, options) {
		const userData = await User.findById(userId, { fields: ['id', 'username', 'firstName', 'middleName', 'lastName', 'role'] });
		const userGroup = await User.app.models.userGroup.findById(groupId);
		await User.app.models.groupMember.findOrCreate({
			where: {
				and: [{ clientId: userData.clientId }, {
					groupId: groupId
				}, {
					userId: userData.id
				}]
			}
		}, {
			userId: userData.id, groupId: userGroup.id, groupName: userGroup.name, userData: userData, admin: false, clientId: userData.clientId
		});

		userGroup.users.findIndex((userId) => userId == userData.id) < 0 && userGroup.users.push(userData.id) && userGroup.save(options);

		return userGroup;


	}


	function isSuperAdmin(roleId) {
		return roleId == '1';
	}



	function selectRole(roleId) {
		switch (roleId) {
			case '1':
				return roles.SUPERADMIN;
				break;
			case '2':
				return roles.CLIENTADMIN
				break;
			case '3':
				return roles.ADMIN
				break;
			case '':
				return roles.CLINICIAN;
				break;

			default:
				return roles.CLINICIAN;
				break;
		}
	}

	User.remoteMethod('registerUser', {
		accepts: [{
			arg: 'options',
			type: 'object',
			http: 'optionsFromRequest'
		},
		{
			arg: 'password',
			type: 'string',
			required: false
		},
		{
			arg: 'email',
			type: 'string',
			required: true
		},
		{
			arg: 'roleId',
			type: 'string',
			required: true
		},
		{
			arg: 'firstName',
			type: 'string',
			required: true
		},
		{
			arg: 'middleName',
			type: 'string',
			required: true
		}, {
			arg: 'lastName',
			type: 'string',
			required: true
		},
		{
			arg: 'primaryAddress',
			type: 'address',
			required: true
		},
		{
			arg: 'secondaryAddress',
			type: 'address',
			required: false
		},
		{
			arg: 'primaryPhoneNumber',
			type: 'string',
			required: true
		},
		{
			arg: 'secondaryPhoneNumber',
			type: 'string',
			required: true
		},
		{
			arg: 'pharmacyLicenseNumber',
			type: 'string',
			required: false
		},
		{
			arg: 'gender',
			type: 'string',
			required: true
		},
		{
			arg: 'dob',
			type: 'string',
			required: true
		},
		{
			arg: 'ssn',
			type: 'string',
			required: false
		},
		{
			arg: 'photoId',
			type: 'string',
			required: false
		},
		{
			arg: 'clientId',
			type: 'string',
			required: false
		},
		{
			arg: 'languages',
			type: 'array',
			required: false
		},
		{
			arg: 'specialisations',
			type: 'array',
			required: true
		}, {
			arg: 'userGroup',
			type: 'string',
			required: false
		}], returns: {
			arg: 'data',
			type: 'object',
			root: true
		},
		http: {
			verb: 'post',
			path: '/:roleId/register'
		}
	});
}
