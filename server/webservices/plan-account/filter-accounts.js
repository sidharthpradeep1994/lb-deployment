module.exports = function (PlanAccount) {

  PlanAccount.filterAccounts = async function (options, skip, limit, searchString, searchField,from,to) {
    try {
      const token = options && options.accessToken;
      const userId = token && token.userId;
      skip = skip ? skip : 0;
      limit = limit ? limit : 0;
      const user = await PlanAccount.app.models.user.findById(userId);
      const clientData = await PlanAccount.app.models.client.findById(user.clientId);
      const filter = {
        where: { and: [{ clientId: clientData.id }] }, skip, limit
      };
     /*  searchString && searchString !== 'undefined' && searchString.trim() !== '' && filter.where.and.push({
        name: {
          like: searchString
        }
      }); */
      searchString && searchString.trim() !== '' && searchString.trim() !== 'undefined' && searchField.trim() != 'undefined'
				&& (() => {
					const searchFilter = {};
					searchFilter[searchField] = { like: new RegExp(searchString.trim() + '$') };
					filter.where.and.push(searchFilter);

				})();
      from && from.trim() !== '' && from !== 'null' && filter.where.and.push({ createdAt: { gte: new Date(from) } });
      to && to.trim() !== '' && to !== 'null' && filter.where.and.push({ createdAt: { lte: new Date(to) } });
      console.log(filter.where.and)
      const planAccounts = await PlanAccount.find(filter);
      const count = await PlanAccount.count(filter.where);
      return { planAccounts, count };
    }
    catch (error) {
      throw error;
    }


  }

  PlanAccount.remoteMethod('filterAccounts', {
    accepts: [
      { arg: 'options', type: 'object', http: 'optionsFromRequest' },
      { arg: 'skip', type: 'number', required: false },
      { arg: 'limit', type: 'number', required: false },
      { arg: 'searchString', type: 'string', required: false },
      { arg: 'searchField', type: 'string', required: false },
      { arg: 'from', type: 'string',description: 'created from',required: false},
      { arg: 'to',type: 'string',description: 'created to',required: false}
    ],
    returns: {
      arg: 'data', root: true, type: 'object'
    },
    http: {
      verb: 'get', path: '/filter'
    }
  });


}
