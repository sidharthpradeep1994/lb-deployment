module.exports = function (PlanAccount) {
  PlanAccount.createAccount = async function (options, name, description) {
    try {
      const token = options && options.accessToken;
      const userId = token && token.userId;
      const userData = await PlanAccount.app.models.user.findById(userId);
      return await PlanAccount.create({ name, description, clientId: userData.clientId }, options);
    }
    catch (error) {
      throw new Error(error);
    }
  }

  PlanAccount.remoteMethod('createAccount', {
    accepts: [
      { arg: 'options', type: 'object', http: 'optionsFromRequest' },
      { arg: 'name', type: 'string', required: true },
      { arg: 'description', type: 'string', required: true }],
    returns: { arg: 'data', type: 'object', root: true },
    http: { path: '/save', verb: 'POST' }
  });
}
