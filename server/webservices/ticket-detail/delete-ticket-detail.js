module.exports = function (ticketDetail) {
	
	ticketDetail.deleteTicketDetail = async function (options) {
		try {
             return await ticketDetail.app.models.ticketDetail.destroyAll({ });
		} catch (error) {
			throw error;
		}

	}

	ticketDetail.remoteMethod('deleteTicketDetail', {
		accepts: [{
			arg: 'options',
			type: 'object',
			http: 'optionsFromRequest'
		},
		], returns: {
			arg: 'data',
			type: 'object',
			root: true
		},
		http: {
			verb: 'post',
			path: '/deletecomments'
		}
	});
}