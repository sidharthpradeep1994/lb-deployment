module.exports = function (Ticketdetail) {
	
	Ticketdetail.fetchTicketDetail = async function (options,ticketId) {
		try {
            return await Ticketdetail.app.models.ticketDetail.find({ where: { and: [{ ticketId: ticketId }] }, skip: 0, limit: 0, fields: [], order: '' })
		} catch (error) {
			throw error;
		}

	}

	Ticketdetail.remoteMethod('fetchTicketDetail', {
		accepts: [{
			arg: 'options',
			type: 'object',
			http: 'optionsFromRequest'
		},
		{
			arg: 'ticketId',
			type: 'string',
			required: true
		}
		], returns: {
			arg: 'data',
			type: 'object',
			root: true
		},
		http: {
			verb: 'post',
			path: '/fetchcomments'
		}
	});
}