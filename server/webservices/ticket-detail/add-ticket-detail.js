module.exports = function (Ticketdetail) {
	
	Ticketdetail.addTicketDetail = async function (options,ticketId,message,priority,name) {
		try {
			const token = options && options.accessToken;
			const userId = token && token.userId;
			const user = await Ticketdetail.app.models.user.findById(userId);
			let dateNow = new Date();
			if(user.role=='clientAdmin')
			{
				const client = await Ticketdetail.app.models.client.findById(user.clientId);
				 await Ticketdetail.create({
					ticketId:ticketId,
					userId:userId,
					role:user.role,
					message:message,
					clientId:user.clientId,
					clientName:client.name,
					name:name,
					priority:priority,
					createdAt:dateNow
				});
			}
			else if (user.role=='superAdmin')
			{
				await Ticketdetail.create({
					ticketId:ticketId,
					userId:userId,
					role:user.role,
					message:message,
					name:name,
					priority:priority,
					createdAt:dateNow
				});
			}
			return await Ticketdetail.app.models.ticketDetail.find({ where: { and: [{ ticketId: ticketId }] }, skip: 0, limit: 0, fields: [], order: '' })
			
		} catch (error) {
			throw error;
		}

	}

	Ticketdetail.remoteMethod('addTicketDetail', {
		accepts: [{
			arg: 'options',
			type: 'object',
			http: 'optionsFromRequest'
		},
		{
			arg: 'ticketId',
			type: 'string',
			required: true
		},
		{
			arg:'message',
			type:'string',
			required:'true'
		},
		{
			arg:'priority',
			type :'string',
			required:true
		},
		{
			arg: 'name',
			type: 'string',
			required: true
		}
		], returns: {
			arg: 'data',
			type: 'object',
			root: true
		},
		http: {
			verb: 'post',
			path: '/addcomment'
		}
	});
}