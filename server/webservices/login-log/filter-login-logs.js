module.exports = function (LoginLog) {
    const AppError = require('../../utils/app-error');
    const Roles = require('../../lib/models/roles');
    LoginLog.filterLogs = async function (options, clientId, skip, limit, userId) {

        try {

            const accessToken = options && options.accessToken;
            const id = accessToken && accessToken.userId;

            const userData = await LoginLog.app.models.user.findById(id);

            if (userData.clientId !== clientId && userData.role !== Roles.SUPERADMIN) {
                throw new AppError('Forbidden access', 403)
            }

            filter = [
                // { clientId: clientId }
            ];

            userId && filter.push({ userId: userId });

            const loginLogs = await LoginLog.find({ where: { and: filter }, skip: skip, limit: limit, order: 'createdAt DESC' });

            const count = await LoginLog.count({ and: filter });

            return {
                loginLogs, count
            };





        }
        catch (error) {
            throw error;
        }


    }


    LoginLog.remoteMethod('filterLogs', {
        accepts: [
            { arg: 'options', type: 'object', http: 'optionsFromRequest' },
            { arg: 'clientId', type: 'string', required: true },
            { arg: 'skip', type: 'number', required: true },
            { arg: 'limit', type: 'number', required: true },
            { arg: 'userId', type: 'string', required: true }
        ], returns: {
            arg: 'data', type: 'object', root: true
        },
        http: { path: '/:clientId/filter', verb: 'GET' }
    })


}