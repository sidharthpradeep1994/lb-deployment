module.exports = function (therapy) {
	therapy.addClientTherapy = async function (options, name, categoryId, configId, shortDesc, tagId, reportingStatements) {
		try {
			const token = options && options.accessToken;
			const userId = token && token.userId;

			const category = await therapy.app.models.category.findById(categoryId);
			const config = await therapy.app.models.configType.findById(configId);
			if (!category || !config) {
				throw new Error('Invalid config or catgeory id');
				return;
			}
			// const reportingStatements = getReportingStatements(config.reportingProperties);
			const tags = [];
			tagId && tags.push(tagId);
			return await therapy.create({
				name: name,
				configId: config.id,
				configName: config.name,
				categoryId: category.id,
				categoryName: category.name,
				reportingStatements: reportingStatements,
				shortDesc: shortDesc,
				tags: tags,
				createdBy: userId
			}, options);


		} catch (error) {
			throw error;
		}

	}

	function getReportingStatements(reportingProperties) {
		const reportingStatements = [];

		reportingProperties.map((property) => reportingStatements.push({
			propertyName: reportingProperties.propertyName,
			description: reportingProperties.description,
			value: ''
		}));
		return reportingStatements;
	}

	therapy.remoteMethod('addClientTherapy', {
		accepts: [{
			arg: 'options',
			type: 'object',
			http: 'optionsFromRequest'
		},

		{
			arg: 'name',
			type: 'string',
			required: true
		}, {
			arg: 'categoryId',
			type: 'string',
			required: true
		},
		{
			arg: 'configId',
			type: 'string',
			required: true
		},
		{
			arg: 'shortDesc',
			type: 'string',
			required: true
		},
		{
			arg: 'tagId',
			type: 'string',
			required: true
		}], returns: {
			arg: 'data',
			type: 'object',
			root: true
		},
		http: {
			verb: 'post',
			path: '/add'
		}
	});
}
