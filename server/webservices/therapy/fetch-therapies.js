module.exports = function (therapy) {
	therapy.fetchTherapies = async function (options, category, programId) {
		try {
			const token = options && options.accessToken;
			const userId = token && token.userId;
			const user = await therapy.app.models.user.findById(userId);
			const client = await therapy.app.models.client.findById(user.clientId);
			programId = programId || client.selectedProgram;
			if (!user || !user.clientId) {
				throw new Error('Authorization Error');
			}

			const therapies = await therapy.find({ where: { and: [{ categoryName: category }, { clientId: user.clientId }, { programId: programId }] }, skip: 0, limit: 0, order: ['configId ASC', 'index ASC'] });
			const sortedTherapies = {};
			//Treated as objects for easy handling
			therapies.forEach(therapy => {
				sortedTherapies[therapy.configId] ? sortedTherapies[therapy.configId].therapies.push(therapy) :
					sortedTherapies[therapy.configId] = { configName: therapy.configName, therapies: [therapy] };
			});

			const therapyList = [];
			for (const key in sortedTherapies) {
				if (sortedTherapies.hasOwnProperty(key)) {
					sortedTherapies[key].id = key;
					therapyList.push(sortedTherapies[key]);

				}
			}

			therapyList.sort((current, prev) => current.id - prev.id);
			return therapyList;
		} catch (error) {
			throw error;
		}

	}

	therapy.remoteMethod('fetchTherapies', {
		accepts: [{
			arg: 'options',
			type: 'object',
			http: 'optionsFromRequest'
		},
		{
			arg: 'category',
			type: 'string',
			required: true
		},
		{
			arg: 'programId',
			type: 'string',
			required: true
		}], returns: {
			arg: 'data',
			type: 'object',
			root: true
		},
		http: {
			verb: 'get',
			path: '/fetch'
		}
	});
}
