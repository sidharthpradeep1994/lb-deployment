const Roles = require('../lib/models/roles');

module.exports = [
    {
        accessType: '*',
        principalType: 'ROLE',
        principalId: '$everyone',
        permission: 'DENY',
        property: '*',
    },
    //add case log
    {
        accessType: 'EXECUTE',
        principalType: 'ROLE',
        principalId: Roles.CLIENTADMIN,
        permission: 'ALLOW',
        property: 'addCaseLog'
    },
    {
        accessType: 'EXECUTE',
        principalType: 'ROLE',
        principalId: Roles.CLINICIAN,
        permission: 'ALLOW',
        property: 'addCaseLog'
    },
    // Add Medication 
    {
        accessType: 'EXECUTE',
        principalType: 'ROLE',
        principalId: Roles.CLIENTADMIN,
        permission: 'ALLOW',
        property: 'addMedication'
    },
    {
        accessType: 'EXECUTE',
        principalType: 'ROLE',
        principalId: Roles.CLINICIAN,
        permission: 'ALLOW',
        property: 'addMedication'
    },

    // Assign Cases To users

    {
        accessType: 'EXECUTE',
        principalType: 'ROLE',
        principalId: Roles.CLIENTADMIN,
        permission: 'ALLOW',
        property: 'asssignCasesToUsers'
    },

    // Assign users
    {
        accessType: 'EXECUTE',
        principalType: 'ROLE',
        principalId: 'clientAdmin',
        permission: 'ALLOW',
        property: 'assignUsers'
    },

    // complete patient case 
    {
        accessType: 'EXECUTE',
        principalType: 'ROLE',
        principalId: Roles.WORKINGCLINICIAN,
        permission: 'ALLOW',
        property: 'completePatientCase'
    },
    // download pdf
    {
        accessType: 'EXECUTE',
        principalType: 'ROLE',
        principalId: Roles.CLIENTADMIN,
        permission: 'ALLOW',
        property: 'downloadPdf'
    },

    {
        accessType: 'EXECUTE',
        principalType: 'ROLE',
        principalId: Roles.CLINICIAN,
        permission: 'ALLOW',
        property: 'downloadPdf'
    },

    // download reports
    {
        accessType: 'EXECUTE',
        principalType: 'ROLE',
        principalId: Roles.CLIENTADMIN,
        permission: 'ALLOW',
        property: 'downloadReports'
    },
    {
        accessType: 'EXECUTE',
        principalType: 'ROLE',
        principalId: Roles.CLINICIAN,
        permission: 'ALLOW',
        property: 'downloadReports'
    },
    // fetch all Recommendations
    {
        accessType: 'EXECUTE',
        principalType: 'ROLE',
        principalId: Roles.CLINICIAN,
        permission: 'ALLOW',
        property: 'fetchAllRecommendations'
    },
    {
        accessType: 'EXECUTE',
        principalType: 'ROLE',
        principalId: Roles.CLIENTADMIN,
        permission: 'ALLOW',
        property: 'fetchAllRecommendations'
    },
    // Fetch monthly case list
    {
        accessType: 'EXECUTE',
        principalType: 'ROLE',
        principalId: Roles.CLINICIAN,
        permission: 'ALLOW',
        property: 'fetchMonthlyCaseList'
    },
    {
        accessType: 'EXECUTE',
        principalType: 'ROLE',
        principalId: Roles.CLIENTADMIN,
        permission: 'ALLOW',
        property: 'fetchMonthlyCaseList'
    },
    {
        accessType: 'EXECUTE',
        principalType: 'ROLE',
        principalId: Roles.SUPERADMIN,
        permission: 'ALLOW',
        property: 'fetchMonthlyCaseList'
    },
    //fetch overall recommendations 
    {
        accessType: 'EXECUTE',
        principalType: 'ROLE',
        principalId: Roles.CLINICIAN,
        permission: 'ALLOW',
        property: 'fetchOverallRecommendations'
    },
    {
        accessType: 'EXECUTE',
        principalType: 'ROLE',
        principalId: Roles.CLIENTADMIN,
        permission: 'ALLOW',
        property: 'fetchOverallRecommendations'
    },
    //fetch recommendation
    {
        accessType: 'EXECUTE',
        principalType: 'ROLE',
        principalId: Roles.CLINICIAN,
        permission: 'ALLOW',
        property: 'fetchRecommendation'
    },
    {
        accessType: 'EXECUTE',
        principalType: 'ROLE',
        principalId: Roles.CLIENTADMIN,
        permission: 'ALLOW',
        property: 'fetchRecommendation'
    },
    //fetch status count 

    {
        accessType: 'EXECUTE',
        principalType: 'ROLE',
        principalId: '$authenticated',
        permission: 'ALLOW',
        property: 'fetchStatusCount'
    },
    //fetch summary 
    {
        accessType: 'EXECUTE',
        principalType: 'ROLE',
        principalId: Roles.CLINICIAN,
        permission: 'ALLOW',
        property: 'fetchSummary'
    },
    {
        accessType: 'EXECUTE',
        principalType: 'ROLE',
        principalId: Roles.CLIENTADMIN,
        permission: 'ALLOW',
        property: 'fetchSummary'
    },
    //filter registries
    {
        accessType: 'EXECUTE',
        principalType: 'ROLE',
        principalId: Roles.CLINICIAN,
        permission: 'ALLOW',
        property: 'filterRegistries'
    },
    {
        accessType: 'EXECUTE',
        principalType: 'ROLE',
        principalId: Roles.CLIENTADMIN,
        permission: 'ALLOW',
        property: 'filterRegistries'
    },
    {
        accessType: 'EXECUTE',
        principalType: 'ROLE',
        principalId: Roles.SUPERADMIN,
        permission: 'ALLOW',
        property: 'filterRegistries'
    },
    //filter to Select
    {
        accessType: 'EXECUTE',
        principalType: 'ROLE',
        principalId: Roles.CLIENTADMIN,
        permission: 'ALLOW',
        property: 'filterToSelect'
    },
    //remove medication 
    {
        accessType: 'EXECUTE',
        principalType: 'ROLE',
        principalId: Roles.CLINICIAN,
        permission: 'ALLOW',
        property: 'removeMedication'
    },
    {
        accessType: 'EXECUTE',
        principalType: 'ROLE',
        principalId: Roles.CLIENTADMIN,
        permission: 'ALLOW',
        property: 'removeMedication'
    },
    //reset medication
    {
        accessType: 'EXECUTE',
        principalType: 'ROLE',
        principalId: Roles.CLINICIAN,
        permission: 'ALLOW',
        property: 'resetMedication'
    },
    {
        accessType: 'EXECUTE',
        principalType: 'ROLE',
        principalId: Roles.CLIENTADMIN,
        permission: 'ALLOW',
        property: 'resetMedication'
    },
    //save pdf letters
    {
        accessType: 'EXECUTE',
        principalType: 'ROLE',
        principalId: Roles.CLINICIAN,
        permission: 'ALLOW',
        property: 'savePDFLetters'
    },
    {
        accessType: 'EXECUTE',
        principalType: 'ROLE',
        principalId: Roles.CLIENTADMIN,
        permission: 'ALLOW',
        property: 'savePDFLetters'
    },
    //save therapy
    {
        accessType: 'EXECUTE',
        principalType: 'ROLE',
        principalId: Roles.CLINICIAN,
        permission: 'ALLOW',
        property: 'saveTherapy'
    },
    {
        accessType: 'EXECUTE',
        principalType: 'ROLE',
        principalId: Roles.CLIENTADMIN,
        permission: 'ALLOW',
        property: 'saveTherapy'
    },
    //scheduled cases
    {
        accessType: 'EXECUTE',
        principalType: 'ROLE',
        principalId: Roles.CLINICIAN,
        permission: 'ALLOW',
        property: 'scheduledCases'
    },
    {
        accessType: 'EXECUTE',
        principalType: 'ROLE',
        principalId: Roles.CLIENTADMIN,
        permission: 'ALLOW',
        property: 'scheduledCases'
    },
    //suspend cases
    {
        accessType: 'EXECUTE',
        principalType: 'ROLE',
        principalId: Roles.CLINICIAN,
        permission: 'ALLOW',
        property: 'suspendCase'
    },
    {
        accessType: 'EXECUTE',
        principalType: 'ROLE',
        principalId: Roles.CLIENTADMIN,
        permission: 'ALLOW',
        property: 'suspendCase'
    },
    // update allergy 
    {
        accessType: 'EXECUTE',
        principalType: 'ROLE',
        principalId: Roles.CLINICIAN,
        permission: 'ALLOW',
        property: 'updateAllergy'
    },
    {
        accessType: 'EXECUTE',
        principalType: 'ROLE',
        principalId: Roles.CLIENTADMIN,
        permission: 'ALLOW',
        property: 'updateAllergy'
    },
    //update case notes
    {
        accessType: 'EXECUTE',
        principalType: 'ROLE',
        principalId: Roles.CLINICIAN,
        permission: 'ALLOW',
        property: 'updateCaseNotes'
    },
    {
        accessType: 'EXECUTE',
        principalType: 'ROLE',
        principalId: Roles.CLIENTADMIN,
        permission: 'ALLOW',
        property: 'updateCaseNotes'
    },
    //update medication notes
    {
        accessType: 'EXECUTE',
        principalType: 'ROLE',
        principalId: Roles.CLINICIAN,
        permission: 'ALLOW',
        property: 'updateMedicationNotes'
    },
    {
        accessType: 'EXECUTE',
        principalType: 'ROLE',
        principalId: Roles.CLIENTADMIN,
        permission: 'ALLOW',
        property: 'updateMedicationNotes'
    },
    //update medication
    {
        accessType: 'EXECUTE',
        principalType: 'ROLE',
        principalId: Roles.CLINICIAN,
        permission: 'ALLOW',
        property: 'updateMedication'
    },
    {
        accessType: 'EXECUTE',
        principalType: 'ROLE',
        principalId: Roles.CLIENTADMIN,
        permission: 'ALLOW',
        property: 'updateMedication'
    },
    //update medicaine order
    {
        accessType: 'EXECUTE',
        principalType: 'ROLE',
        principalId: Roles.CLINICIAN,
        permission: 'ALLOW',
        property: 'updateMedicineOrder'
    },
    {
        accessType: 'EXECUTE',
        principalType: 'ROLE',
        principalId: Roles.CLIENTADMIN,
        permission: 'ALLOW',
        property: 'updateMedicineOrder'
    },
    // update overall recommendation order
    {
        accessType: 'EXECUTE',
        principalType: 'ROLE',
        principalId: Roles.CLINICIAN,
        permission: 'ALLOW',
        property: 'updateOverallRecommendation'
    },
    {
        accessType: 'EXECUTE',
        principalType: 'ROLE',
        principalId: Roles.CLIENTADMIN,
        permission: 'ALLOW',
        property: 'updateOverallRecommendation'
    },
    // update recommendation
    {
        accessType: 'EXECUTE',
        principalType: 'ROLE',
        principalId: Roles.CLINICIAN,
        permission: 'ALLOW',
        property: 'updateRecommendation'
    },
    {
        accessType: 'EXECUTE',
        principalType: 'ROLE',
        principalId: Roles.CLIENTADMIN,
        permission: 'ALLOW',
        property: 'updateRecommendation'
    },
    //case statistics 
    {
        accessType: 'EXECUTE',
        principalType: 'ROLE',
        principalId: Roles.CLIENTADMIN,
        permission: 'ALLOW',
        property: 'caseStatistics'
    },
    // {
    //     accessType: '*',
    //     principalType: 'ROLE',
    //     principalId: '$authenticated',
    //     permission: 'ALLOW',
    //     property: '*',
    // }
];