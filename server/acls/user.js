module.exports = [
    {
        "accessType": "EXECUTE",
        "principalType": "ROLE",
        "principalId": "$everyone",
        "permission": "ALLOW",
        "property": "fetchResetQuestions"
    },
    {
        "accessType": "EXECUTE",
        "principalType": "ROLE",
        "principalId": "$everyone",
        "permission": "ALLOW",
        "property": "resetUserPassword"
    },
    {
        "accessType": "EXECUTE",
        "principalType": "ROLE",
        "principalId": "$everyone",
        "permission": "ALLOW",
        "property": "userCreateOneTimePassword"
    },
    {
        "accessType": "EXECUTE",
        "principalType": "ROLE",
        "principalId": "$everyone",
        "permission": "ALLOW",
        "property": "resetUserPassword"
    },
    {
        "accessType": "EXECUTE",
        "principalType": "ROLE",
        "principalId": "clientAdmin",
        "permission": "ALLOW",
        "property": "resetOneTimePassword"
    },
    {
        "accessType": "EXECUTE",
        "principalType": "ROLE",
        "principalId": "superAdmin",
        "permission": "ALLOW",
        "property": "resetOneTimePassword"
    },
    {
        "accessType": "EXECUTE",
        "principalType": "ROLE",
        "principalId": "clientAdmin",
        "permission": "ALLOW",
        "property": "updateUser"
    },
    {
        "accessType": "EXECUTE",
        "principalType": "ROLE",
        "principalId": "clientAdmin",
        "permission": "ALLOW",
        "property": "filterToSelect"
    },
    {
        "accessType": "EXECUTE",
        "principalType": "ROLE",
        "principalId": "admin",
        "permission": "ALLOW",
        "property": "filterToSelect"
    },
    {
        "accessType": "EXECUTE",
        "principalType": "ROLE",
        "principalId": "admin",
        "permission": "ALLOW",
        "property": "filterUsers"
    },
    {
        "accessType": "EXECUTE",
        "principalType": "ROLE",
        "principalId": "clientAdmin",
        "permission": "ALLOW",
        "property": "filterUsers"
    },
    {
        "accessType": "EXECUTE",
        "principalType": "ROLE",
        "principalId": "$everyone",
        "permission": "ALLOW",
        "property": "filterUsersForChat"
    },
    {
        "accessType": "EXECUTE",
        "principalType": "ROLE",
        "principalId": "clientAdmin",
        "permission": "ALLOW",
        "property": "fetchUser"
    },
    {
        "accessType": "EXECUTE",
        "principalType": "ROLE",
        "principalId": "admin",
        "permission": "ALLOW",
        "property": "fetchUser"
    },
    {
        "accessType": "EXECUTE",
        "principalType": "ROLE",
        "principalId": "admin",
        "permission": "ALLOW",
        "property": "toggleUserActivation"
    },
    {
        "accessType": "EXECUTE",
        "principalType": "ROLE",
        "principalId": "clientAdmin",
        "permission": "ALLOW",
        "property": "toggleUserActivation"
    },
    {
        "accessType": "EXECUTE",
        "principalType": "ROLE",
        "principalId": "admin",
        "permission": "ALLOW",
        "property": "deleteUser"
    },
    {
        "accessType": "EXECUTE",
        "principalType": "ROLE",
        "principalId": "clientAdmin",
        "permission": "ALLOW",
        "property": "deleteUser"
    },
    {
        "accessType": "EXECUTE",
        "principalType": "ROLE",
        "principalId": "$authenticated",
        "permission": "ALLOW",
        "property": "validatePasswordReset"
    },
    {
        "accessType": "EXECUTE",
        "principalType": "ROLE",
        "principalId": "$everyone",
        "permission": "ALLOW",
        "property": "logout"
    },
]