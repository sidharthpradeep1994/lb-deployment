
module.exports = {
    registerWebservices: require('./register-webservices'),
    registerHooks: require('./register-hooks'),
    registerACLs: require('./register-acl')
}