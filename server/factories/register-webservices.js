/**
 * @param {object} Model The Loopback Model
 * @param {string} modelName The modelName In dash case
 * @param {Array} remoteMethods Array of remote method names
 */
module.exports = function registerWebServices(Model, modelName, remoteMethods) {
  try {
    const typeCheck = require('../lib/helpers/type-checks');
    const toDash = require('../lib/helpers/camelToDash');
    const webservicesPath = '../webservices/';
    if (typeCheck.isArray(remoteMethods)) {
      remoteMethods.map((rm) => {
        let webService = require(`${webservicesPath}${toDash(modelName)}/${toDash(rm)}`);
        if (typeCheck.isFunction(webService)) {
          webService(Model)

        } else throw Error('Incorrect remote method name');
      });
    }
  } catch (error) {
    console.error(error);
  }

};
