/**
 * @param {Object} model The Loopback Model
 * @param {string} modelName The modelName In snake case
 * @param {Array} remoteMethods Array of remote method names
 */
module.exports = function registerHooks(Model, modelName, hooks) {
    try {
      const typeCheck = require('../lib/helpers/type-checks');
      const toDash = require('../lib/helpers/camelToDash');
      const hooksPath = '../hooks/';
      if (typeCheck.isArray(hooks)) {
        hooks.map((hk) => {
          let hook = require(`${hooksPath}${toDash(modelName)}/${toDash(hk)}`);
          if (typeCheck.isFunction(hook)) {
            hook(Model)
  
          } else throw Error('Incorrect hook file name');
        });
      }
    } catch (error) {
      console.error(error);
    }
  
  };
  