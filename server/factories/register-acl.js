/**
 * @param {object} Model The Loopback Model
 * @param {Array} acls 
 */
module.exports = function registerACL(Model, acls) {
    try {
        const typeCheck = require('../lib/helpers/type-checks');
        if (Model && Model.settings && Model.settings.acls && typeCheck.isArray(Model.settings.acls)) {
            Model.settings.acls.push(...acls);
            return Model;
        }
        else if (!typeCheck.isArray(Model.settings.acls)) {
            throw new Error('Model have no ACL settings');
        } else {
            throw new Error('Could not set ACL');
        }
    } catch (error) {
        console.error(error);
    }

};
