var debug = require('debug')('loopback:mixin:track-remote');
module.exports = function (Model, options) {
    var trackAgainst = {};


    options = Object.assign({}, options);
    debug('Init mixin with options', options);


    Model.getApp(function (err, app) {
        const TrackerModel = Model.app.models[options.changeModel];
        trackAgainst[options.changeModel] = Model.modelName;
        Model.afterRemote('**', function (ctx, remoteMethodOutput, next) {
            const TrackerModel = Model.app.models[options.changeModel];
            if (ctx.Model && trackAgainst[ctx.Model.modelName]) {
                // debug(ctx.Model.modelName + ' is being used to track changes against another model. Skipping to avoid infinite recursion');
                return next();
            }
            const methodName = ctx.method.name;

            TrackerModel.create({
                requestId: ctx.req.id, payload: { params: ctx.req.params, query: ctx.req.query, body: ctx.req.body },
                remoteMethod: ctx.method.name,
                url: ctx.req.url,
                orginalUrl: ctx.req.orginalUrl,
                userId: ctx.options && ctx.options.currentUser && ctx.options.currentUser.id,
                response: toJSON(remoteMethodOutput),
                methodName: methodName,
                model: Model.modelName,
                timestamp: Date.now(), createdAt: new Date()
            });
            next();
        });

        function toJSON(data) {
            try {
                return JSON.parse(JSON.stringify(data));
            }
            catch (error) {
                return null;
            }
        }

        Model.afterRemoteError('**', function (ctx, next) {
            if (ctx.Model && trackAgainst[ctx.Model.modelName]) {
                // debug(ctx.Model.modelName + ' is being used to track changes against another model. Skipping to avoid infinite recursion');
                return next();
            }
            //...
            //ctx.req.params, query, body 
            // ctx.method.name
            //ctx.orginalUrl
            //ctx.idKeyNamectx.error
            const TrackerModel = Model.app.models[options.changeModel];
            console.log(TrackerModel)
            // const userId = ctx.options && ctx.options.accessToken && ctx.options.accessToken.userId;
            try {
                process.nextTick(() => TrackerModel.create({
                    requestId: ctx.req.id, payload: { params: ctx.req.params, query: ctx.req.query, body: ctx.req.body },
                    remoteMethod: ctx.method.name,
                    url: ctx.req.url,
                    orginalUrl: ctx.req.orginalUrl,
                    userId: ctx.options && ctx.options.currentUser && ctx.options.currentUser.id,
                    error: ctx.error,
                    model: Model.modelName,
                    timestamp: Date.now(), createdAt: new Date()
                }))
            } catch (error) {
                console.log(error);
            }

            next();
        });
    });
}
