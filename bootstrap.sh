#!/usr/bin/env bash

/vagrant/dependencies/ubuntu.sh

/vagrant/dependencies/nodejs.sh

/vagrant/dependencies/docker.sh

# /vagrant/extras/mongodb.sh

# Add hosts
echo "127.0.0.1 node.test" >> /etc/hosts

# Default to app dir
echo "cd /vagrant" >> /home/vagrant/.bashrc



