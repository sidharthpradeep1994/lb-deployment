# Use one of the standard Node images from Docker Hub 
FROM node:dubnium 
# The Dockerfile's author
LABEL Adil Rahman
# Create a directory in the container where the code will be placed
RUN mkdir -p /loopback-backend
# We'll land here when we SSH into the container.
WORKDIR /loopback-backend


# Copy the current directory contents into the container at /app
COPY . /loopback-backend
# Install nodemodules
RUN npm install 

# Start the server using pm2
# RUN pm2 start server/server.js
# Set this as the default working directory.

# Our Nginx container will forward HTTP traffic to containers of 
# this image via port 3000. For this, 3000 needs to be 'open'.
EXPOSE 3000

ENTRYPOINT [ "node", "server/server.js" ]