#!/usr/bin/env bash

replicas=$1

# Start the docker containers
docker-compose up --build --scale loopback-backend=$replicas -d